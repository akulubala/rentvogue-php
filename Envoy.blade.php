<?php
$repo = 'git@gitlab.deepdevelop.com:deepdevelop-startups/rentvogue-php.git';
$develop_dir = '/srv/rentvogue-php';
$production_dir = '/srv/rentvogue-php';
?>
@servers(['web' => 'dummy@121.201.14.60 -p9527'])

@macro('production', ['on' => 'web'])
    task_production
@endmacro

@macro('develop', ['on' => 'web'])
    task_develop
@endmacro

@task('task_production')
    cd {{ $production_dir }};
    git pull origin master
    composer install --prefer-dist;
    php artisan migrate -n;
	bower install --allow-root;
	gulp
	chown -R nginx vendor
	chgrp -R nginx vendor
@endtask

@task('task_develop')
	cd {{ $develop_dir }};
    git clean
    git checkout -- .
    git pull origin develop
    composer install --no-scripts --prefer-dist;
    php artisan migrate -n;
    php artisan db:seed
    bower install;
    gulp
@endtask

@task('git_production')
    cd {{ $production_dir }};
    git checkout -- .
    git pull origin master
@endtask

@task('git_develop')
    cd {{ $develop_dir }};
    git checkout -- .
    git pull origin develop;
@endtask



