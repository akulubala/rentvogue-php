<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaleServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sale_services', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->integer('admin_id')->unsigned()->index();
			$table->enum('service_type', array('returns','exchanges'))->nullable();
			$table->string('reason_for_service', 200)->nullable();
			$table->enum('acceptance_status', array('waiting_acceptance','accepted','delivering','finished','reject'))->default('waiting_acceptance');
			$table->text('descriptions', 65535)->nullable();
			$table->string('acceptance', 45)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sale_services');
	}

}
