<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 10)->nullable();
			$table->string('nick_name', 10);
			$table->string('cell_phone', 20)->index();
			$table->string('id_number', 45)->nullable();
			$table->string('body_height', 10)->nullable();
			$table->string('body_weight', 10)->nullable();
			$table->string('body_figure', 10)->nullable();
			$table->integer('clothes_size')->unsigned()->nullable();
			$table->string('body_chest', 10)->nullable();
			$table->string('waistline', 10)->nullable();
			$table->string('body_hipline', 10)->nullable();
			$table->string('outseam', 10)->nullable();
			$table->enum('is_active', array('Y','N'))->default('Y');
			$table->boolean('verify_status')->default(0);
			$table->string('user_device_id', 45)->nullable()->index('user_device_id_index');
			$table->string('password', 60);
			$table->string('remember_token', 100)->nullable();
			$table->integer('primary_city')->default(310000);
			$table->string('avatar')->nullable();
			$table->string('id_card_frontend')->nullable();
			$table->string('id_card_backend')->nullable();
			$table->integer('invite_user')->nullable();
			$table->string('invite_code', 64)->index('user_invite_code_index');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
