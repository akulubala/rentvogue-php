<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('comment_user_id');
			$table->integer('order_id');
			$table->integer('product_id')->index('fk_comments_products_idx');
			$table->integer('product_name_group_id')->index('comments_product_name_group_idx');
			$table->integer('stars');
			$table->text('comment', 65535);
			$table->string('tags', 45);
			$table->string('images', 1024);
			$table->enum('is_verify', array('Y','N'))->default('Y');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
