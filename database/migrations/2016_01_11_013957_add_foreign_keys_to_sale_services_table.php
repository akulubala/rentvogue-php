<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSaleServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sale_services', function(Blueprint $table)
		{
			$table->foreign('admin_id', 'fk_sale_services_admins')->references('id')->on('admins')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sale_services', function(Blueprint $table)
		{
			$table->dropForeign('fk_sale_services_admins');
		});
	}

}
