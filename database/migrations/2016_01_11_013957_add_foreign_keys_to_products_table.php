<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->foreign('color_id', 'products_colors')->references('id')->on('colors')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('product_category_id', 'products_product_categories')->references('id')->on('product_categories')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('style_id', 'products_styles')->references('id')->on('styles')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropForeign('products_colors');
			$table->dropForeign('products_product_categories');
			$table->dropForeign('products_styles');
		});
	}

}
