<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_group_id')->index('products_property_group_id_idx');
			$table->integer('name_group_id')->index('products_name_group_id_idx');
			$table->integer('product_category_id')->index('fk_products_product_categories_idx');
			$table->integer('style_id')->unsigned()->index('fk_products_style_idx');
			$table->integer('usage_category')->nullable();
			$table->integer('suit_occasion');
			$table->integer('city')->default(310000);
			$table->string('name', 128);
			$table->integer('color_id')->nullable()->index('fk_products_colors_idx');
			$table->integer('size')->nullable();
			$table->integer('body_figure')->nullable();
			$table->text('describe', 65535);
			$table->integer('market_price');
			$table->integer('deposit');
			$table->integer('four_day_rent_price');
			$table->integer('eight_day_rent_price')->nullable();
			$table->string('supplier');
			$table->string('supplier_phone')->nullable();
			$table->string('supplier_address', 1024);
			$table->string('images', 1024)->nullable();
			$table->enum('is_available', array('Y','N'))->default('Y');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
