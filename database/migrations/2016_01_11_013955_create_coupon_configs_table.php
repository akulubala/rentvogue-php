<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCouponConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coupon_configs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 64);
			$table->integer('type')->default(0);
			$table->string('group', 32);
			$table->integer('need')->unsigned()->default(0);
			$table->integer('cut')->unsigned()->default(0);
			$table->boolean('status')->default(0);
			$table->integer('available_days')->default(0);
			$table->integer('amount');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coupon_configs');
	}

}
