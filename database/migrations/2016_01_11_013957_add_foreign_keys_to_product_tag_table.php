<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_tag', function(Blueprint $table)
		{
			$table->foreign('product_id', 'fk_product_tag_product')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('tag_id', 'fk_product_tag_tags')->references('id')->on('tags')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_tag', function(Blueprint $table)
		{
			$table->dropForeign('fk_product_tag_product');
			$table->dropForeign('fk_product_tag_tags');
		});
	}

}
