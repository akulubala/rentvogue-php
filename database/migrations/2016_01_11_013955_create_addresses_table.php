<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->index('addresses_users_id_index');
			$table->string('contact_user_name', 45)->nullable();
			$table->string('contact_user_cell_phone', 45)->nullable();
			$table->integer('city')->nullable();
			$table->integer('district')->nullable();
			$table->integer('is_default')->default(0);
			$table->text('details', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addresses');
	}

}
