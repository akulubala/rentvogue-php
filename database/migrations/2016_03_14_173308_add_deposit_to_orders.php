<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepositToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            \DB::statement("ALTER TABLE orders MODIFY COLUMN order_status ENUM('not_paid','not_paid_deposit','paid_not_deliveried','exchange_up_level','delivering','recieved','returned','reurned_deposit','sales_warranty','order_canceled','cancel_apply','dealing_cancel','rejected_cancel','returned_cancel_money','exchange_order_finish','paid_not_take')");
            \DB::statement("ALTER TABLE orders MODIFY COLUMN paid_method ENUM('upacp_pc','upacp','upacp_wap','wx','wx_pub_qr','alipay','alipay_qr','alipay_pc_direct')");
            $table->enum('paid_deposit', array('Y', 'N'))->default('Y');
            $table->text('deposit_pingxx_ch', 65535)->nullable();
            $table->enum('deposit_method', array('upacp_pc','upacp','upacp_wap','wx','wx_pub_qr','alipay','alipay_qr','alipay_pc_direct'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('paid_deposit');
            $table->dropColumn('deposit_pingxx_ch');
            $table->dropColumn('deposit_method');
        });
    }
}
