<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOauthsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oauths', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->index('oauths_user_id_foreign');
			$table->enum('oauth_type_name', array('wechat','sina_weibo'));
			$table->string('openid');
			$table->string('unionid')->nullable();
			$table->string('oauth_access_token');
			$table->string('oauth_nick_name');
			$table->string('avatar')->nullable();
			$table->timestamps();
			$table->index(['oauth_type_name','openid']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oauths');
	}

}
