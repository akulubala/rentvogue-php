<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnavaliableRentDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unavaliable_rent_dates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->index('u_rent_product_id_index');
			$table->date('start_date');
			$table->date('end_date');
			$table->timestamps();
			$table->index(['start_date','end_date']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unavaliable_rent_dates');
	}

}
