<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierIdToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('supplier_detail');
            $table->dropColumn('supplier');
            $table->dropColumn('supplier_phone');
            $table->dropColumn('supplier_address');
            $table->integer('supplier_id')->after('eight_day_rent_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('supplier_detail');
            $table->string('supplier');
            $table->string('supplier_phone');
            $table->string('supplier_address');
            $table->dropColumn('supplier_id');
        });
    }
}
