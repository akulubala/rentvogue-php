<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_name_group_id')->unsigned()->nullable()->index();
			$table->integer('parent_order_id')->nullable();
			$table->integer('exchange_to_order_id')->nullable();
			$table->integer('product_id')->index('orders_products_idx');
			$table->integer('user_id')->index('fk_orders_users_idx');
			$table->string('contact_user_name')->nullable();
			$table->string('address', 1024)->default('');
			$table->string('phone', 16);
			$table->enum('rent_days', array('4','8'))->nullable();
			$table->date('rent_start_date');
			$table->date('rent_end_date');
			$table->integer('rent_price')->nullable();
			$table->string('order_number', 45)->nullable();
			$table->text('pingxx_ch', 65535)->nullable();
			$table->integer('coupon_id')->unsigned()->nullable();
			$table->enum('order_status', array('not_paid','paid_not_deliveried','exchange_up_level','delivering','recieved','returned','reurned_deposit','sales_warranty','cancel_apply','dealing_cancel','rejected_cancel','returned_cancel_money','exchange_order_finish'))->default('not_paid');
			$table->enum('paid_method', array('upacp_pc','upacp','upacp_wap','wx','wx_pub_qr','alipay','alipay_pc_direct'))->nullable();
			$table->string('express_company', 45)->nullable();
			$table->string('express_number', 45)->nullable();
			$table->string('reject_cancel_reason', 1024)->nullable();
			$table->dateTime('cancel_apply_date')->nullable();
			$table->timestamps();
			$table->index(['rent_start_date','rent_end_date']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
