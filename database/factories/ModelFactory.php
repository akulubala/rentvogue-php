<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Address::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'user_id' => $faker->numberBetween(1, 30),
        'contact_user_name' => $faker->name,
        'contact_user_cell_phone' => $faker->PhoneNumber,
        'city' => 310000,
        'district' => 310101,
        'is_default' => 0,
        'details' => $faker->address
    ];
});

$factory->define(App\Admin::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'factory_id' => 'xxxxxx',
        'property_group_id' => $faker->numberBetween(1, 20),
        'name_group_id' => $faker->randomElement(['111', '222', '333', '444', '555']),
        'product_category_id' => $faker->numberBetween(1, 4),
        'style_id' => $faker->numberBetween(1,5),
        'usage_category' => $faker->randomElement([10, 20, 30]),
        'occasion_id' => $faker->numberBetween(1,3),
        'body_figure' => $faker->randomElement([10, 20, 30, 40, 50]),
        'city' => $faker->randomElement([110000, 310000, 350200, 440300, 510100]),
        'name' => $faker->word,
        'color_id' => $faker->numberBetween(1, 5),
        'size' => $faker->randomElement([10, 20, 30, 40, 50]),
        'describe' => $faker->text(100),
        'deposit' => $faker->numberBetween(1000, 40000),
        'market_price' => $faker->numberBetween(1000, 40000),
        'four_day_rent_price' => $faker->numberBetween(100, 10000),
        'eight_day_rent_price' => $faker->numberBetween(200, 20000),
        'supplier_id' => $faker->numberBetween(1, 5),
        'images' => ['NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==', 'NjQ4OC0xMDB4MTAwanBnMTQ1MDYxNDE0NTg4OQ==', 'NTk4NDItMTFwbmcxNDUwNjE0MTcyNTE5', 'NTk4NDItMTFwbmcxNDUwNjE0MTcyNTE5'],
    ];
});
$factory->define(App\Order::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    $status = [
        'not_paid',
        'not_paid_deposit',
        'paid_not_deliveried',
        'paid_not_take',
        'exchange_up_level',
        'delivering',
        'recieved',
        'returned',
        'reurned_deposit',
        'sales_warranty',
        'order_canceled',
        'cancel_apply',
        'dealing_cancel',
        'rejected_cancel',
        'returned_cancel_money',
        'exchange_order_finish'
    ];
    return [
        'user_id' => 28,
        'product_name_group_id' => $faker->randomElement(['111', '222', '333', '444', '555']),
        'product_id' => $faker->numberBetween(1, 20),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'rent_days' => $faker->randomElement([4, 8]),
        'rent_start_date' => $faker->date(),
        'rent_end_date' =>$faker->date(),
        'rent_price' => $faker->numberBetween(200, 20000),
        'order_number' => '471966841311',
        'order_status' => $faker->randomElement($status),
        'paid_method' => $faker->randomElement(['upacp_pc', 'upacp', 'upacp_wap', 'wx', 'wx_pub_qr', 'alipay', 'alipay_pc_direct']),
    ];
});

$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'comment_user_id' => $faker->numberBetween(1, 10),
        'order_id' => $faker->numberBetween(1, 10),
        'product_id' => $faker->numberBetween(1, 100),
        'product_name_group_id' => $faker->randomElement(['111', '222', '333', '444', '555']),
        'stars' => $faker->numberBetween(1, 5),
        'comment' => $faker->text(100),
        'tags' => implode('|', [$faker->numberBetween(1, 4), $faker->numberBetween(1, 4), $faker->numberBetween(1, 4)]),
        'images' => implode('|', ['NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==', 'NjQ4OC0xMDB4MTAwanBnMTQ1MDYxNDE0NTg4OQ==', 'NTk4NDItMTFwbmcxNDUwNjE0MTcyNTE5', 'NTk4NDItMTFwbmcxNDUwNjE0MTcyNTE5']),
        'is_verify' => $faker->randomElement(['Y', 'N']),
    ];
});

$factory->define(App\Refund::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'order_id' => $faker->unique()->numberBetween(1, 20),
        'express_company' => $faker->word,
        'express_number' => $faker->unique()->randomnumber(),
    ];
});

$factory->define(App\Refund::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'order_id' => $faker->unique()->numberBetween(1, 20),
        'express_company' => $faker->word,
        'express_number' => $faker->unique()->randomnumber(),
    ];
});

$factory->define(App\Favorite::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('zh_CN');
    return [
        'user_id' => $faker->unique()->numberBetween(1, 10),
        'product_name_group_id' => $faker->randomElement(['111', '222', '333', '444', '555']),
    ];
});
