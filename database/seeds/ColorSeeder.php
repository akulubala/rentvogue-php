<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Color::count();
        if (empty($counts)) {
            Color::create([
                'rgb' => '#FFFAFA',
            ]);
            Color::create([
                'rgb' => '#FFFF00',
            ]);
            Color::create([
                'rgb' => '#FF3030',
            ]);
            Color::create([
                'rgb' => '#4876FF',
            ]);
            Color::create([
                'rgb' => '#FFE1FF',
            ]); 
        }
    }
}
