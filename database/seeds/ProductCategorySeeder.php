<?php

use Illuminate\Database\Seeder;
use App\ProductCategory;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('zh_CN');
        $counts = ProductCategory::count();
        if (empty($counts)) {
            ProductCategory::create([
                'series' => '经典系列',
                'name' => '卡莫尼',
                'image' => 'NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==',
                'order' => 0,
            ]);
            ProductCategory::create([
                'series' => '经典系列',
                'name' => '尼莫',
                'image' => 'NjQ4OC0xMDB4MTAwanBnMTQ1MDYxNDE0NTg4OQ==',
                'order' => 2,
            ]);
            ProductCategory::create([
                'series' => '纯白系列',
                'name' => '哈里',
                'image' => 'NTk4NDItMTFwbmcxNDUwNjE0MTcyNTE5',
                'order' => 1,
            ]);
            ProductCategory::create([
                'series' => '纯黑系列',
                'name' => '时代',
                'image' => 'NTk4NDItMTFwbmcxNDUwNjE0MTcyNTE5',
                'order' => 3,
            ]);            
        }
    }
}
