<?php

use Illuminate\Database\Seeder;
use App\Refund;

class RefundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Refund::class, 20)->create();
    }
}
