<?php

use Illuminate\Database\Seeder;
use App\Occasion;

class OccasionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Occasion::count();
        if (empty($counts)) {
            Occasion::create([
                'name' => '晚宴礼服',
                'english_name' => 'Large Evening Wear',
                'image' => '',
                'intro' => '身姿动人，让你耀眼全场',
                'recommend' => 1,
                'order' => 0,
            ]);
            Occasion::create([
                'name' => '经典婚纱',
                'english_name' => 'Classic Wedding Dress',
                'image' => '',
                'intro' => '穿上婚纱，拥抱人生中最完美的你',
                'recommend' => 1,
                'order' => 1,
            ]);
            Occasion::create([
                'name' => '晚宴礼服',
                'english_name' => 'Large Evening Wear',
                'image' => '',
                'intro' => '衬托出完美演出，你是耀眼的明星',
                'recommend' => 1,
                'order' => 2,
            ]);
        }
    }
}
