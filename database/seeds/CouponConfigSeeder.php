<?php

use Illuminate\Database\Seeder;
use App\CouponConfig;

class CouponConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = CouponConfig::count();
        if (empty($counts)) {
            CouponConfig::create([
                'name' => '新手优惠券1',
                'group' => md5('CouponConfig1'),
                'need' => 100,
                'cut' => 10,
                'available_days' => 7,
                'amount' => 999,
            ]);
            CouponConfig::create([
                'name' => '新手优惠券2',
                'group' => md5('CouponConfig1'),
                'need' => 200,
                'cut' => 20,
                'available_days' => 7,
                'amount' => 999,
            ]);
            CouponConfig::create([
                'name' => '新手优惠券3',
                'group' => md5('CouponConfig1'),
                'need' => 300,
                'cut' => 30,
                'available_days' => 7,
                'amount' => 999,
            ]);
            CouponConfig::create([
                'name' => '新手优惠券4',
                'group' => md5('CouponConfig2'),
                'need' => 20,
                'cut' => 20,
                'available_days' => 7,
                'amount' => 999,
            ]);
            CouponConfig::create([
                'name' => '新手优惠券5',
                'group' => md5('CouponConfig2'),
                'need' => 30,
                'cut' => 30,
                'available_days' => 7,
                'amount' => 999,
            ]); 
            CouponConfig::create([
                'name' => '邀请返利',
                'group' => md5('CouponConfig3'),
                'need' => 100,
                'cut' => 100,
                'available_days' => 30,
                'amount' => 999,
            ]); 
        }
    }
}
