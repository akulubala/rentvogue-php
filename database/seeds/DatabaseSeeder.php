<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AdminSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(StyleTableSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(OrderTableSeeder::class);
        $this->call(RefundSeeder::class);
        $this->call(FavoriteTableSeeder::class);
        $this->call(HotWordSeeder::class);
        $this->call(CouponConfigSeeder::class);
        $this->call(AddressSeeder::class);
        $this->call(SlideSeeder::class);
        $this->call(orderSmsTableSeeder::class);
        $this->call(SupplierSeeder::class);
        $this->call(OccasionSeeder::class);

        Model::reguard();
    }
}
