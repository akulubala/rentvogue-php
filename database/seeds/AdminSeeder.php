<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Admin::where('user_name', '=', 'rentvogue')->count();
        if (!$counts) {
            Admin::create([
                'user_name' => 'rentvogue',
                'role' => 'super_admin',
                'password' => Hash::make('rentvogue1212')
            ]);
        }
    }
}
