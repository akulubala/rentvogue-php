<?php

use Illuminate\Database\Seeder;
use App\Style;

class StyleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Style::count();
        if (!$counts) {
            Style::create([
                    'name' => '长裙'
            ]);
            Style::create([
                    'name' => '短裙'
            ]);
            Style::create([
                    'name' => '欧美范'
            ]);
            Style::create([
                    'name' => '英伦范'
            ]);
            Style::create([
                    'name' => '帅气美'
            ]);
        }
    }
}
