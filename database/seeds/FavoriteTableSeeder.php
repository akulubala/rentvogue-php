<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use App\Favorite;
use Illuminate\Database\Seeder;

class FavoriteTableSeeder extends Seeder {

	public function run()
	{
		factory(Favorite::class, 50)->create();
	}

}
