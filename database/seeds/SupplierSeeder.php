<?php

use Illuminate\Database\Seeder;
use App\Supplier;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Supplier::count();
        if (!$counts) {
            Supplier::create([
                    'name' => '一号供应商',
                    'phone' => '13757573827',
                    'address' => '上海',
                    'detail' => '上海',
            ]);
            Supplier::create([
                    'name' => '二号供应商',
                    'phone' => '13757573827',
                    'address' => '北京',
                    'detail' => '北京',
            ]);
            Supplier::create([
                    'name' => '三号供应商',
                    'phone' => '13757573827',
                    'address' => '深圳',
                    'detail' => '深圳',
            ]);
            Supplier::create([
                    'name' => '四号供应商',
                    'phone' => '13757573827',
                    'address' => '厦门',
                    'detail' => '厦门',
            ]);
            Supplier::create([
                    'name' => '五号供应商',
                    'phone' => '13757573827',
                    'address' => '广州',
                    'detail' => '广州',
            ]);
        }
    }
}
