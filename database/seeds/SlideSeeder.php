<?php

use Illuminate\Database\Seeder;
use App\Slide;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Slide::count();
        if (empty($counts)) {
            Slide::create([
                'name' => 'slide1',
                'type' => 0,
                'related_id' => 1,
                'image' => 'NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==',
                'order' => 0,
            ]);
            Slide::create([
                'name' => 'slide2',
                'type' => 0,
                'related_id' => 2,
                'image' => 'NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==',
                'order' => 4,
            ]);
            Slide::create([
                'name' => 'slide3',
                'type' => 0,
                'related_id' => 3,
                'image' => 'NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==',
                'order' => 3,
            ]);
            Slide::create([
                'name' => 'slide4',
                'type' => 1,
                'related_id' => 222,
                'image' => 'NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==',
                'order' => 1,
            ]); 
            Slide::create([
                'name' => 'slide5',
                'type' => 1,
                'related_id' => 333,
                'image' => 'NjYzNTkwLXh4anBnMTQ1MDYxNDE4OTU5Mg==',
                'order' => 2,
            ]);             
        }
    }
}
