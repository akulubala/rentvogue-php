<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $counts = User::count();
        if ($counts == 0) {
            for ($i= 0 ; $i < 30 ; $i++) {
                User::create([
                    'name' => $faker->name,
                    'nick_name' => $faker->userName,
                    'cell_phone' => '100000000' . $i,
                    'id_number' => $faker->numerify('##################'),
                    'body_height' => $faker->numberBetween(100, 260),
                    'body_weight' => $faker->numberBetween(30, 150),
                    'verify_status' => $faker->numberBetween(0, 3),
                    'body_figure' => $faker->randomElement(['10', '20', '30', '40', '50']),
                    'clothes_size' => $faker->randomElement(['10', '20', '30', '40', '50']),
                    'body_chest' => $faker->numberBetween(50, 100),
                    'body_hipline' => $faker->numberBetween(20, 80),
                    'outseam' => $faker->numberBetween(50, 80),
                    'password' => Hash::make('11111111'),
                    'remember_token' => str_random(60)
                ]);
            }
        }
    }
}
