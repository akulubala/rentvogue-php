<?php

use App\OrderSmsLog;
use Illuminate\Database\Seeder;

class orderSmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!$count = OrderSmsLog::count()) {
            for ($i = 1; $i < 20 ; $i++) {
                OrderSmsLog::create([
                    'user_id' => $i,
                    'order_id' => $i,
                    'title' => $i . 'sms sms sms ',
                    'type' => array_rand([2,4,7,11,12,13]),
                    'content' => 'content'
                ]);
            }
        }
    }
}
