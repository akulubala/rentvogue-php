<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Tag::count();
        if (empty($counts)) {
            Tag::create([
                'style_id' => 1,
                'name' => '很合适',
            ]);
            Tag::create([
                'style_id' => 1,
                'name' => '裙子很美',
            ]);
            Tag::create([
                'style_id' => 1,
                'name' => '女神范',
            ]);
            Tag::create([
                'style_id' => 2,
                'name' => '时尚感',
            ]);
            Tag::create([
                'style_id' => 2,
                'name' => '酷炫',
            ]);
            Tag::create([
                'style_id' => 3,
                'name' => '很好看',
            ]);
        }
    }
}
