<?php

use Illuminate\Database\Seeder;
use App\HotWord;

class HotWordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = HotWord::count();
        if (empty($counts)) {
            HotWord::create([
                'word' => '裙子',
            ]);
            HotWord::create([
                'word' => '耐克',
            ]);
            HotWord::create([
                'word' => '女神',
            ]);
            HotWord::create([
                'word' => '裤子',
            ]);            
        }
    }
}
