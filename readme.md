# Rentvogue-php
## 运行环境

* PHP 5.6
* MySQL 5.6
* Nginx 
* Node 5.2.0

安装上述运行环境。

## 工具

* composer
* bower

安装上述工具。

## 克隆代码并安装依赖

```
git clone git@gitlab.deepdevelop.com:deepdevelop-startups/rentvogue-php.git
cd <仓库位置>
composer install --no-scripts --prefer-dist
composer dump-autoload

npm install
bower install
```

## 设置配置文件

```
cp .env.example .env
```

根据实际情况修改。

## 设置数据库

```
php artisan migrate
php artisan db:seed
```

## 构建

```
npm run gulp
npm run build
```
