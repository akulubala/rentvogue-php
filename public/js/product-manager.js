$(document).ready(function() {
    $('.admin-form').delegate('.add_same_products', 'click', function(){
        var currDiv = $(this).parents('.form-group');
        var pregPattern = /\d+/gi;

        if (!currDiv.find('.colors').val()) {
            alert('产品颜色,尺码,数量,图片均不能为空！');
            return false;
        }

        if (currDiv.nextUntil('.product_preview').length == 0) {
            if (!currDiv.find('.sizes').val() || !currDiv.find('.amounts').val()) {
                alert('产品颜色,尺码,数量,图片均不能为空！');
                return false;
            }
        } else {
            if (!currDiv.nextUntil('.product_preview').last().find('.sizes').val() ||
                !currDiv.nextUntil('.product_preview').last().find('.amounts').val()) {
                alert('产品颜色,尺码,数量,图片均不能为空！');
                return false;
            }
        }

        if (!$(this).hasClass('different_color')) { //添加同色产品
            var sizeAmountName = currDiv.nextUntil('.product_preview')
                                        .length == 0 ? 0 : currDiv.nextUntil('.product_preview')
                                                                  .last()
                                                                  .find('select')
                                                                  .attr('name'),
                colorAmountName = currDiv.find('select')
                                         .first()
                                         .attr('name');
                colorAmountIndex = parseInt(pregPattern.exec(colorAmountName)[0]);
            if (sizeAmountName !== 0) {
                var pregPatternx = /\d+/g;
                pregPatternx.exec(sizeAmountName);
                var sizeAmountIndex = pregPatternx.exec(sizeAmountName);
                ++sizeAmountIndex;
            } else {
                sizeAmountIndex = 1;
            }

            var unavailableSizes = [],
                availableSizes = [],
                allSizes = $('.color-size-amounts').first().find('.sizes').children('option'),
                sizeGroupDivs = currDiv.nextUntil('.product_preview');
            $.each(allSizes, function(i) {
                if ($(this).val()) {
                    availableSizes.push({'key' : $(this).val(), 'size': $(this).text() })
                }
            });
            if (sizeGroupDivs.length > 0) {
                $.each(sizeGroupDivs, function(i) {
                    var v = $(this).find('.sizes').children('option:selected').val(),
                        size = $(this).find('.sizes').children('option:selected').text();
                    if (v) {
                        unavailableSizes.push({'key' : v , 'size' : size});
                    }
                });
            }
            unavailableSizes.push({'key' : currDiv.find('.sizes').children('option:selected').val(), 'size' : currDiv.find('.sizes').children('option:selected').text()});

            availableSizes = _.reject(availableSizes, function(item) {
                var good = false;
                _.each(unavailableSizes, function(tm) {
                   if (_.isEqual(tm,item)) {
                        good = true;
                        return;
                   }
                });
                return good;
            });
            if (_.isEmpty(availableSizes)) {
                    alert('所有尺寸均已添加');
                    return false;
            }
            var tpl = _.template($("#same_color_products").html())({
                        size: "size["+ colorAmountIndex +"]["+ sizeAmountIndex +"]",
                        amount: "amount["+ colorAmountIndex +"]["+sizeAmountIndex+"]",
                        availableSizes : availableSizes
                    });
            $(tpl).insertBefore(currDiv.nextAll('.product_preview').first());
        } else { //添加其他颜色产品
            var lastPreview = $('.product_preview').last().children('.gallery-box');
            if (lastPreview.length > 0) {
                var colorAmountName = $('.color-group').last()
                                                   .find('select')
                                                   .first()
                                                   .attr('name'),
                next = $('#pickfiles').parents('.form-group'),
                colorAmountIndex = parseInt(pregPattern.exec(colorAmountName)[0]) + 1;
                var unavailableColors = [],
                    availableColors = [],
                    colorGroupDivs = $('.color-group');
                $.each(colorGroupDivs.first().find('.colors').children('option'), function() {
                    if ($(this).val()) {
                        availableColors.push({'key': $(this).val(), 'color': $(this).text()});
                    }
                })
                if (colorGroupDivs.length > 0) {
                    $.each(colorGroupDivs, function(i) {
                        var v = $(this).find('.colors').children('option:selected').val(),
                            color = $(this).find('.colors').children('option:selected').text();
                        if (v) {
                            unavailableColors.push({'key' : v , 'color' : color});
                        }
                    });
                }

                availableColors = _.reject(availableColors, function(item) {
                                        var good = false;
                                        _.each(unavailableColors, function(tm) {
                                               if (_.isEqual(tm,item)) {
                                                    good = true;
                                                    return;
                                               }
                                            });
                                            return good;
                                });

                if (_.isEmpty(availableColors)) {
                    alert('所有颜色均已添加');
                    return false;
                }

                tpl = _.template($("#different_color_products").html())({
                        color: "color["+ colorAmountIndex +"]",
                        size: "size["+ colorAmountIndex +"][0]",
                        amount: "amount["+colorAmountIndex+"][0]",
                        idx: colorAmountIndex,
                        availableColors: availableColors
                    });
                $(tpl).insertBefore(next);
                $.each(lastPreview, function() {
                    $(this).find('.btn-group').addClass('hidden');
                });
            } else {
                alert('请先添加产品图片');
            }
        }
    });
    //删除尺码
    $('.admin-form').delegate('.delete_size', 'click', function() {
        $(this).parents('.size-group').remove();
    });
    //删除颜色
    $('.admin-form').delegate('.delete_same_products', 'click', function(){
            if (flow.isUploading()) {
                alert('图片正在上传，请稍后..');
                return false;
            }
            var currentDiv = $(this).parents('.form-group'),
                idx = currentDiv.nextAll('.product_preview').first().attr('idx');
            $.each($('#images').children('.' + idx + 'products'), function(i) {
                    $(this).remove();
            });
            $.each(currentDiv.prev('.product_preview').children('.gallery-box'), function() {
                    $(this).find('.remove_picture')
                           .removeClass('hidden');
            });
            if (currentDiv.nextAll('.color-group').length < 1) {
                currentDiv.nextUntil('#add_image_div')
                          .remove()
                          .end()
                          .remove();
            } else {
                currentDiv.nextUntil('.color-group')
                          .remove()
                          .end()
                          .remove();
            }
    });
    //移除图片
    $('.admin-form').delegate('.remove_picture', 'click', function() {
        if (flow.isUploading()) {
            if (!window.confirm('图片正在上传确定要删除吗？')) {
                return false;
            }
        }
        flow.cancel();
        var previewIdx = $(this).parents('.product_preview').attr('idx'),
            gallery = $(this).parents('.gallery-box'),
            productIdx = parseInt($(this).parents('.gallery-box').index()) - 1;

        gallery.remove();
        $('#images').find("input[name='product_image["+ previewIdx +"]["+ productIdx +"]']").remove()
    });
    //更改颜色
    $('.admin-form').delegate('.colors', 'change', function() {
        $(this).parent('div').prev('div').css('background-color', $(this).children('option:selected').val() ? $(this).children('option:selected').text() : '');
    });

    $('#product_form').validate({
        rules: {
            product_category_id: {
                required: true,
            },
            usage_category: {
                required: true
            },
            suit_occasion: {
                required: true,
            },
            name: {
                required: true,
            },
            body_figure: {
                required: true,
            },
            deposit: {
                required: true
            },
            four_day_rent_price: {
                required: true
            },
            eight_day_rent_price: {
                required: true
            },
            supplier: {
                required: true
            },
            supplier_phone: {
                required: true
            },
            supplier_address: {
                required: true
            },
            describe: {
                required: true
            },
            images_guard: {
                required: {
                     depends:function(){
                           valid = true;
                           $.each($('.color-group'), function() {
                                var preview = $(this).nextAll('.product_preview').first();
                                if (preview.children('.gallery-box').length === 0) {
                                    valid = false;
                                    return;
                                }
                           });
                           return valid === false ? true : null;
                    }
                }
            }
        },
        messages: {
            product_category_id: {
                required: '产品品牌不能为空',
            },
            usage_category: {
                required: '使用类别不能为空'
            },
            suit_occasion: {
                required: '使用场合不能为空',
            },
            name: {
                required: '产品名称不能为空',
            },
            body_figure: {
                required: '体型不能为空',
            },
            deposit: {
                required: '押金金额不能为空'
            },
            four_day_rent_price: {
                required: '租期四天价格不能为空'
            },
            eight_day_rent_price: {
                required: '租期八天价格不能为空'
            },
            supplier: {
                required: '供货商不能为空'
            },
            supplier_phone: {
                required: '供货商电话不能为空'
            },
            supplier_address: {
                required: '供货商地址不能为空'
            },
            describe: {
                required: '商品描述不能为空'
            },
            images_guard: {
                required: '产品图片不能为空'
            }
        },
        onclick: false,
        onfocusout: false,
        onkeyup: false,
        ignore: [],
        showErrors: function(errorMap, errorList) {
            var errorMessage = '';
            for(var key in errorMap) {
                errorMessage += errorMap[key] + '\r\n';
            }
            var tpl = _.template($('#js_errors_template').html(),{variable: 'items'})(errorMap);
            if ($('#content').has($('#js_errors'))) {
                $('#content').find('#js_errors')
                        .remove()
            }
            $('#content').prepend(tpl);

        },
        errorPlacement: function(errors, element,t) {

        }
    });
});
