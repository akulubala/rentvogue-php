(function($) {
    /**
     * 对于POST、PUT、DELETE请求，服务器会进行csrf验证，默认为请求添加_token参数。
     */
    var defaultAjaxMethod = $.ajax;
    $.ajax = function(options) {
        var token,
            type = options.type.toUpperCase();

        if (_.isUndefined(options.data) || _.isNull(options.data)) {
            options.data = {};
        }

        if (_.indexOf(['POST', 'PUT', 'DELETE'], type) > -1) {
            token = $("meta[name='csrf_token']").attr('content');
            if (!_.isUndefined(token)) {
                options.data._token = token;
            }
        }

        return defaultAjaxMethod(options);
    };

    $.parseQueryString = function(queryString) {
        if (_.isUndefined(queryString)) {
            queryString = document.location.search;
        }

        if (!_.isString(queryString)) {
            return {};
        }

        queryString = queryString.trim().replace(/^(\?|#|&)/, '');
        if (_.isEmpty(queryString)) {
            return {};
        }

        return _.reduce(queryString.split('&'), function(result, param) {
            var parts = param.replace(/\+/g, ' ').split('='),
                key = decodeURIComponent(parts[0]),
                value = _.isUndefined(parts[1]) ? null : decodeURIComponent(parts[1]);

            if (!result.hasOwnProperty(key)) {
                result[key] = value;
            } else if (Array.isArray(result[key])) {
                result[key].push(value);
            } else {
                result[key] = [result[key], value];
            }

            return result;
        }, {});
    };
})(jQuery);
