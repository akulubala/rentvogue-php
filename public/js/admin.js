jQuery(document).ready(function() {
"use strict";
// Init Theme Core
Core.init();
$('.btn-option').click(function() {
    var type = $(this).attr('option'),
        data_key = $(this).attr('data-key'),
        data_v = $(this).attr('data-v'),
        url = $(this).attr('data-url'),
        data = null;
    if (typeof data_key !== 'undefined') {
        if (typeof data_v !== 'undefined') {
            data = JSON.parse('{"' + data_key + '" : "' + data_v + '"}');
        }
    }
    if (type === 'delete') {
        if (window.confirm('数据删除后将无法恢复，是否继续？')) {
            $.ajax({
                type : type,
                url : url,
                data : data
            }).done(function() {
                location.reload();
            }).fail(function(response) {
                if (typeof response.responseJSON != 'undefined') {
                    alert(response.responseJSON.error);
                } else {
                    alert('操作失败，请稍后重试');
                }
            });
        }
    } else {
        $.ajax({
                type : type,
                url : url,
                data : data
            }).done(function() {
                location.reload();
            }).fail(function() {
                alert('操作失败，请稍后重试');
            });
    }

    $.each($('.sub-nav').find('li'), function() {
        if ($(this).hasClass('active')) {
            $(this).parent('ul').prev('a').addClass('menu-open');
        }
    });
});
$.each($('.sub-nav').find('li'), function() {
    if ($(this).hasClass('active')) {
        $(this).parent('ul').prev('a').addClass('menu-open');
    }
});
function unique_string() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}
});
