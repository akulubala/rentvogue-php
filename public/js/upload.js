Flow.FlowFile.prototype.defaultChunkEvent = Flow.FlowFile.prototype.chunkEvent;
Flow.FlowFile.prototype.chunkEvent = function(chunk, event, message) {
    if (event === 'success' && !this.error) {
        this.flowObj.fire('fileChunkSucess', this, chunk, JSON.parse(message));
    }
    this.defaultChunkEvent(chunk, event, message);
};
Flow.FlowChunk.prototype.prepareXhrRequest = function(_, _, _, blob) {
    var target = this.getTarget(
        this.flowObj.opts.target + '/mkblk/' + (this.endByte - this.startByte),
        [
            'name=' + this.fileObj.uniqueIdentifier + '.' + this.fileObj.getExtension(),
            'chunk=' + (this.offset + 1),
            'chunks=' + this.fileObj.chunks.length
        ]
    );
    this.xhr.open('POST', target, true);
    this.xhr.setRequestHeader('Authorization', 'UpToken ' + this.flowObj.opts.upload_token);
    return blob;
};
flow = new Flow({
    target: 'http://upload.qiniu.com',
    simultaneousUploads: 3,
    chunkSize: 4194304, //4 * 1024 * 1024,
    testChunks: false,
    forceChunkSize: true,
    singleFile: false,
    allowDuplicateUploads: true,
    upload_token: $('input[name="qiniu_token"]').val(),
});
flow.assignBrowse(
    $('#pickfiles'),
    false,
    false,
    { accept: 'image/*' }
);
flow.on('fileAdded', function(file, e){
    var reader = new FileReader(),
        insertBeforeDiv = $('#pickfiles').parents('.form-group')
                                         .prev('.product_preview');
    if (insertBeforeDiv.children('.gallery-box').length >= 4) {
        alert('同一颜色产品图片最多四张');
        return false;
    }
    if (flow.isUploading()) {
        alert('图片正在上传，请稍后..');
        return false;
    }
    if (file.size > 52428800) {
                alert('图片大小不得超过50M');
                return false;
    }
    reader.onload = function(e) {
        var image = new Image();
        image.src = e.target.result
        image.onload = function(){
            var width = this.width,
                height = this.height;
            //if (width != 836 || height != 1250) {
                //alert('图片尺寸需为836 * 1250');
                //flow.cancel();
            //} else {
                var tpl = _.template($("#product_preview").html())({'image':this.src});
                insertBeforeDiv.append(tpl);
            //}
        };

    }
    if (file.file) {
        reader.readAsDataURL(file.file);
    }
});
flow.on('fileProgress', function(file, chunk) {
    var speed = file.currentSpeed,
        percent = (file.sizeUploaded() * 100 / file.size).toFixed(2)  + '%',
        insertBeforeDiv = $('#pickfiles').parents('.form-group').prev('.product_preview');
    if (speed < 1024) {
        speed = parseFloat((speed).toFixed(2)) + 'BYTE/S';
    } else {
        speed = parseFloat((speed / 1024).toFixed(2)) + 'KB/S';
    }
    $.each(insertBeforeDiv.children('.gallery-box').find('.progress-bar'), function() {
        if ($(this).css('width') == '0px') {
            $(this).css('width', percent).html(percent);
            return;
        }
    });
});
flow.on('filesSubmitted', function() {
    flow.upload();
});
flow.on('fileChunkSucess', function(file, chunk, response) {
    if (_.isUndefined(file.ctx)) {
        file.ctx = response.ctx;
    } else {
        file.ctx += ',' + response.ctx;
    }
});
flow.on('fileSuccess', function(file) {
    var key = Base64.encode(file.uniqueIdentifier + (new Date()).valueOf());
    $.ajax({
        type: 'POST',
        url: flow.opts.target + '/mkfile/' + file.size + '/key/' + Base64.encode(key),
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8',
            'Authorization': 'UpToken ' + flow.opts.upload_token
        },
        dataType: 'JSON',
        data: file.ctx,
    }).done(function(response) {
        var preview = $('#pickfiles').parents('.form-group').prev('.product_preview');
        preview.children('.gallery-box')
               .last()
               .end()
               .find('.progress-bar')
               .css('width', '100%')
               .html('100%');
        $('<input type="hidden" name="images[]" value="'+ response.key +'" />').appendTo($('#images'));
    }).fail(function() {
        flow.fire('fileError', file);
    });
});
flow.on('fileError', function(file, message){
    console.log(message);
});
