/* Chinese initialisation for the jQuery UI date picker plugin. */
/* Written by Cloudream (cloudream@gmail.com). */
( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional[ "zh-CN" ] = {
	closeText: "关闭",
	prevText: "&#x3C;上月",
	nextText: "下月&#x3E;",
	currentText: "今天",
	monthNames: [ "01月","02月","03月","04月","05月","06月",
	"07月","08月","09月","10月","11月","12月" ],
	monthNamesShort: [ "一月","二月","三月","四月","五月","六月",
	"七月","八月","九月","十月","十一月","十二月" ],
	dayNames: [ "星期日","星期一","星期二","星期三","星期四","星期五","星期六" ],
	dayNamesShort: [ "周日","周一","周二","周三","周四","周五","周六" ],
	dayNamesMin: [ "周日","周一","周二","周三","周四","周五","周六" ],
	weekHeader: "周",
	dateFormat: "yy-mm-dd",
	firstDay: 0,
	isRTL: false,
	showMonthAfterYear: true,
	showOtherMonths: true,
	yearSuffix: "年"
};
datepicker.setDefaults( datepicker.regional[ "zh-CN" ] );

return datepicker.regional[ "zh-CN" ];

} ) );