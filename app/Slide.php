<?php

namespace App;

class Slide extends BaseModel
{
    protected $fillable = [
        'name',
    	'type',
        'order',
    	'related_id',
        'image',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'related_id', 'name_group_id');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'related_id', 'id');
    }

    public function image()
    {
        return $this->image ? env('QINIU_DOMAIN_NAME') . '/' . $this->image : null;
    }

    public function type()
    {
        if ($this->type == 0) {
            return '系列';
        } else {
            return '单品';
        }
    }

    public function related_name()
    {
        if ($this->type == 0) {
            return $this->category->name;
        } else {
            return $this->product->name;
        }
    }
}
