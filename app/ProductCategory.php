<?php

namespace App;

class ProductCategory extends BaseModel
{
    protected $fillable = [
    	'name',
    	'image',
        'series',
        'recommend',
        'order',
        'detail',
    ];

    public function products()
    {
        return $this->HasMany(Product::class);
    }

    public function image()
    {
        return $this->image ? env('QINIU_DOMAIN_NAME') . '/' . $this->image : null;
    }

    public function scopeRecommend($query)
    {
        return $query->where('recommend', 1);
    }
}
