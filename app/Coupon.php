<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {

	protected $fillable = [
		'cell_phone',
		'config_id',
		'group',
		'deadline',
		'referee',
        'status'
	];

    public function config()
    {
        return $this->belongsTo(CouponConfig::class, 'config_id', 'id');
    }

    public function getStatusAttribute($value)
    {
        if ($this->deadline < date('Y-m-d H:i:s')) {
            return 'expired';
        } else {
            return $value;
        }
    }

    public function scopeAvailable($query)
    {
        return $query->where('status', 'available');
    }
}
