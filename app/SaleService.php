<?php

namespace App;

use App\Admin;
use Illuminate\Database\Eloquent\Model;

class SaleService extends BaseModel
{
    protected $fillable = [
                               'order_id',
                               'admin_id',
                               'service_type',
                               'reason_for_service',
                               'acceptance_status',
                               'descriptions',
                               'acceptance'
                          ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
