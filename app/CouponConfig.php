<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponConfig extends Model {

	protected $fillable = [
		'type',
		'group',
		'need',
		'cut',
		'status',
		'available_days',
		'name',
		'amount'
	];

}
