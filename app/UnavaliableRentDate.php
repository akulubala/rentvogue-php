<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class UnavaliableRentDate extends Model
{
    protected $fillable = ['product_id', 'start_date', 'end_date'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
