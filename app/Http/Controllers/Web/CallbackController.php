<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Order;
use App\UnavaliableRentDate;
use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use DB;

class CallbackController extends Controller
{
    public function pingxxResult(Order $order, UnavaliableRentDate $unavaliable)
    {
        $response = file_get_contents("php://input");
        $event = json_decode($response);
        // 对异步通知做处理
        if (!isset($event->type)) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
            exit("fail");
        }
        switch ($event->type) {
            case "charge.succeeded":
                DB::beginTransaction();
                    try {
                        $orderNumber = $event->data->object->order_no;
                        $orderNumber = substr($orderNumber, 0, 13);
                        $order = $order->where('order_number', $orderNumber)->first();
                        if ($order->paid_deposit == 'Y') {
                            if ($order->express_company == 'self') {
                                $order->order_status = 'paid_not_take';
                            } else {
                                $order->order_status = 'paid_not_deliveried';
                            }
                            $order->transaction_no = $event->data->object->transaction_no;
                        } else {
                            $order->order_status = 'not_paid_deposit';
                        }
                        $order->save();
                    } catch (Exception $e) {
                        custom_logs('pingxx_charge_succeeded', '订单支付'.$orderNumber.'回调支付失败');
                        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
                        exit("fail");
                    }
                    try {
                        $unavaliable->create(['product_id' => $order->product_id,
                                          'start_date' => $order->rent_start_date,
                                          'end_date' => Carbon::parse($order->rent_end_date)->addDays(4)->toDateString()]);
                    } catch (Exception $e) {
                        DB::rollback();
                        custom_logs('pingxx_charge_succeeded', '订单'.$orderNumber.'更新付款成功失败');
                        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
                        exit("fail");
                    }
                DB::commit();
                //删掉之前的缓存重新计算
                get_unavaliable_rent_dates($order->product->property_group_id, true);
                header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
                break;
            case "refund.succeeded":
                $orderNumber = strtoupper($event->data->object->order_no);
                try {
                    $orderNumber = strtoupper($event->data->object->order_no);
                    $order = $order->where('order_number', '=', $orderNumber)->first();
                    $order->order_status = 'returned_cancel_money';
                    $order->save();
                    //删掉订单对应的不可租时间记录
                    $unavaliable->where('product_id', $order->product_id)
                                ->where('start_date', $order->start_date)
                                ->delete();
                    luosimao_sms($order->user->cell_phone, '退单金额已返还, 订单号:'. $orderNumber . '【rentvogue】');
                } catch (Exception $e) {
                    custom_logs('pingxx_charge_succeeded', '订单支付'.$orderNumber.'回调支付失败');
                    header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
                    exit("fail");
                }
                //删掉之前的缓存重新计算
                get_unavaliable_rent_dates($order->product->property_group_id, true);
                header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
                break;
            default:
                header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request');
                break;
        }
    }
}
