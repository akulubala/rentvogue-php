<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Feedback;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    private $feedback;

    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $feedbacks = $this->feedback
                           ->search($filter)
                           ->orderBy('created_at', 'desc')
                           ->paginate(config('view.pagination.backend'));

        return view('admin.feedback.index', compact('feedbacks', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feedback = $this->feedback->find($id);
        
        return response()->json(['details' => nl2br($feedback->details)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->feedback->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '反馈信息']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
