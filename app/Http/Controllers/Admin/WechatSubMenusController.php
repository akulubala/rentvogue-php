<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\WechatMenusRequest;
use App\WechatMenu;
use Illuminate\Http\Request;

class WechatSubMenusController extends Controller
{
    public function create($menuID)
    {
        $menu = WechatMenu::find($menuID);
        return view('admin.wechat_menus.sub_menus.create', compact('menu'));
    }

    public function store(WechatMenusRequest $request, $menuID)
    {
        WechatMenu::create($request->all());
        return redirect()->route('admin.wechat-menus.show', $menuID)
                         ->with('success', trans('status.success.store', ['name' => '二级菜单']));
    }

    public function edit($menuID, $subMenuID)
    {
        $menu = WechatMenu::find($menuID);
        $subMenu = WechatMenu::find($subMenuID);
        return view('admin.wechat_menus.sub_menus.edit', compact('menu', 'subMenu'));
    }

    public function update(WechatMenusRequest $request, $menuID, $subMenuID)
    {
        $inputs = $request->all();
        WechatMenu::find($subMenuID)->update($inputs);
        return redirect()->route('admin.wechat-menus.show', $menuID)
                         ->with('success', trans('status.success.update', ['name' => '二级菜单']));
    }

    public function destroy($menu, $subMenu)
    {
        WechatMenu::where('id', '=', $subMenu)->delete();
        session()->flash('success', trans('status.success.delete', ['name' => '二级菜单']));
    }
}
