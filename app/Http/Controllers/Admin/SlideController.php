<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Slide;
use App\ProductCategory;
use App\Product;
use App\Http\Requests\Admin\SlideRequest;
use App\Http\Controllers\Controller;
use Input;

class SlideController extends Controller
{
    private $slide;
    private $category;
    private $product;

    public function __construct(Slide $slide, ProductCategory $category, Product $product)
    {
        $this->slide = $slide;
        $this->category = $category;
        $this->product = $product;
    }

    public function index(Request $request)
    {
        $slides = $this->slide
                        ->orderBy('order')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.slide.index', compact('slides'));
    }

    public function create()
    {
        $categories = $this->category->all()->lists('name', 'id');
        $products = $this->product->groupBy('name_group_id')->get()->lists('name', 'name_group_id');
        $type = 0;
        $related_id = 0;

        return view('admin.slide.create', compact('categories', 'products', 'type', 'related_id'));
    }

    public function store(SlideRequest $request)
    {
        $this->slide->create($request->all());
        return redirect()->route('admin.slide.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '幻灯片']
                            )
                         );
    }

    public function edit($id)
    {
        $slide = $this->slide->findOrfail($id);
        $image = $slide->image();
        $related_id = $slide->related_id;
        $type = $slide->type;
        $categories = $this->category->all()->lists('name', 'id');
        $products = $this->product->groupBy('name_group_id')->get()->lists('name', 'name_group_id');

        return view('admin.slide.edit', compact('slide', 'image', 'categories', 'products', 'type', 'related_id'));
    }

    public function update(SlideRequest $request, $id)
    {
        try {
            $this->slide->find($id)->update($request->all());
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.slide.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '幻灯片']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->slide->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '幻灯片']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }

    public function postOrder(Request $request)
    {
        $order_value = $request->get('order_value');
        foreach ($order_value as $id => $order) {
            $slide = $this->slide->find($id);
            if (!$slide) {
                continue;
            }
            $slide->order = $order;
            $slide->save();
        }
        return response()->json([], 200);
    }
}
