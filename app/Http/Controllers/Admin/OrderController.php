<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrderRequest;
use App\Order;
use App\OrderSmsLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JPush;

class OrderController extends Controller
{
    public function index(Request $request, Order $order)
    {
        if ($request->get('next_week_delivery')) {
            $orders = $order->DeliveryNextWeek()
                             ->paginate(config('view.pagination.backend'));
            $status = config('system.database.order_status');
            return view('admin.order.delivering', compact('orders', 'status'));
        } else {
            $orders = $order->search($request->get('filter'))
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));
            $status = config('system.database.order_status');
            return view('admin.order.index', compact('orders', 'status'));
        }

    }

    public function show($order_id, Order $order)
    {
        $order = $order->find($order_id);

        return view('admin.order.show', compact('order'));
    }

    public function update(OrderRequest $request, Order $order, OrderSmsLog $orderSmsLog, $id)
    {
        $order = $order->findOrFail($id);
        if ($request->get('order_status', null)) {
            /**
             * 更改定单状态
             */
            $order->order_status = $request->get('order_status');
            $this->changeOrderStatus($order, $orderSmsLog);
            session()->flash('success', trans('status.success.update', ['name' => '订单']));
        } else {
            /**
             * 录入快递地址
             */
            $inputs = $request->only(['name', 'value']);
            $name = $inputs['name'];
            $order->$name = $inputs['value'];
        }
        $order->save();
    }

    public function getCancelAccept(Request $request, Order $order)
    {
        $orderId = $request->get('order_id');
        $order = $order->find($orderId);
        $chId = $order->pingxx_ch['id'];

        $hours = Carbon::parse($order->cancel_apply_date)->diffInHours(Carbon::parse($order->rent_start_date), false);
        $amount = 0;
        $rent_price = $order->rent_price;
        //去掉押金的租用价格进行计算，判断是否已经支付押金
        if ($order->paid_deposit == 'Y') {
            $rent_price = $rent_price - $order->product->deposit;
        }
        if ($hours > 24 && $hours < 7 * 24) {
            $amount = $rent_price * 0.8;
        }
        if ($hours < 24) {
            $amount = $rent_price;
        }
        //如果支付了押金连同押金一起返还
        if ($order->paid_deposit == 'Y') {
            $amount += $order->product->deposit;
        }
        if ($amount > 0) {
            try {
                $response = json_decode(pingxx_refund($chId, $amount), true);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()], 400);
            }
            if ($response['status'] == 'pending') {
                return response()->json(['url' => $response['url']], 200);
            } elseif ($response['status'] == 'succeeded') {
                return response()->json([], 200);
            } else {
                custom_logs('pingxx_refund', json_encode($response, true). '|'. 'ch_id:' . $chId);
                return response()->json(['error' => '退款失败'], 200);
            }
        }
        return response()->json([], 200);
    }

    private function changeOrderStatus($order, $orderSmsLog)
    {
        $available = [2,4,7,11,12,13];
        $status = $order->order_status;
        if (in_array($status, $available)) {
            $cellPhone = $order->user->cell_phone;
            $messageTitle = config('system.order_status_change_message')[$status];
            // send sms
            $message = $messageTitle. '【Rentvogue】';
            luosimao_sms($cellPhone, $message);
            // send push notice
            $deviceId = $order->user->user_device_id;
            if ($deviceId !== null) {
                try {
                    $client = new JPush(config('services.jpush.app_key'), config('services.jpush.master_secret'), storage_path('logs/jpush.log'));
                    $result = $client->push()
                                ->setPlatform('ios')
                                ->addAllAudience()
                                ->addIosNotification($message, 'iOS sound', JPush::DISABLE_BADGE, true, 'iOS category', array("type"=>"order_change", "order_id"=>$order->id))
                                ->send();
                } catch(\Exception $e) {
                    custom_logs('jpush', $e->getMessage());
                }

            }
            $orderSmsLog->user_id = $order->user_id;
            $orderSmsLog->order_id = $order->id;
            $orderSmsLog->title = $messageTitle;
            $orderSmsLog->type = $status;
            $orderSmsLog->content = $message;
            $orderSmsLog->save();
        }
        //需要退还押金
        if ($status === 7) {
            $this->return_deposit($order);
        }
    }

    private function return_deposit($order)
    {
        //判断是否分两次支付
        if (empty($order->deposit_pingxx_ch)) {
            $chId = $order->pingxx_ch['id'];
        } else {
            $chId = $order->deposit_pingxx_ch['id'];
        }
        $deposit = $order->user->verify_status == 3 ? $order->product->deposit * 0.8 : $order->product->deposit;
        try {
            $response = json_decode(pingxx_refund($chId, $deposit), true);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        if ($response['status'] == 'pending') {
            return response()->json(['url' => $response['url']], 200);
        } elseif ($response['status'] == 'succeeded') {
            return response()->json([], 200);
        } else {
            custom_logs('pingxx_refund', json_encode($response, true). '|'. 'ch_id:' . $chId);
            return response()->json(['error' => '退还押金失败'], 200);
        }
        return response()->json([], 200);
    }
}
