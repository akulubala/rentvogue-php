<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AdminsRequest;
use App\Http\Controllers\Controller;
use App\Admin;
use Config;
use Input;
use Hash;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        $admins = Admin::search(Input::get('filter'))->orderBy('last_logined_at', 'desc')
                                                     ->paginate(
                                                        Config::get('view.pagination.backend')
                                                       );

        return view('admin.admin.index', compact('admins'));
    }

    public function create()
    {
        return view('admin.admin.create');
    }

    public function store(AdminsRequest $request)
    {

        $exist = Admin::withTrashed()->where('user_name', '=', $request->get('user_name'))->first();
        if ($exist) {
            $exist->restore();
            $exist->user_name = $request->get('user_name');
            $exist->role = $request->get('role');
            $exist->password = Hash::make(Input::get('password'));
            $exist->save();
        } else {
            Admin::create([
                'user_name' => $request->get('user_name'),
                'role' => $request->get('role'),
                'password' => Hash::make(Input::get('password')),
            ]);
        }

        return redirect()->route('admin.admin.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => $request->get('role') === 'super_admin' ? '超级管理员' : '管理员']
                                )
                         );
    }

    public function edit($id)
    {
        $admin = Admin::findOrfail($id);

        return view('admin.admin.edit', compact('admin'));
    }

    public function update(AdminsRequest $request, $id)
    {
        $password = Input::get('new_password');
        $admin = Admin::find($id);
        $admin->user_name = Input::get('user_name');
        $admin->role = Input::get('role');
        if (!empty($password)) {
            $admin->password = Hash::make($password);
        }
        $admin->save();

        return redirect()->route('admin.admin.index')
                         ->with('success', trans('status.success.update', ['name' => $request->get('role') === 'super_admin' ? '超级管理员' : '管理员']));
    }

    public function destroy($id)
    {
        $administrator = Admin::find($id);
        /*
         * 当删除用户为登录用户自身的时候，主动退出登录
         */
        if ($administrator->id === Auth::admin()->get()->id) {
            Auth::admin()->logout();
        }
        try {
            session()->flash('success', trans('status.success.delete', ['name' => $administrator->role === 'super_admin' ? '超级管理员' : '管理员']));
            $administrator->delete();
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }

    public function getResetPassword()
    {
        return view('admin.admin.password');
    }

    public function postResetPassword(AdminsRequest $request)
    {
        Auth::admin()->get()->update([
            'password' => Hash::make($request->get('new_password'))
        ]);
        return redirect()->back()
                         ->with('success', trans('status.success.reset_pass_update', ['name' => '管理员']));
    }
}
