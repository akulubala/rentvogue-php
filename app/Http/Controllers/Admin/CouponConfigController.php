<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\CouponConfig;
use App\Http\Requests\Admin\TagRequest;
use App\Http\Controllers\Controller;
use Input;
use DB;

class CouponConfigController extends Controller
{
    private $coupon_config;

    public function __construct(CouponConfig $coupon_config)
    {
        $this->coupon_config = $coupon_config;
    }

    public function index()
    {
        $coupons = DB::table('coupon_configs')
                        ->select('name', 'group', DB::raw('MIN(created_at) as created_at'))
                        ->groupBy('group')
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.coupon.index', compact('coupons'));
    }

    public function show($group)
    {
        $coupons = $this->coupon_config->where('group', $group)
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.coupon.show', compact('coupons', 'group'));
    }

    public function create()
    {
        $group = Input::get('group');
        if (empty($group)) {
            $group = md5(uniqid());
        }
        return view('admin.coupon.create', compact('group'));
    }

    public function store(Request $request)
    {
        $coupon = $this->coupon_config->create($request->all());
        return redirect()->route('admin.coupon_config.show', $coupon->group)
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '优惠券']
                            )
                         );
    }

    public function edit($id)
    {
        $coupon = $this->coupon_config->findOrfail($id);
        $group = $coupon->group;

        return view('admin.coupon.edit', compact('coupon', 'group'));
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token', '_method');
        $coupon = $this->coupon_config->find($id);
        try {
            $coupon->update($inputs);
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.coupon_config.show', $coupon->group)
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '优惠券']
                            )
                         );
    }

    public function destroy($id, Request $request)
    {
        try {
            $inputs = $request->all();
            if (!empty($inputs['group'])) {
                $this->coupon_config->where('group', $inputs['group'])->delete();
            } else {
                $this->coupon_config->find($id)->delete();
            }
            session()->flash('success', trans('status.success.delete', ['name' => '优惠券']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
