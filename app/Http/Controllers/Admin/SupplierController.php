<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Supplier;
use App\Http\Requests\Admin\SupplierRequest;
use App\Http\Controllers\Controller;
use Input;

class SupplierController extends Controller
{
    private $supplier;

    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    public function index(Request $request)
    {
        $suppliers = $this->supplier
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.supplier.index', compact('suppliers'));
    }

    public function create()
    {
        return view('admin.supplier.create');
    }

    public function store(SupplierRequest $request)
    {
        $this->supplier->create($request->all());
        return redirect()->route('admin.supplier.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '供应商']
                            )
                         );
    }

    public function edit($id)
    {
        $supplier = $this->supplier->findOrfail($id);
        return view('admin.supplier.edit', compact('supplier'));
    }

    public function update(Request $request, $id)
    {
        try {
            $this->supplier->find($id)->update($request->all());
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.supplier.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '供应商']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->supplier->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '供应商']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
