<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Occasion;
use App\Http\Requests\Admin\OccasionsRequest;
use App\Http\Controllers\Controller;
use Input;

class OccasionController extends Controller
{

    private $occasion;

    public function __construct(Occasion $occasion)
    {
        $this->occasion = $occasion;
    }

    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $occasions = $this->occasion
                           ->search($filter)
                           ->orderBy('recommend', 'desc')
                           ->orderBy('order')
                           ->paginate(config('view.pagination.backend'));


        return view('admin.occasion.index', compact('occasions', 'filter'));
    }

    public function create()
    {
        return view('admin.occasion.create');
    }

    public function store(OccasionsRequest $request)
    {
        $this->occasion->create($request->all());
        return redirect()->route('admin.occasion.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '场合']
                            )
                         );
    }

    public function edit($id)
    {
        $occasion = $this->occasion->findOrfail($id);
        $image = $occasion->image();
        return view('admin.occasion.edit', compact('occasion', 'image'));
    }

    public function update(OccasionsRequest $request, $id)
    {
        $inputs = $request->except('_method', '_token');
        try {
            $this->occasion->where('id', $id)->update($inputs);
        } catch (\Exception $e) {
            return redirect()->back();
        }

        if (count($inputs) == 1) {
            session()->flash('success', trans('status.success.update', ['name' => '场合']));
            return response()->json(null);
        }

        return redirect()->route('admin.occasion.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '场合']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->occasion->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '场合']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }

    public function postOrder(Request $request)
    {
        $order_value = $request->get('order_value');
        foreach ($order_value as $id => $order) {
            $occasion = $this->occasion->find($id);
            if (!$occasion || $occasion->recommend != 1) {
                continue;
            }
            $occasion->order = $order;
            $occasion->save();
        }
        return response()->json([], 200);
    }
}
