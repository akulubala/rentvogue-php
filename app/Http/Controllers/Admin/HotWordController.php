<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\HotWord;
use App\Http\Requests\Admin\HotWordRequest;
use App\Http\Controllers\Controller;
use Input;

class HotWordController extends Controller
{

    private $hot_word;

    public function __construct(HotWord $hot_word)
    {
        $this->hot_word = $hot_word;
    }

    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $hot_words = $this->hot_word
                        ->search($filter)
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.hot_word.index', compact('hot_words', 'filter'));
    }

    public function create()
    {
        return view('admin.hot_word.create');
    }

    public function store(HotWordRequest $request)
    {
        $this->hot_word->create($request->all());
        return redirect()->route('admin.hot_word.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '热词']
                            )
                         );
    }

    public function edit($id)
    {
        $hot_word = $this->hot_word->findOrfail($id);
        return view('admin.hot_word.edit', compact('hot_word'));
    }

    public function update(Request $request, $id)
    {
        try {
            $this->hot_word->find($id)->update($request->all());
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.hot_word.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '热词']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->hot_word->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '热词']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
