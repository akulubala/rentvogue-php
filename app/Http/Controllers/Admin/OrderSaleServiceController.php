<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\SaleService;
use Illuminate\Http\Request;
use Auth;

class OrderSaleServiceController extends Controller
{
    private $service;

    private $order;

    private $guard;

    public function __construct(SaleService $service, Order $order)
    {
        $this->service = $service;
        $this->order = $order;
    }

    public function index(Request $request, $orderId)
    {
        $initialed = $this->order->find($orderId)->saleServices()->count();
        $lastStatus = 'waiting_acceptance';
        if ($initialed == 0) {
            $this->order->find($orderId)->saleServices()
                        ->create(['admin_id' => Auth::admin()->get()->id]);
        } else {
            $lastStatus = $this->order->find($orderId)->saleServices()->get()->last()->acceptance_status;
        }
        $services = $this->order->find($orderId)->saleServices()->orderBy('created_at', 'asc')->get();
        $serviceStatus = [
                            'waiting_acceptance' => '初始化',
                            'accepted' => '已接受',
                            'delivering' => '处理中',
                            'finished' => '已完成',
                            'reject' => '已拒绝'
                        ];

        return view('admin.sale_service.index', compact('services', 'orderId', 'serviceStatus', 'lastStatus'));
    }

    public function update(Request $request, $orderId, $serviceId)
    {
        $service = $this->service->find($serviceId);
        if ($acceptanceStatus = $request->get('acceptance_status', null)) {
            $this->order
                 ->find($orderId)
                 ->saleServices()
                 ->create([
                               'admin_id' => Auth::admin()->get()->id,
                               'service_type' => $service->service_type,
                               'acceptance_status' => $acceptanceStatus
                        ]);
        } else {
            $key = $request->get('name');
            $value = $request->get('value');
            $service->$key = $value;
            $service->save();
        }
        session()->flash('success', trans('status.success.delete', ['name' => '服务单']));
    }

    public function destroy($id, $serviceId)
    {
        try {
            $this->service->find($serviceId)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '服务单']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
