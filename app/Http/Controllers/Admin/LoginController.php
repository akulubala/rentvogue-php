<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use Auth;

class LoginController extends Controller
{
    public function getIndex()
    {
        if (Auth::admin()->check()) {
            return redirect()->route('admin.index.index');
        }
        return view('admin.login.index');
    }

    public function postSignIn(LoginRequest $request)
    {
        Auth::admin()->get()->update([
                'last_logined_at' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.index.index');
    }

    public function getSignOut()
    {
        Auth::admin()->logout();

        return redirect()->route('admin.login.index');
    }
}
