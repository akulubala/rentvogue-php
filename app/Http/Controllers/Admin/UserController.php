<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Config;
use Input;
use Hash;
use Auth;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index(Request $request)
    {
        $verify_status = [
            null => '全部',
            0 => '未上传',
            1 => '已上传',
            2 => '未通过',
            3 => '已通过',
        ];
        $filter = $request->get('filter');
        $users = $this->user
                        ->search($filter)
                        ->orderBy('id', 'desc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.user.index', compact('users', 'filter', 'verify_status'));
    }

    public function create()
    {
        return view('admin.admin.create');
    }

    public function store(Request $request)
    {
        Admin::create([
            'user_name' => $request->get('user_name'),
            'role' => $request->get('role'),
            'password' => Hash::make(Input::get('password')),
        ]);

        return redirect()->route('admin.admin.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => $request->get('role') === 'super_admin' ? '超级管理员' : '管理员']
                                )
                         );
    }

    public function edit($id)
    {
        $user = User::findOrfail($id);

        return view('admin.user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);
        $inputs = $request->except('_method', '_token');
        foreach ($inputs as $key => $value) {
            $user->$key = $value;
        }
        $user->save();

        if (count($inputs) == 1) {
            session()->flash('success', trans('status.success.update', ['name' => '用户']));
            return response()->json(null);
        }

        return redirect()->route('admin.user.index')
                         ->with('success', trans('status.success.update', ['name' => '用户']));
    }

    public function destroy($id)
    {
        $user = $this->user->find($id);
        try {
            $user->oauths()->delete();
            $user->delete();
        }catch(\PDOException $e){
            if ($e->getCode() == 23000) {
                return response()->json(['error' => '该用户有订单信息，无法删除'], 403);
            }
        }
        session()->flash('success', trans('status.success.delete', ['name' => '用户']));
    }

    public function verify()
    {
        $filter = Input::get('filter');
        $users = $this->user
                      ->where('verify_status', 1)
                      ->search($filter)
                      ->orderBy('id', 'desc')
                      ->paginate(config('view.pagination.backend'));

        return view('admin.verify.index', compact('users', 'filter'));
    }

    public function address($userId)
    {
        $addresses = $this->user->find($userId)->addresses()->get();
        return response()->json(['data' => $addresses], 200);
    }

    public function addressConfig()
    {
        return response()->json(['data' => config('system.database.city_code')], 200);
    }
}
