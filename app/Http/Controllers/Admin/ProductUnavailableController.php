<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ProductUnavailableRequest;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductUnavailableController extends Controller
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index($productId)
    {
        $product = $this->product->find($productId);
        $nameGroupId = $product->name_group_id;
        $unavailables = $product->unavailable()
                                 ->paginate(
                                    config('view.pagination.backend')
                                 );
        $startDay = Carbon::today();
        return view('admin.product_unavailable.index', compact('unavailables', 'productId', 'startDay', 'nameGroupId', 'product'));
    }

    public function store(ProductUnavailableRequest $request, $productId)
    {
        $inputs = $request->only(['start_date', 'end_date']);
        if ($inputs['start_date'] > $inputs['end_date']) {
            return redirect()->refresh()
                   ->withErrors(['结束日期必须大于起始时间，添加失败']);
        }
        $inputs['product_id'] = (int)$productId;
        $product = $this->product->find($productId);
        //找出现有的不可租时间进行交集查询
        $rentDates = $product->unavailable()->get(['start_date', 'end_date']);
        foreach ($rentDates as $rentDate) {
            if (!empty(date_intersects($inputs['start_date'], $inputs['end_date'], $rentDate->start_date, $rentDate->end_date))) {
                return redirect()->refresh()
                       ->withErrors(['添加的日期与现有的日期有重复，添加失败']);
            }
        }
        $product->unavailable()->create($inputs);
        return redirect()->route('admin.product.product_unavailable.index', $product->id)
                     ->with('success','添加不可租日期成功');
    }

    public function destroy($productId, $unavailableId)
    {
        $this->product->find($productId)->unavailable()->find($unavailableId)->delete();
    }
}
