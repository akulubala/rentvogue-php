<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\WechatMenusRequest;
use App\WechatMenu;
use Illuminate\Http\Request;

class WechatMenusController extends Controller
{
    public function index()
    {
        $wechatMenus = WechatMenu::where('menu_level', '=', 1)->get();
        return view('admin.wechat_menus.index', compact('wechatMenus'));
    }

    public function create()
    {
        return view('admin.wechat_menus.create');
    }

    public function store(WechatMenusRequest $request)
    {
        WechatMenu::create($request->all());
        return redirect()->route('admin.wechat-menus.index')
                         ->with('success', trans('status.success.store', ['name' => '一级菜单']));
    }

    public function show($id)
    {
        $menu = WechatMenu::findOrFail($id);
        return view('admin.wechat_menus.show', compact('menu'));
    }

    public function edit($id)
    {
        $menu = WechatMenu::findOrFail($id);
        return view('admin.wechat_menus.create', compact('menu'));
    }

    public function update(WechatMenusRequest $request, $id)
    {
        $inputs = $request->all();
        WechatMenu::find($id)->update($inputs);
        return redirect()->route('admin.wechat-menus.index')
                         ->with('success', trans('status.success.update', ['name' => '一级菜单']));
    }

    public function destroy($id)
    {
        WechatMenu::where('id', '=', $id)
                  ->OrWhere('parent_id', '=', $id)->delete();
        session()->flash('success', trans('status.success.delete', ['name' => '一级菜单']));
    }
}
