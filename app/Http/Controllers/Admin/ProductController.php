<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ProductRequest;
use App\Product;
use App\ProductCategory;
use App\Style;
use App\Supplier;
use App\Occasion;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index(Request $request, Color $color)
    {
        $currPage = $request->get('page', 1);
        $limit = config('view.pagination.backend');
        $query = $this->product
                      ->search($request->get('filter'))
                      ->groupBy('name_group_id')
                      ->orderBy('created_at', 'desc');

        $total = $query->get()->count();
        $products = $query->skip($limit*($currPage-1))
                      ->take($limit)
                      ->get();

        $results = [];
        $i = 0;
        foreach ($products as $key => $value) {
            $results[$key] = $value;
        }
        $results = new LengthAwarePaginator($results, $total, $limit, $currPage, ['path' => '/admin/product/']);
        return view('admin.product.index', compact('results'));
    }

    public function create(ProductCategory $category, Color $color, Style $style, Supplier $supplier, Occasion $occasion)
    {
        $categories = $category->lists('name', 'id');
        $occasions = $occasion->lists('name', 'id')->toArray();
        $usageCategories = config('system.database.usage_categories');
        $styles = $style->lists('name', 'id')->toArray();
        $suppliers = $supplier->lists('name', 'id')->toArray();
        $bodyFigures = config('system.database.body_figures');
        $dbCitys = config('system.database.city_code');
        $cities = [];
        foreach ($dbCitys as $key => $value) {
                $cities[$key] = $value['name'];
        }
        $token = qiniu_token();
        return view('admin.product.create', compact('token', 'categories', 'occasions', 'usageCategories', 'styles','bodyFigures', 'cities', 'suppliers'));
    }

    public function store(ProductRequest $request)
    {
        $inputs = $request->except('_token', 'qiniu_token');
        $inputs['name_group_id'] = 1;
        $lastProduct = $this->product->orderBy('name_group_id', 'desc')->first();
        if (!empty($lastProduct)) {
            $inputs['name_group_id'] = $lastProduct->name_group_id + 1;
        }
        $propertyGroupId = 1;
        if (!empty($lastPropertyGroupId = $this->product->orderBy('property_group_id', 'desc')->first())) {
            $propertyGroupId = $lastPropertyGroupId->property_group_id + 1;
        }
        $inputs['property_group_id'] = $propertyGroupId;
        $inputs['is_available'] = 'N';
        foreach ($inputs as $key => $value) {
            $this->product->$key = $value;
        }
        $this->product->save();
        return redirect()->route('admin.product.index')
                     ->with('success',
                            '创建产品属性成功，请继续添加产品图片及尺寸'
                     );
    }

    public function edit($nameGroupId, ProductCategory $category, Color $color, Style $style, Supplier $supplier, Occasion $occasion)
    {
        $product = $this->product->where('name_group_id', '=', $nameGroupId)->orderBy('created_at', 'desc')->first();
        $categories = $category->lists('name', 'id');
        $occasions = $occasion->lists('name', 'id')->toArray();
        $usageCategories = config('system.database.usage_categories');
        $colors = $color->lists('rgb', 'id')->toArray();
        $styles = $style->lists('name', 'id')->toArray();
        $suppliers = $supplier->lists('name', 'id')->toArray();
        $sizes = config('system.database.sizes');
        $bodyFigures = config('system.database.body_figures');
        $token = qiniu_token();
        $dbCitys = config('system.database.city_code');
        $cities = [];
        foreach ($dbCitys as $key => $value) {
                $cities[$key] = $value['name'];
        }
        //找出所有产品图片
        $products = $this->product->where('name_group_id', '=', $nameGroupId)->get();
        $images = [];
        foreach ($products as $product) {
            $images[] = $product->images;
        }
        $images = array_unique($images);
        $imagesList = [];
        foreach ($images as $image) {
            $image_array = json_decode($image, true);
            if (empty($image_array)) {
                continue;
            }
            $imagesList = array_merge($imagesList, $image_array);
        }
        return view('admin.product.edit', compact('product', 'categories', 'occasions', 
            'usageCategories', 'productColors', 'styles', 'bodyFigures', 'token', 
            'cities', 'suppliers', 'imagesList'));
    }

    public function update(ProductRequest $request, $nameGroupId)
    {
        $inputs = $request->except('_token', '_method','qiniu_token');
        $this->product->where('name_group_id', '=', $nameGroupId)->update($inputs);
        return redirect()->route('admin.product.index')
                         ->with('success', trans('status.success.update', ['name' => '产品属性']));
    }

    public function destroy($productId)
    {
        $product = $this->product->find($productId);
        if ($product->factory_id == null) {
            try{
                $product->find($productId)->delete();
            } catch (\Exception $e) {
                return response()->json(['error' => '产品有其他数据与之相关，不能被删除'], 400);
            }
        } else {
            return response()->json(['error' => '产品有其他属性与之关联，请先删除相关项目'], 400);
        }

    }
}
