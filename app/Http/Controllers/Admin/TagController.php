<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Tag;
use App\Style;
use App\Http\Requests\Admin\TagRequest;
use App\Http\Controllers\Controller;
use Input;

class TagController extends Controller
{

    private $tag;
    private $style;

    public function __construct(Tag $tag, Style $style)
    {
        $this->tag = $tag;
        $this->style = $style;
    }

    public function index(Request $request)
    {
        $styles = $this->style->all()->lists('name', 'id')->toArray();
        $styles[null] = '全部款式';
        $filter = $request->get('filter');
        $tags = $this->tag
                        ->search($filter)
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.tag.index', compact('tags', 'filter', 'styles'));
    }

    public function create()
    {
        $styles = $this->style->all()->lists('name', 'id');
        return view('admin.tag.create', compact('styles'));
    }

    public function store(TagRequest $request)
    {
        $this->tag->create($request->all());
        return redirect()->route('admin.tag.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '标签']
                            )
                         );
    }

    public function edit($id)
    {
        $tag = $this->tag->findOrfail($id);
        $styles = $this->style->all()->lists('name', 'id');
        return view('admin.tag.edit', compact('tag', 'styles'));
    }

    public function update(Request $request, $id)
    {
        try {
            $this->tag->find($id)->update($request->all());
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.tag.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '标签']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->tag->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '标签']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
