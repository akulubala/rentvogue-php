<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Comment;
use App\Tag;
use App\Http\Requests\Admin\TagRequest;
use App\Http\Controllers\Controller;
use Input;

class CommentController extends Controller
{
    private $comment;
    private $tag;

    public function __construct(Comment $comment, Tag $tag)
    {
        $this->comment = $comment;
        $this->tag = $tag;
    }

    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $comments = $this->comment
                        ->search($filter)
                        ->orderBy('created_at', 'desc')
                        ->paginate(config('view.pagination.backend'));

        $tags = $this->tag->all()->lists('name', 'id');
        foreach ($comments as &$comment) {
            $comment_tags = [];
            foreach ($comment->tags as $tag) {
                $comment_tags[] = $tags[$tag];
            }
            $comment->tags = implode('|', $comment_tags);
        }
        return view('admin.comment.index', compact('comments', 'filter'));
    }

    public function create()
    {
        return view('admin.comment.create');
    }

    public function store(TagRequest $request)
    {
        $this->comment->create($request->all());
        return redirect()->route('admin.comment.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '产品评论']
                            )
                         );
    }

    public function edit($id)
    {
        $comment = $this->comment->findOrfail($id);
        $tags = $this->tag->lists('name', 'id');

        return view('admin.comment.edit', compact('comment', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token', '_method', 'name');
        if (!empty($inputs['tags'])) {
            $inputs['tags'] = implode('|', $inputs['tags']);
        }
        try {
            $this->comment->where('id', $id)->update($inputs);
        } catch (\Exception $e) {
            return redirect()->back();
        }
        if (count($inputs) == 1 && isset($inputs['is_verify'])) {
            session()->flash('success', trans('status.success.update', ['name' => '产品评论']));
            return response()->json(null);
        }
        return redirect()->route('admin.comment.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '产品评论']
                            )
                         );
    }

    public function destroy($id)
    {
        $this->comment->find($id)->delete();
        session()->flash('success', trans('status.success.delete', ['name' => '产品评论']));
    }
}
