<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Favorite;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function getIndex(User $user, Order $order, Product $product, Favorite $favorite)
    {

        $productCounts = $product->where('is_available', '=', 'Y')->count();
        $userCounts = $user->count();
        $orderCounts = $order->count();
        $favorite3Products = $favorite->select([DB::raw("count(*) as count"), 'product_name_group_id'])
                                       ->groupBy('product_name_group_id')
                                       ->limit(3)
                                       ->get();
        $fivePaidOrders = $order->where('order_status', '=', 'paid_not_deliveried')
                                ->orderBy('created_at', 'desc')
                                ->limit(5)
                                ->get();
        $users = $user->SixMonthsCounts();
        $orders = $order->SixMonthsCounts();
        $amounts = $order->SixMonthsAmountCounts();
        $currentDeliveryOrders = $order->DeliveryNextWeek()->paginate(config('view.pagination.backend'));
        return view('admin.index.index', compact('productCounts',
                                                 'userCounts',
                                                 'orderCounts',
                                                 'favorite3Products',
                                                 'fivePaidOrders',
                                                 'users',
                                                 'orders',
                                                 'currentDeliveryOrders',
                                                 'amounts'));
    }

    public function getToken()
    {
    	return response()->json(['uptoken' => qiniu_token()], 200);
    }
}
