<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Admin\ProductPropertyRequest;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductPropertyController extends Controller
{
    private $product;
    private $color;

    public function __construct(Product $product, Color $color)
    {
        $this->product = $product;
        $this->color = $color;
    }

    public function index($nameGroupId)
    {
        $properties = $this->product
                          ->where('name_group_id', '=', $nameGroupId)
                          ->whereNotNull('factory_id')
                          ->orderBy('color_id', 'desc')
                          ->orderBy('created_at', 'desc')
                          ->paginate(config('view.pagination.backend'));
        return view('admin.product_property.index', compact('properties', 'nameGroupId'));
    }

    public function create($nameGroupId)
    {
        $colors = $this->color->lists('rgb', 'id')->toArray();
        $sizes = config('system.database.sizes');
        $token = qiniu_token();
        $domain = env('QINIU_DOMAIN_NAME');
        return view('admin.product_property.create', compact('colors', 'sizes', 'token', 'nameGroupId', 'domain'));
    }

    public function store(ProductPropertyRequest $request, $nameGroupId)
    {
        $inputs = $request->only(['factory_id', 'color_id', 'size', 'images']);
        $product = $this->product->where('name_group_id', '=', $nameGroupId)
                                 ->orderBy('created_at', 'desc')
                                 ->select(
                                    'id',
                                    'factory_id',
                                    'property_group_id',
                                    'name_group_id',
                                    'product_category_id',
                                    'style_id',
                                    'usage_category',
                                    'occasion_id',
                                    'city',
                                    'name',
                                    'color_id',
                                    'size',
                                    'body_figure',
                                    'describe',
                                    'market_price',
                                    'deposit',
                                    'four_day_rent_price',
                                    'eight_day_rent_price',
                                    'supplier_id',
                                    'images'
                                 )->first();
        if ($product->factory_id === null) {
            $inputs['is_available'] = 'Y';
            foreach ($inputs as $key => $value)
            {
                $product->$key = $value;
            }
            $product->save();
        } else {
            $oldProduct = $product->toArray();
            unset($oldProduct['id']);
            if ($inputs['color_id'] == $oldProduct['color_id']) {
                if ($inputs['size'] == $oldProduct['size']) {
                    $inputs['property_group_id'] = $oldProduct['property_group_id'];
                } else {
                    $maxProperty = $this->product->orderBy('property_group_id', 'desc')
                                                     ->first();
                    $inputs['property_group_id'] = $maxProperty ? $maxProperty->property_group_id + 1 : 1;
                }
            } else {
                $maxProperty = $this->product->orderBy('property_group_id', 'desc')
                                                     ->first();
                $inputs['property_group_id'] = $maxProperty ? $maxProperty->property_group_id + 1 : 1;
            }
            foreach ($oldProduct as $key => $value)
            {
                    if (isset($inputs[$key])) {
                        $oldProduct[$key] = $inputs[$key];
                    }
            }
            $this->product->create($oldProduct);
        }
        return redirect()->route('admin.product.product_property.index', $nameGroupId)
                     ->with('success',
                            trans('status.success.store',
                                ['name' => '产品详情']
                            )
                     );
    }

    public function edit($nameGroupId, $id)
    {
        $property = $this->product->findOrFail($id);
        $colors = $this->color->lists('rgb', 'id')->toArray();
        $sizes = config('system.database.sizes');
        $token = qiniu_token();
        $domain = env('QINIU_DOMAIN_NAME');
        $original_images = $property->original_images();
        return view('admin.product_property.edit', compact('property', 'colors', 'sizes', 'token', 'original_images', 'domain'));
    }

    public function update(ProductPropertyRequest $request, $nameGroupId, $id)
    {
        $product = $this->product
                        ->findOrFail($id);
        if (!$request->has('value')) {
            if ($request->has('is_available')) {
                $product->is_available = $request->get('is_available');
                $product->save();
                return response()->json([], 200);
            } else {
                $inputs = $request->only(['factory_id', 'color_id', 'size', 'images']);
                if ($inputs['color_id'] != $product->color_id || $inputs['size'] != $product->size) {
                    $existProperty = $this->product->where('name_group_id', '=', $nameGroupId)
                                                   ->where('color_id', '=', $inputs['color_id'])
                                                   ->where('size', '=', $inputs['size'])
                                                   ->first();
                    if ($existProperty) {
                        $inputs['property_group_id'] = $existProperty->property_group_id;
                    } else {
                        $maxProperty = $this->product->orderBy('property_group_id', 'desc')
                                                     ->first();
                        $inputs['property_group_id'] = $maxProperty ? $maxProperty->property_group_id + 1 : 1;;
                    }
                }
                foreach ($inputs as $key => $value) {
                    if ($value) {
                        $product->$key = $value;
                    }
                }
                $product->save();
                return redirect()->route('admin.product.product_property.index', $nameGroupId)
                     ->with('success',
                            trans('status.success.update',
                                ['name' => '产品详情']
                            )
                     );
            }

        } else {
            $inputs['factory_id'] = $request->get('value');
            $inputs['is_available'] = 'Y';
            $oldProduct = $product->toArray();
            $excepts = ['id', 'is_available', 'updated_at', 'created_at'];
            $inserts = [];
            foreach ($oldProduct as $key => $value)
            {
                if ($key === 'images') {
                    $images = json_decode($value, true);
                    $withOutMask = [];
                    foreach ($images as $img) {
                        $withOutMask[] = str_replace(config('services.qiniu.water_mask'), '', $img);
                    }
                    $value = json_encode($withOutMask);
                }
                if (!in_array($key, $excepts)) {
                    if (isset($inputs[$key])) {
                         $value = $inputs[$key];
                    }
                    $inserts[$key] = $value;
                }
            }
            DB::table('products')->insert($inserts);
            return response()->json([], 200);

        }
    }

    public function destroy($nameGroupId, $id, Order $order)
    {
        try{
            $inOrder = $order->where('product_id', $id)->first();
            if ($inOrder) {
                return response()->json(['error' => '产品有订单数据与之相关，不能被删除'], 400);
            }
            $count = $this->product
                          ->where('name_group_id', '=', $nameGroupId)
                          ->count();
            if ($count > 1) {
                $this->product->find($id)->delete();
            } else {
                $this->product
                     ->find($id)
                     ->update([
                        'factory_id' => null,
                        'color_id' => null,
                        'size' => null,
                        'is_available' => 'N'
                     ]);

            }

        } catch (\Exception $e) {
            return response()->json(['error' => '产品有其他数据与之相关，不能被删除'], 400);
        }
    }
}
