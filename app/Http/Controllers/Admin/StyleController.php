<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Style;
use App\Http\Requests\Admin\StyleRequest;
use App\Http\Controllers\Controller;
use Input;

class StyleController extends Controller
{

    private $style;

    public function __construct(Style $style)
    {
        $this->style = $style;
    }

    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $styles = $this->style
                           ->search($filter)
                           ->orderBy('created_at', 'desc')
                           ->paginate(config('view.pagination.backend'));

        return view('admin.style.index', compact('styles', 'filter'));
    }

    public function create()
    {
        return view('admin.style.create');
    }

    public function store(StyleRequest $request)
    {
        $this->style->create($request->all());
        return redirect()->route('admin.style.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '产品款式']
                            )
                         );
    }

    public function edit($id)
    {
        $style = $this->style->findOrfail($id);
        return view('admin.style.edit', compact('style'));
    }

    public function update(Request $request, $id)
    {
        try {
            $this->style->find($id)->update($request->all());
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.style.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '产品款式']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->style->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '产品款式']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
