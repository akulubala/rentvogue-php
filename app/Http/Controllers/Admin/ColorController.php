<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Color;
use App\Http\Requests\Admin\ColorRequest;
use App\Http\Controllers\Controller;
use Input;

class ColorController extends Controller
{

    private $color;

    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    public function index(Request $request)
    {
        $colors = $this->color
                        ->orderBy('order_sequence', 'asc')
                        ->paginate(config('view.pagination.backend'));

        return view('admin.color.index', compact('colors'));
    }

    public function create()
    {
        return view('admin.color.create');
    }

    public function store(ColorRequest $request)
    {
        $this->color->create($request->all());
        return redirect()->route('admin.color.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '颜色']
                            )
                         );
    }

    public function edit($id)
    {
        $color = $this->color->findOrfail($id);
        return view('admin.color.edit', compact('color'));
    }

    public function update(Request $request, $id)
    {
        try {
            $this->color->find($id)->update($request->all());
        } catch (\Exception $e) {
            return redirect()->back();
        }
        return redirect()->route('admin.color.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '颜色']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->color->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '颜色']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }
}
