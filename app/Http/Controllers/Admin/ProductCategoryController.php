<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProductCategory;
use App\Http\Requests\Admin\ProductCategoriesRequest;
use App\Http\Controllers\Controller;
use Input;

class ProductCategoryController extends Controller
{

    private $category;

    public function __construct(ProductCategory $category)
    {
        $this->category = $category;
    }

    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $categories = $this->category
                           ->search($filter)
                           ->orderBy('recommend', 'desc')
                           ->orderBy('order')
                           ->paginate(config('view.pagination.backend'));


        return view('admin.product_category.index', compact('categories', 'filter'));
    }

    public function create()
    {
        return view('admin.product_category.create');
    }

    public function store(ProductCategoriesRequest $request)
    {
        $this->category->create($request->all());
        return redirect()->route('admin.product_category.index')
                         ->with('success',
                                trans('status.success.store',
                                ['name' => '产品类别']
                            )
                         );
    }

    public function edit($id)
    {
        $category = $this->category->findOrfail($id);
        $image = $category->image();
        return view('admin.product_category.edit', compact('category', 'image'));
    }

    public function update(ProductCategoriesRequest $request, $id)
    {
        $inputs = $request->except('_method', '_token');
        try {
            $this->category->where('id', $id)->update($inputs);
        } catch (\Exception $e) {
            return redirect()->back();
        }

        if (count($inputs) == 1) {
            session()->flash('success', trans('status.success.update', ['name' => '产品类别']));
            return response()->json(null);
        }

        return redirect()->route('admin.product_category.index')
                         ->with('success',
                                trans('status.success.update',
                                ['name' => '产品类别']
                            )
                         );
    }

    public function destroy($id)
    {
        try {
            $this->category->find($id)->delete();
            session()->flash('success', trans('status.success.delete', ['name' => '产品类别']));
        } catch (\Exception $e) {
            return response()->json(['error' => '当前被删除数据有其他数据与之相关，不能被删除'], 400);
        }
    }

    public function postOrder(Request $request)
    {
        $order_value = $request->get('order_value');
        foreach ($order_value as $id => $order) {
            $category = $this->category->find($id);
            if (!$category || $category->recommend != 1) {
                continue;
            }
            $category->order = $order;
            $category->save();
        }
        return response()->json([], 200);
    }
}
