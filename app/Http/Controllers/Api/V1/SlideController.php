<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Slide;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    private $slide;

    public function __construct(Slide $slide) {
        $this->slide = $slide;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = $this->slide->orderBy('order')->get();
        $data = [];
        foreach ($slides as $slide) {
            $slide_data = $slide->toArray();
            if ($slide->type == 1) {
                $slide_data['link'] = route('api.v1.product.show', $slide->related_id);
            } else {
                $slide_data['link'] = route('api.v1.product.index') . '?category=' . $slide->related_id;
            }
            $slide_data['related_name'] = $slide->related_name();
            $slide_data['image'] = $slide->image();
            $data[] = $slide_data;
        }
        return response()->json(['data' => ['slides' => $data]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
