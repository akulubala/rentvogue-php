<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests\Api\CommentRequest;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Product;
use App\Tag;
use Auth;

class ProductCommentController extends Controller
{
    private $comment;
    private $product;
    private $tag;

    public function __construct(Comment $comment, Product $product, Tag $tag)
    {
        $this->comment = $comment;
        $this->product = $product;
        $this->tag = $tag;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($groupId)
    {
        $comments = $this->comment->verified()->nameGroup($groupId)->orderBy('created_at', 'desc')
                    ->paginate(6)
                    ->appends(\Request::except('page'));
        $data = array();
        $tags = $this->tag->withTrashed()->lists('name', 'id');
        foreach ($comments as $comment) {
            $comment_tags = [];
            foreach ($comment->tags as $tag) {
                $comment_tags[] = $tags[$tag];
            }
            $data[] = [
                'comment_user_id' => $comment->comment_user_id,
                'user_name' => $comment->user->nick_name,
                'stars' => $comment->stars,
                'comment' => $comment->comment,
                'tags' => $comment_tags,
                'images' => json_decode($comment->images),
                'size' => config('system.database.sizes')[$comment->product->size],
                'occasion' => $comment->product->occasion->name,
                'created_at' => (string)$comment->created_at,
            ];
        }
        return response()->json(
            ['data' => [
                'total' => $comments->total(),
                'per_page' => $comments->perPage(),
                'current_page' => $comments->currentPage(),
                'last_page' => $comments->lastPage(),
                'next_page_url' => $comments->nextPageUrl(),
                'prev_page_url' => $comments->previousPageUrl(),
                'data' => $data,
            ]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($productId, CommentRequest $request)
    {
        $userId = Auth::user()->get()->id;
        $product = $this->product->find($productId);
        if (empty($product)) {
            return response()->json(['message' => config('system.error_code.invalid_user_data')['message'], 'error_code' => config('system.error_code.invalid_user_data')['code']], 400);
        }
        $inputs = $request->all();
        $comment = [
            'comment_user_id' => $userId,
            'order_id' => $inputs['order_id'],
            'product_id' => $product->id,
            'product_name_group_id' => $product->name_group_id,
            'stars' => $inputs['star'],
            'comment' => $inputs['comment'],
            'tags' => $inputs['tags'],
            'images' => $inputs['images']
        ];
        $comment = $this->comment->create($comment);

        return response()->json(['data' => $comment], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
