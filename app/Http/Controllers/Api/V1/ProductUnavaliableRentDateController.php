<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\UnavaliableRentDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductUnavaliableRentDateController extends Controller
{
    public function index($property_group_id)
    {
        $unavaliableRentDates = get_unavaliable_rent_dates($property_group_id);
        return response()->json(['data' => $unavaliableRentDates], 200);
    }
}
