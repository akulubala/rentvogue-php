<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Style;

class StyleTagController extends Controller
{
    private $tag;
    private $style;

    public function __construct(Tag $tag, Style $style)
    {
        $this->tag = $tag;
        $this->style = $style;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($style_id)
    {
        $style = $this->style->find($style_id);
        if (empty($style)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        return response()->json(['data' => ['tags' => $style->tags()->get()->lists('name', 'id')]], 200);
    }

    public function show($tagId)
    {
        $tagIds = explode('_', $tagId);
        $tags = $this->tag->whereIn('id', $tagIds)->get();
        return response()->json(['data' => ['tags' => $tags]], 200);
    }

}
