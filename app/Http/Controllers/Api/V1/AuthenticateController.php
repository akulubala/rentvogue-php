<?php

namespace App\Http\Controllers\Api\V1;

use App\Coupon;
use App\CouponConfig;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Api\AuthenticationRequest;
use App\OAuth;
use App\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use JWTAuth;
use Socialite;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;


class AuthenticateController extends Controller
{
    public function postSignIn(Request $request)
    {
        $credentials = $request->only('cell_phone', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => config('system.error_codes.invalid_user')['message'], 'error_code' => config('system.error_codes.invalid_user')['code']], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => config('system.error_codes.token_failed')['message'], 'error_code' => config('system.error_codes.token_failed')['code']], 500);
        }
        $user = JWTAuth::toUser($token);
        if ($user->is_active === 'N') {
            return response()->json(['message' => config('system.error_codes.forbidden_user')['message'], 'error_code' => config('system.error_codes.forbidden_user')['code']], 403);
        }
        return response()->json(['data' => ['token' => $token]], 200);
    }

    public function postSignUp(AuthenticationRequest $request, User $user, Coupon $coupon, CouponConfig $coupon_config)
    {
        $inputs = array_merge($request->only('cell_phone', 'nick_name'),
                             ['password' => Hash::make($request->get('password'))]);

        //查找是否被邀请用户
        $invite = $coupon->where('cell_phone', $inputs['cell_phone'])
                         ->where('referee', '>', 0)->first();
        if (!empty($invite)) {
            $inputs['invite_user'] = $invite->referee;
        }

        $user = $user->create($inputs);
        if (!empty($invite)) {
            //注册邀请方获取优惠券
            $referee = $user->find($invite->referee);
            $coupon_configs = $coupon_config->where('group', config('system.invite_group'))->get();
            if (!empty($coupon_configs)) {
                foreach ($coupon_configs as $coupon_config) {
                    //更新优惠券数量
                    if ($coupon_config->amount > 0) {
                        $coupon_config->amount--;
                        $coupon_config->save();
                        $coupon->create([
                            'cell_phone' => $referee->cell_phone,
                            'config_id' => $coupon_config->id,
                            'deadline' => Carbon::now()->addDays($coupon_config->available_days)->toDateString(),
                            'group' => $coupon_config->group,
                            'referee' => $user->id,
                        ]);
                    }
                }
            }
        }
        $token = JWTAuth::fromUser($user);
        return response()->json(['data'=> ['user' => $user, 'token' => $token]], 201);
    }

    public function getSignOut()
    {
        return response()->json((object)[], 200);
    }

    public function postAppOauth(AuthenticationRequest $request)
    {
        $token = $request->get('access_token');
        $oauthType = $request->get('oauth_type');
        $oauthType = $oauthType === 'weibo' ? 'weibo' : ($oauthType === 'weixin-web' ? 'weixin-web' : 'weixin');
        $openId = $request->get('openid', null);
        try {
            if ($oauthType !== 'weibo') {
                $oauthResponse = Socialite::driver($oauthType)->stateless()->user($token, $openId);
            } else {
                $oauthResponse = Socialite::driver($oauthType)->stateless()->user($token);
            }
        } catch (\Exception $e) {
            custom_logs('oauth_login', $e->getMessage());
            return response()->json(['message' => config('system.error_codes.oauth_login')['message'], 'error_code' => config('system.error_codes.oauth_login')['code']], 400);
        }
        if ($oauthType == 'weibo') {
            $oauthAttr['oauth_type_name'] = 'sina_weibo';
            $oauthAttr['openid'] = $oauthResponse->id;
            $oauthAttr['unionid'] = isset($oauthResponse->user['unionid']) ? $oauthResponse->user['unionid'] : null;
            $oauthAttr['avatar'] = $oauthResponse->avatar;
            $oauthAttr['oauth_nick_name'] = $oauthResponse->name ? $oauthResponse->name : $oauthResponse->nickname;
            //user datas to be store
            $userAttr['nick_name'] = isset($oauthResponse->nickname) ? $oauthResponse->nickname : $oauthResponse->name;
            // $userAttr['gender'] = $oauthResponse->user['gender'] == 'm' ? 'male' : 'female';
            // $userAttr['province'] = $oauthResponse->user['province'];
            // $userAttr['city'] = $oauthResponse->user['city'];
        } else {
             // oauth datas to be store
            $oauthAttr['oauth_type_name'] = 'wechat';
            $oauthAttr['openid'] = $oauthResponse->id;
            $oauthAttr['unionid'] = isset($oauthResponse->user['unionid']) ? $oauthResponse->user['unionid'] : null;
            $oauthAttr['avatar'] = $oauthResponse->avatar;
            $oauthAttr['oauth_nick_name'] = $oauthResponse->name ? $oauthResponse->name : $oauthResponse->nickname;
            //user datas to be store
            $userAttr['nick_name'] = isset($oauthResponse->nickname) ? $oauthResponse->nickname : $oauthResponse->name;
            // $userAttr['gender'] = $oauthResponse->user['sex'] == '0' ? 'female' : 'male';
            // $userAttr['province'] = $oauthResponse->user['province'];
            // $userAttr['city'] = $oauthResponse->user['city'];
        }
        $oauthAttr['oauth_access_token'] = $token;
        /**
         * check if user already in db
         */
        if ($oauthAttr['unionid'] === null) {
            $oauth = OAuth::firstOrNew(['openid' => $oauthAttr['openid']]);
        } else {
            $oauth = OAuth::firstOrNew(['unionid' => $oauthAttr['unionid']]);
        }

        if (!$oauth->exists) {
            $user = User::create($userAttr);
            $oauthAttr['user_id'] = $user->id;
            $oauthAttr['oauth_access_token'] = $token;
            OAuth::create($oauthAttr);
            $responseCode = 201;
        } else {
            $user = User::find($oauth->user_id);
            if (!empty(array_diff([
                                    'oauth_type_name' => $oauthType,
                                    'openid' => $oauth->openid,
                                    'unionid' => $oauth->unionid,
                                    'avatar' => $oauth->avatar,
                                    'oauth_nick_name' => $oauth->oauth_user_name
                                  ], $oauthAttr))) {
                foreach ($oauthAttr as $key => $value) {
                        $oauth->$key = $value;
                }
                $oauth->save();
            }
            $responseCode = 200;
        }
        $token = JWTAuth::fromUser($user);
        return response()->json(['data' => ['token' => $token]], $responseCode);
    }

}
