<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Style;
use App\Color;
use App\Occasion;
use Auth;

class ProductCategoryController extends Controller
{
    private $category;
    private $style;
    private $color;
    private $occasion;

    public function __construct(ProductCategory $category, Style $style, Color $color, Occasion $occasion)
    {
        $this->category = $category;
        $this->style = $style;
        $this->color = $color;
        $this->occasion = $occasion;
    }

    public function index()
    {
        $user = Auth::user()->get();
        $favorites = array();
        if (empty($user)) {
            $primary_city = 310000;
        } else {
            $primary_city = $user->primary_city;
        }

        $usage_categories = config('system.database.usage_categories');
        $sizes = config('system.database.sizes');
        $body_figures = config('system.database.body_figures');

        $categories = $this->category->all();
        $category_data = array();
        foreach ($categories as $category) {
            $category_data[$category->id] = [
                'name' => $category->name,
                'series' => $category->series,
                'image' => $category->image(),
                'count' => $category->products()->avaliable()->where('city', $primary_city)->groupBy('name_group_id')->get()->count(),
                'recommend' => $category->recommend,
                'order' => $category->order,
            ];
        }

        $occasions = $this->occasion->all();
        $occasion_data = array();
        foreach ($occasions as $occasion) {
            $occasion_data[$occasion->id] = [
                'name' => $occasion->name,
                'english_name' => $occasion->english_name,
                'image' => $occasion->image(),
                'recommend' => $occasion->recommend,
                'order' => $occasion->order,
                'intro' => $occasion->intro,
            ];
        }

        $style = $this->style->all()->lists('name', 'id');

        $color = $this->color->orderBy('order_sequence', 'asc')->select(['id', 'rgb'])->get();

        return response()->json([
            'data' => [
                'usage_categories' => (object)$usage_categories,
                'occasions' => (object)$occasion_data,
                'sizes' => (object)$sizes,
                'body_figures' => (object)$body_figures,
                'product_category' => (object)$category_data,
                'style' => $style,
                'color' => $color,
            ]
        ], 200);
    }

    public function show($categoryId)
    {
        $category = $this->category->find($categoryId);
        if (empty($category)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        return response()->json(['data' => ['detail' => $category->detail]], 200);
    }
}
