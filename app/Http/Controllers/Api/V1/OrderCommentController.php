<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Order;
use App\OrderComment;
use Auth;
use Illuminate\Http\Request;

class OrderCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderCommentRequest $request, OrderComment $orderComment, Order $order, $orderID)
    {
        $order = $order->findOrFail($orderID);
        if (Auth::user()->get()->id !== $order->user_id) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $inputs = $request->only('star','images', 'contents');
        $inputs['user_id'] = Auth::user()->get()->id;
        $inputs['order_id'] = $order->id;
        $comment = $orderComment->create($inputs);
        $orderComment->save();
        return response()->json(['data' => ['comment' => $comment->toArray()]], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
