<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests\Api\CouponRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Coupon;
use App\CouponConfig;
use Carbon\Carbon;
use Auth;
use Input;

class CouponController extends Controller
{
    private $user;
    private $coupon;
    private $coupon_config;

    public function __construct(User $user, Coupon $coupon, CouponConfig $coupon_config)
    {
        $this->user = $user;
        $this->coupon = $coupon;
        $this->coupon_config = $coupon_config;
    }

    public function index()
    {
        $coupons = Auth::user()->get()->coupons()->available()->get();
        $data = [];
        foreach ($coupons as &$coupon) {
            if (Carbon::now() > Carbon::parse($coupon->deadline)) {
                $coupon->status = 'expired';
                $coupon->save();
                continue;
            }
            $data[] = [
                'id' => $coupon->id,
                'name' => $coupon->config->name,
                'need' => $coupon->config->need,
                'cut' => $coupon->config->cut,
                'deadline' => $coupon->deadline,
                'created_at' => (string)$coupon->created_at,
            ];
        }

        return response()->json(['data' => ['coupons' => $data]], 200);
    }

    public function store(CouponRequest $request)
    {
        $inputs = $request->all();
        $referee = 0;
        $coupon_configs = $this->coupon_config->where('group', $inputs['group'])->get();
        if (empty($coupon_configs)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        //检查是否已经领过优惠券
        $flag = $this->coupon->where('cell_phone', $inputs['cell_phone'])
                             ->where('group', $inputs['group'])
                             ->first();
        if (!empty($flag)) {
            return response()->json([
                'message' => config('system.error_codes.already_get_coupon')['message'],
                'error_code' => config('system.error_codes.already_get_coupon')['code']
            ], 400);
        }

        //判断是否是新用户活动
        if ($inputs['group'] == config('system.new_group')) {
            $isExist = $this->user->where('cell_phone', $inputs['cell_phone'])->first();
            if ($isExist) {
                return response()->json([
                    'message' => config('system.error_codes.only_new_user')['message'],
                    'error_code' => config('system.error_codes.only_new_user')['code']
                ], 400);
            }
        }

        if (!empty($inputs['invite'])) {
            $invite = $this->user->where('invite_code', $inputs['invite'])->first();
            if (!empty($invite)) {
                $referee = $invite->id;
            }
        }
        $data = [];
        foreach ($coupon_configs as $coupon_config) {
            if ($coupon_config->amount > 0) {
                $coupon_config->amount--;
                $coupon_config->save();
                $data[] = $this->coupon->create([
                    'cell_phone' => $inputs['cell_phone'],
                    'config_id' => $coupon_config->id,
                    'deadline' => Carbon::now()->addDays($coupon_config->available_days)->toDateString(),
                    'group' => $coupon_config->group,
                    'referee' => $referee,
                ]);
            }
        }

        if (empty($data)) {
            return response()->json([
                'message' => config('system.error_codes.coupon_run_out')['message'],
                'error_code' => config('system.error_codes.coupon_run_out')['code']
            ], 404);
        }

        return response()->json(['data' => ['coupons' => $data]], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coupon = Auth::user()->get()->coupons()->find($id);
        if (empty($coupon)) {
             return response()->json([
                'message' => config('system.error_codes.invalid_user_data')['message'],
                'error_code' => config('system.error_codes.invalid_user_data')['code']
            ], 400);
        }

        if (Carbon::now() > Carbon::parse($coupon->deadline)) {
            $coupon->status = 'expired';
            $coupon->save();
        }
        $data = [
            'id' => $coupon->id,
            'name' => $coupon->config->name,
            'need' => $coupon->config->need,
            'cut' => $coupon->config->cut,
            'deadline' => $coupon->deadline,
            'status' => $coupon->status,
            'created_at' => (string)$coupon->created_at,
        ];

        return response()->json(['data' =>  $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CouponRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
