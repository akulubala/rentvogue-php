<?php

namespace App\Http\Controllers\Api\V1;

use App\HotWord;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UsersRequest;
use App\Order;
use App\Product;
use App\WordCounter;
use App\Favorite;
use Auth;
use Cache;
use DB;
use Illuminate\Http\Request;
use Input;

class ProductController extends Controller
{
    private $product;
    private $word_counter;
    private $favorite;
    private $hotword;
    private $order;

    public function __construct(Product $product, WordCounter $word_counter, Favorite $favorite, HotWord $hotword, Order $order)
    {
        $this->product = $product;
        $this->hotword = $hotword;
        $this->$order = $order;
        $this->word_counter = $word_counter;
        $this->favorite = $favorite;
    }

    public function index()
    {
        $user = Auth::user()->get();
        $favorites = array();
        if (empty($user)) {
            $primary_city = 310000;
        } else {
            $primary_city = $user->primary_city;
            foreach ($user->favorites()->get() as $favorite) {
                $favorites[] = $favorite->product_name_group_id;
            }
        }
        $products = $this->product->avaliable()->primaryCity($primary_city);

        $orderId = Input::get('order_id', null);
        if (!empty($orderId)) {
            if (!Auth::user()->check()) {
                return response()->json(['message' => config('system.error_codes.token_invalid')['message'], 'error_code' => config('system.error_codes.token_invalid')['code']], 401);
            }
            $orderId = Input::get('order_id');
            $order = Order::findOrFail($orderId);
            if ($order->order_status !== 3 || $order->user_id !== Auth::user()->get()->id) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
            }
            $products = $products->upLevelProducts($order);
        } else {
            if (!empty(Input::get('rent_start', null)) && !empty(Input::get('rent_end', null))) {
                $products = $products->rentRange();
            }

            if (!empty(Input::get('category', null))) {
                $products = $products->category(Input::get('category'));
            }
            if (!empty(Input::get('usage', null))) {
                $products = $products->usage(Input::get('usage'));
            }
            if (!empty(Input::get('size', null))) {
                $products = $products->size(Input::get('size'));
            }
            if (!empty(Input::get('color', null))) {
                $products = $products->color(Input::get('color'));
            }
            if (!empty(Input::get('style', null))) {
                $products = $products->style(Input::get('style'));
            }
            if (!empty(Input::get('occasion', null))) {
                $products = $products->occasion(Input::get('occasion'));
            }
            if (!empty(Input::get('body_figure', null))) {
                $products = $products->body(Input::get('body_figure'));
            }
            if (!empty(Input::get('start_price', null))) {
                $products = $products->startPrice(Input::get('start_price'));
            }
            if (!empty(Input::get('end_price', null))) {
                $products = $products->endPrice(Input::get('end_price'));
            }
            if (!empty(Input::get('prices', null))) {
                $range = config('system.database.prices')[Input::get('prices')];
                if (!empty($range)) {
                    $products = $products->pricesRange($range);
                }
            }

            if (!empty(Input::get('name', null))) {
                $search = Input::get('name');
                $products = $products->name($search);
                //统计热词
                $word = $this->word_counter->where('word', $search)->first();
                if (!$word) {
                    $this->word_counter->create(['word' => $search]);
                } else {
                    $word->count++;
                    $word->save();
                }
            }
        }

        $products = $products->groupBy('name_group_id')->orderBy('updated_at', 'desc')
                    ->paginate(6)
                    ->appends(\Request::except('page'));

        $data = array();

        foreach ($products as $product) {
            $data[] = [
                'id' => $product->name_group_id,
                'img' => $product->defaultImage(),
                'name' => $product->name,
                'intro' => $product->intro,
                'price' => $product->four_day_rent_price,
                'market_price' => $product->market_price,
                'deposit' => $product->deposit,
                'is_favorite' => in_array($product->name_group_id, $favorites) ? 1 : 0,
                'link' => route('api.v1.product.show', $product->name_group_id) . ($orderId ? '?order_id=' . $orderId : ''),
            ];
        }

        return response()->json(
            ['data' => [
                'total' => $products->total(),
                'per_page' => $products->perPage(),
                'current_page' => $products->currentPage(),
                'last_page' => $products->lastPage(),
                'next_page_url' => $products->nextPageUrl(),
                'prev_page_url' => $products->previousPageUrl(),
                'data' => $data
            ]], 200);
    }

    public function show($name_group_id)
    {
        $user = Auth::user()->get();
        $favorites = array();
        if (empty($user)) {
        } else {
            foreach ($user->favorites()->get() as $favorite) {
                $favorites[] = $favorite->product_name_group_id;
            }
        }

        $product = $this->product->where('name_group_id', $name_group_id)->first();
        if (empty($product)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }

        //找出同款商品
        $others = $this->product->nameGroup($product->name_group_id)->get();

        $other_data = array();
        $exist = array();
        foreach ($others as $other) {
            $images = json_decode($other->images, true);
            foreach ($images as &$image) {
                $image = [
                    'img' => trim($image, '/'),
                    'thumb' => trim($image, '/'),
                ];
            }
            $exist_data = [
                'size' => $other->size,
                'color' => $other->color_id,
            ];
            if (in_array($exist_data, $exist)) {
                continue;
            } else {
                $exist[] = $exist_data;
            }
            $other_data[] = [
                'property_group_id' => $other->property_group_id,
                'size' => config('system.database.sizes')[$other->size],
                'color' => $other->color->rgb,
                'link' => route('api.v1.product.show', $other->name_group_id),
                'images' => $images,
                'unavaliable' => get_unavaliable_rent_dates($other->property_group_id),
            ];
        }

        $images = json_decode($product->images, true);
        foreach ($images as &$image) {
            $image = [
                'img' => trim($image, '/'),
                'thumb' => trim($image, '/'),
            ];
        }
        $product_data = $product->toArray();
        $product_data['images'] = $images;
        $product_data['market_price'] = $product->market_price;
        $product_data['product_category'] = $product->brand->name;
        $product_data['style'] = $product->style->name;
        $product_data['color'] = $product->color->rgb;
        $product_data['suit_occasion'] = $product->occasion->name;
        $product_data['usage_category'] = config('system.database.usage_categories')[$product->usage_category];
        $product_data['size'] = config('system.database.sizes')[$product->size];
        $product_data['body_figure'] = config('system.database.body_figures')[$product->body_figure];
        $product_data['is_favorite'] = in_array($product->name_group_id, $favorites) ? 1 : 0;
        $product_data['unavaliable'] = get_unavaliable_rent_dates($product->property_group_id);
        if ($product->supplier) {
            $product_data['supplier'] = $product->supplier->name;
            $product_data['supplier_phone'] = $product->supplier->phone;
            $product_data['supplier_address'] = $product->supplier->address;
            $product_data['supplier_detail'] = $product->supplier->detail;
        } else {
            $product_data['supplier'] = '';
            $product_data['supplier_phone'] = '';
            $product_data['supplier_address'] = '';
            $product_data['supplier_detail'] = '';
        }
        $product_data['other_data'] = $other_data;

        return response()->json([
            'data' => $product_data
        ], 200);
    }

    public function recommend()
    {
        $products = [];
        $output = [
            'data' => [],
        ];

        if (empty(Input::get('name_group_id', null))) {
            $recommend_list = Cache::get('recommend_list');
            if (empty($recommend_list)) {
                $id_list = DB::table('orders')
                            ->select('product_name_group_id', DB::raw('COUNT(*) as total'))
                            ->groupBy('product_name_group_id')
                            ->orderBy('total', 'desc')
                            ->limit(200)
                            ->lists('product_name_group_id');
                $recommend_list = implode(',', $id_list);
                Cache::put('recommend_list', $recommend_list, 10);
            } else {
                $id_list = explode(',', $recommend_list);
            }
            $products = $this->product->avaliable()->whereIn('name_group_id', $id_list);
            $products = $products->groupBy('name_group_id')
                            ->orderByRaw(DB::raw("FIELD(name_group_id, $recommend_list)"))
                            ->paginate(6)
                            ->appends(\Request::except('page'));
            $output['data'] = [
                'total' => $products->total(),
                'per_page' => $products->perPage(),
                'current_page' => $products->currentPage(),
                'last_page' => $products->lastPage(),
                'next_page_url' => $products->nextPageUrl(),
                'prev_page_url' => $products->previousPageUrl(),
            ];
        } else {
            $extract_product = $this->product->avaliable()->where('name_group_id', Input::get('name_group_id'))->first();
            if ($extract_product) {
                $products = $this->product->where(function ($query) use ($extract_product) {
                    $query->where('style_id', $extract_product->style_id)
                        ->orWhere('product_category_id', $extract_product->product_category_id);
                })->groupBy('name_group_id')->orderByRaw('RAND()')->limit(12)->get();
            } else {
                return response()->json([
                    'message' => config('system.error_codes.data_not_exists')['message'],
                    'error_code' => config('system.error_codes.data_not_exists')['code']
                ], 404);
            }
        }

        $data = array();

        foreach ($products as $product) {
            $data[] = [
                'id' => $product->name_group_id,
                'img' => $product->defaultImage(),
                'name' => $product->name,
                'price' => $product->four_day_rent_price,
                'market_price' => $product->market_price,
                'deposit' => $product->deposit,
                'link' => route('api.v1.product.show', $product->name_group_id),
            ];
        }

        $output['data']['data'] = $data;

        return response()->json($output, 200);

    }

    public function favorite($group_id)
    {
        //验证
        $products = $this->product->where('name_group_id', $group_id)->first();
        if (empty($products)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        $favorites = Auth::user()->get()->favorites();
        $favorite = $favorites->where('product_name_group_id', $group_id)
                              ->first();

        if (empty($favorite)) {
            $exist = $favorites->withTrashed()
                               ->where('product_name_group_id', $group_id)
                               ->first();
            if (!empty($exist)) {
                $exist->restore();
            } else {
                $favorites->create([
                    'product_name_group_id' => $group_id,
                ]);
            }
        } else {
            $favorite->delete();
        }
        return response()->json(null, 200);
    }

}
