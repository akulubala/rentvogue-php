<?php

namespace App\Http\Controllers\Api\V1;

use App\Address;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Api\UserOrderRequest;
use App\Order;
use App\Product;
use App\UnavaliableRentDate;
use App\User;
use App\Supplier;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Input;

class UserOrderController extends Controller
{
    private $product;
    private $user;
    private $order;
    private $address;

    public function __construct(Product $product, User $user, Order $order, Address $address, Supplier $supplier)
    {
        $this->product = $product;
        $this->user = $user;
        $this->order = $order;
        $this->address = $address;
        $this->supplier = $supplier;
    }

    public function index($userId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }

        $orderStatus = Input::get('order_status', null);
        $orders = [];
        if ($orderStatus) {
            if (!in_array((int)$orderStatus - 1, array_keys(config('system.database.order_status')))) {
                return response()->json([], 200);
            }
        } else {
            $orderStatus = null;
        }
        $orders = $this->order->where(function($query) use ($userId, $orderStatus) {
                if (!$orderStatus) {
                    $query->where('user_id', '=', $userId);
                } else {
                    $query->where('user_id', '=', $userId)
                          ->whereRaw("order_status={$orderStatus}");
                }
        })->get();
        $results = [];
        if (!$orders->isEmpty()) {
            $expressPhone = config('system.database.express_contact_phone');
            $expressCompany = config('system.database.express_company');
            foreach ($orders as $key => $order) {
                $postDatas = $this->getExpressDatas($order['express_number']);
                $order['payment_method'] = str_contains($order->paid_method, 'alipay') ? '支付宝' : (str_contains($order->paid_method, 'wx') ? '微信' : '银联');
                if (!empty($order->deposit_pingxx_ch)) {
                    $order['deposit_method'] = str_contains($order->deposit_method, 'alipay') ? '支付宝' : (str_contains($order->deposit_method, 'wx') ? '微信' : '银联');
                } else {
                    $order['deposit_method'] = $order['payment_method'];
                }
                $order['default_image'] = $order->product->defaultImage();
                $order['actual_deposit'] = $order->user->verify_status == 3 ? $order->product->deposit * 0.8 : $order->product->deposit;
                $order['product_rgb'] = $order->product->color->rgb;
                $order['coupon_name'] = $order->coupon_id ? $order->coupon->config->name : null;
                $order['coupon_price'] =  $order->coupon_id ? $order->coupon->config->cut : null;
                $expressCompanyCode = $order->express_company;
                $order['express_company'] = $expressCompanyCode ? $expressCompany[$expressCompanyCode] : '';
                $order['express_contact_phone'] = $expressCompanyCode ? $expressPhone[$expressCompanyCode] : '';
                $order['express_details'] = $this->getExpressDatas($order->express_number);
                $order['express_details'] = $postDatas;
                $order['supplier'] = $order->product->supplier;
                $results[$key] = $order;
            }
        }

        return response()->json(['data' => $results], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserOrderRequest $request, $userId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        } else {
            $user = $this->user->find($userId);
        }

        $inputs = $request->only(['property_group_id', 'rent_days', 'rent_start_date', 'rent_end_date', 'address_id', 'exchange_order_id', 'coupon_id', 'remarks', 'deposit', 'supplier_id']);
   
        //判断是否传入送货地址     
        $address = null;
        if (isset($inputs['address_id'])) {
            $address = $this->address->find($inputs['address_id']);
            if (!$address) {
                return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
            }
        }

        //判断是否传入供应商
        $supplier = null;
        if (isset($inputs['supplier_id'])) {
            $supplier = $this->supplier->find($inputs['supplier_id']);
            if (!$supplier) {
                return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
            }
        }

        //送货地址和供应商必须存在一个
        if ($address === null && $supplier === null) {
            return response()->json(['message' => config('system.error_codes.must_have_address_or_supplier')['message'], 'error_code' => config('system.error_codes.must_have_address_or_supplier')['code']], 400);
        }

        if (!empty($inputs['exchange_order_id'])) {
            $exchangeOrder = $this->order->find($inputs['exchange_order_id']);
            if ($exchangeOrder->order_status !== 3 || $exchangeOrder->user_id !== (int)$userId) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
            }

        }
        /**
         * 换货的时候时间还是可以选
         */
        $unRentStartDate = $inputs['rent_start_date'];
        $unRentEndDate = Carbon::parse($inputs['rent_end_date'])->addDays(4)->toDateString();

        $propertyGroupId = $inputs['property_group_id'];
        $productIds = Product::where('property_group_id', $propertyGroupId)
                              ->where('is_available', 'Y')
                              ->lists('id')->toArray();

        if (empty($productIds)) {
            return response()->json(['message' => config('system.error_codes.no_products')['message'], 'error_code' => config('system.error_codes.no_products')['code']], 400);
        }

        /**
         * 检查产品是否可租
         */
        $unrentable = get_unavaliable_rent_dates($propertyGroupId);
        if ($inputs['rent_days'] == 4) {
            $unrentable = $unrentable['four_days'];
        } elseif ($inputs['rent_days'] == 8) {
            $unrentable = $unrentable['eight_days'];
        }
        if (!empty($unrentable)) {
            $validRentDate = true;
            foreach ($unrentable as $u) {
                //只需要判断开始日期跟不可租是否有交集
                if (!empty(date_intersects($unRentStartDate, $unRentStartDate, $u['start_date'], $u['end_date']))) {
                    $validRentDate = false;
                    break;
                }
            }
            if (!$validRentDate) {
                return response()->json(['message' => config('system.error_codes.invalid_rent_dates')['message'], 'error_code' => config('system.error_codes.invalid_rent_dates')['code']], 400);
            }
        }
        /**
         * 从可租产品中挑选一个
         */
        if (!empty($unrentable)) {
            $rentedProducts =  UnavaliableRentDate::whereIn('product_id', $productIds)
                                               ->select(['product_id', 'start_date', 'end_date'])
                                               ->get()
                                               ->toArray();
        }
        /**
         * 选取一个可租产品,排除和当前出租时间有交集的产品即可
         */
        $choosedProductId = null;
        if (!empty($rentedProducts)) {
            $exceptProducts = [];
            foreach ($rentedProducts as $rentedDatas) {
                $hasIntersects = date_intersects($unRentStartDate, $unRentEndDate, $rentedDatas['start_date'], $rentedDatas['end_date']);
                if (!empty($hasIntersects)) {
                    $exceptProducts[] = $rentedDatas['product_id'];
                }
            }
            foreach ($productIds as $id) {
                if (!in_array($id, $exceptProducts)) {
                        $choosedProductId = $id;
                        break;
                }
            }
        } else {
            $choosedProductId = $productIds[array_rand($productIds)];
        }

        $product = $this->product->findOrFail($choosedProductId);

        //判断是否延后支付押金同时是否是三天内
        if (!empty($inputs['deposit'])) {
            $days = Carbon::now()->diffInDays(Carbon::parse($unRentStartDate), false);
            if ($days <= 3) {
                return response()->json(['message' => config('system.error_codes.must_pay_deposit')['message'], 'error_code' => config('system.error_codes.must_pay_deposit')['code']], 400);
            }
        }

        //计算支付金额
        $payPrice = ($inputs['rent_days'] == '4' ? $product->four_day_rent_price : $product->eight_day_rent_price);
        $deposit = $product->deposit;
        if ($user->verify_status == 3) {
            $deposit = $deposit * 0.8;
        }
        if (empty($inputs['deposit'])) {
            $amount = $payPrice + $deposit;
            $paid_deposit = 'Y';
        } else {
            $amount = $payPrice;
            $paid_deposit = 'N';
        }
        if ($inputs['coupon_id']) {
            $coupon = Auth::user()->get()
                                  ->coupons()
                                  ->where('id', '=', $inputs['coupon_id'])
                                  ->where('status', '=', 'available')
                                  ->first();
            if ($coupon) {
                if ($coupon->status == 'available') {
                    if ($payPrice >= $coupon->config->need) {
                        $amount = $amount - $coupon->config->cut;
                    } else {
                        $amount = $amount - $payPrice;
                    }
                    $coupon->status = 'used';
                    $coupon->save();
                    $couponId = $coupon->id;
                }
            }
        }

        $order = $this->order->create([
            'parent_order_id' => isset($inputs['exchange_order_id']) ? $inputs['exchange_order_id'] : null,
            'product_name_group_id' => $product->name_group_id,
            'product_id' => $choosedProductId,
            'user_id' => $userId,
            'contact_user_name' => $address ? $address->contact_user_name : $supplier->name,
            'address' => $address ? config('system.database.city_code')[$address->city]['name'] . ',' . config('system.database.city_code')[$address->city]['district'][$address->district] . ',' . $address->details : $supplier->address,
            'phone' => $address ? $address->contact_user_cell_phone : $supplier->phone,
            'rent_days' => $inputs['rent_days'],
            'rent_start_date' => $inputs['rent_start_date'],
            'rent_end_date' => $inputs['rent_end_date'],
            'remarks' => $inputs['remarks'],
            'rent_price' => $amount,
            'order_number' => unique_string_number(),
            'order_status' => isset($inputs['exchange_order_id']) ? 'paid_not_deliveried' : 'not_paid',//换货默认是支付状态
            'coupon_id' => isset($couponId) ? $couponId : null,
            'paid_deposit' => $paid_deposit,
        ]);

        //如果是上门自提需要设置快递公司为self
        if ($supplier) {
            $order->express_company = 'self';
            $order->save();
        }

        //原始换货订单更改状态及插入新订单号
        if (isset($exchangeOrder)) {
            $exchangeOrder->exchange_to_order_id = $order->id;
            $exchangeOrder->order_status = 'exchange_order_finish';
            $exchangeOrder->save();
        }

        $result = ['data' => ['order' => $order->toArray()]];

        return response()->json($result, 200);
    }

    public function pay(UserOrderRequest $request, $userId, $orderId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $order = $this->order->find($orderId);
        if (!$order) {
            return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
        }
        if ($order->user_id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $inputs = $request->only(['paid_method', 'call_back']);
        //每次支付都生成一个新的支付对象订单号
        $order_number = $order->order_number . mt_rand(10000, 99999);
        try {
            if (isset($inputs['call_back'])) {
                $pingxxCharge = json_decode(pingxx_charge($inputs['paid_method'], $order_number, $order->rent_price, $order->product->name, null, $inputs['call_back']), true);
            } else {
                $pingxxCharge = json_decode(pingxx_charge($inputs['paid_method'], $order_number, $order->rent_price, $order->product->name), true);
            }
        } catch (Exception $e) {
            return response()->json(['message' => config('system.error_codes.payment_failed')['message'], 'error_code' => config('system.error_codes.payment_failed')['code']], 500);
        }

        $order->paid_method = $inputs['paid_method'];
        $order->pingxx_ch = json_encode($pingxxCharge, true);
        $order->save();

        $result = ['data' => ['ch' => $pingxxCharge, 'order' => $order->toArray()]];

        return response()->json($result, 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userId, $orderId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $order = $this->order->find($orderId);
        if (!$order) {
            return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
        }
        if ($order->user_id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $expressPhone = config('system.database.express_contact_phone');
        $expressCompany = config('system.database.express_company');
        $postDatas = $this->getExpressDatas($order['express_number']);
        $order['payment_method'] = str_contains($order->paid_method, 'alipay') ? '支付宝' : (str_contains($order->paid_method, 'wx') ? '微信' : '银联');
        if (!empty($order->deposit_pingxx_ch)) {
            $order['deposit_method'] = str_contains($order->deposit_method, 'alipay') ? '支付宝' : (str_contains($order->deposit_method, 'wx') ? '微信' : '银联');
        } else {
            $order['deposit_method'] = $order['payment_method'];
        }
        $order['default_image'] = $order->product->defaultImage();
        $order['actual_deposit'] = $order->user->verify_status == 3 ? $order->product->deposit * 0.8 : $order->product->deposit;
        $order['product_rgb'] = $order->product->color->rgb;
        $order['coupon_name'] = $order->coupon_id ? $order->coupon->config->name : null;
        $order['coupon_price'] =  $order->coupon_id ? $order->coupon->config->cut : null;
        $order['express_number'] = $order->express_number;
        $expressCompanyCode = $order->express_company;
        $order['express_company'] = $expressCompanyCode ? $expressCompany[$expressCompanyCode] : '';
        $order['express_contact_phone'] = $expressCompanyCode ? $expressPhone[$expressCompanyCode] : '';
        $order['express_details'] = $postDatas;
        $order['supplier'] = $order->product->supplier;
        return response()->json(['data' => $order], 200);
    }

    public function update(UserOrderRequest $request, $userId, $orderId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }

        $order = $this->order->find($orderId);
        if (!$order) {
            return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
        }
        if ($order->user_id !== (int)$userId) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $orderStatus = $request->get('order_status');

        //取消订单操作
        if ($orderStatus == 10) {
            $hours = Carbon::now()->diffInHours(Carbon::parse($order->rent_start_date), false);
            if ($hours < 0) {
                return response()->json(['message' => config('system.error_codes.cancel_order_time_invalid')['message'], 'error_code' => config('system.error_codes.cancel_order_time_invalid')['code']], 400);
            }
            if ($order->order_status !== 2) {
                if ($order->order_status === 1) {
                    $order->order_status = 'order_canceled';
                    $order->save();
                } else {
                    return response()->json(['message' => config('system.error_codes.cancel_order_status_invalid')['message'], 'error_code' => config('system.error_codes.cancel_order_status_invalid')['code']], 400);
                }
            } else {
                $order->order_status = 'cancel_apply';
                $order->cancel_apply_date = date('Y-m-d H:i:s', time());
                $order->save();
            }
        }

        //签收操作
        if ($orderStatus == 5) {
            $order->order_status = 'recieved';
            $order->save();
        }

        return response()->json(['data' => $order->toArray()], 200);
    }

    public function deposit(UserOrderRequest $request, $userId, $orderId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $order = $this->order->find($orderId);
        if (!$order) {
            return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
        }
        if ($order->user_id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $inputs = $request->only(['paid_method', 'call_back']);
        $deposit = $order->user->verify_status == 3 ? $order->product->deposit * 0.8 : $order->product->deposit;
        //生成与原订单号相关的押金订单号，用于PINGXX识别不同订单
        $deposit_order_number = $order->order_number . mt_rand(10000, 99999) . 'D';
        try {
            if (isset($inputs['call_back'])) {
                $pingxxCharge = json_decode(pingxx_charge($inputs['paid_method'], $deposit_order_number, $deposit, $order->product->name, config('services.pingxx.deposit_subject'), $inputs['call_back']), true);
            } else {
                $pingxxCharge = json_decode(pingxx_charge($inputs['paid_method'], $deposit_order_number, $deposit, $order->product->name, config('services.pingxx.deposit_subject')), true);
            }
        } catch (Exception $e) {
            return response()->json(['message' => config('system.error_codes.payment_failed')['message'], 'error_code' => config('system.error_codes.payment_failed')['code']], 500);
        }

        $order->paid_deposit = 'Y';
        $order->deposit_method = $inputs['paid_method'];
        $order->deposit_pingxx_ch = json_encode($pingxxCharge, true);
        
        $order->rent_price += $deposit;
        $order->save();
        $result = ['data' => ['ch' => $pingxxCharge, 'order' => $order->toArray()]];

        return response()->json($result, 200);
    }

    public function getExpressByNumber($expressNumber)
    {
        $expressInfos = $this->getExpressDatas($expressNumber);
        $ch = new \Curl\Curl();
        $response = $ch->post(config('services.express.jump_url'), $expressInfos);

        return response()->json(['data' => json_decode($response, true)], 200);
    }

    private function getExpressDatas($expressNumber)
    {
        $requestData= "{\"OrderCode\":\"\",\"ShipperCode\":\""."SF"."\",\"LogisticCode\":\"".$expressNumber."\"}";
        $datas = array(
            'EBusinessID' => config('services.express.b_id'),
            'RequestType' => '1002',
            'RequestData' => urlencode($requestData),
            'DataType' => '2',
        );
       $datas['DataSign'] = urlencode(base64_encode(md5($requestData.config('services.express.key'))));
       return $datas;
    }

    public function getOrderByOrderNo($order_number)
    {
        $order = $this->order->where('order_number', $order_number)->first();
        if ($order) {
            if (Auth::user()->get()->id !== (int)$order->user_id) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
            }
            $output = [
                'order_id' => $order->id,
                'supplier' => $order->product->supplier->toArray(),
            ];
            return response()->json(['data' => $output], 200);
        } else {
            return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
        }
    }

    public function destroy($userId, $orderId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
        }
        $order = $this->order->find($orderId);
        if (!$order) {
            return response()->json(['message' => config('system.error_codes.data_not_exists')['message'], 'error_code' => config('system.error_codes.data_not_exists')['code']], 404);
        } else {
            $order->delete();
            return response()->json(['data' => []], 200);
        }
    }
}
