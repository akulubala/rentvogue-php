<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Http\Request;

class UserSmsController extends Controller
{

    public function index($userId, User $user)
    {
        if ($userId) {
            if (Auth::user()->get()->id !== (int)$userId) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
            }
        } else {
            $userId = Auth::user()->get()->id;
        }
        $sms = $user->find($userId)->sms()->paginate(10);
        return response()->json(
            ['data' => [
                'total' => $sms->total(),
                'per_page' => $sms->perPage(),
                'current_page' => $sms->currentPage(),
                'last_page' => $sms->lastPage(),
                'next_page_url' => $sms->nextPageUrl(),
                'prev_page_url' => $sms->previousPageUrl(),
                'data' => $sms->items()
            ]], 200);
    }

    public function destroy($userId, $smsId, User $user)
    {
        if ($userId) {
            if (Auth::user()->get()->id !== (int)$userId) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
            }
        } else {
            $userId = Auth::user()->get()->id;
        }
        $sms = $user->find($userId)->sms()->where('id', '=', $smsId)->delete();
        return response()->json((object)[], 200);
    }
}
