<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserFavoriteController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
             return response()->json([
                'message' => config('system.error_codes.invalid_user_data')['message'],
                'error_code' => config('system.error_codes.invalid_user_data')['code']
            ], 400);
        }

        $favorites = $this->user->find($userId)->favorites()->orderBy('updated_at', 'desc')->paginate(6);
        $data = $favorites->toArray();
        $product_data = [];
        foreach ($favorites as $favorite) {
            $product_data[] = [
                'id' => $favorite->product_name_group_id,
                'img' => $favorite->product->defaultImage(),
                'name' => $favorite->product->name,
                'price' => $favorite->product->four_day_rent_price,
                'market_price' => $favorite->product->market_price,
                'deposit' => $favorite->product->deposit,
                'link' => route('api.v1.product.show', $favorite->product_name_group_id),
                'created_at' => (string)$favorite->created_at,
            ];
        }
        $data['data'] = $product_data;

        return response()->json([
            'data' => (object)$data,
        ], 200);
    }

}
