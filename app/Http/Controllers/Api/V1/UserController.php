<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UsersRequest;
use App\OAuth;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{
    private $user;
    private $oauth;

    public function __construct(User $user, OAuth $oauth)
    {
        $this->user = $user;
        $this->oauth = $oauth;
    }

    public function update($id, UsersRequest $request)
    {
        if ($id) {
            if (Auth::user()->get()->id !== (int)$id) {
                return response()->json(['message' => config('system.error_codes.invalid_user_data')['message'], 'error_code' => config('system.error_codes.invalid_user_data')['code']], 400);
            }
        }
        $inputs = $request->except('cell_phone_verify_code',
                                   'is_active',
                                   'id_verify_passed',
                                   'old_password',
                                   'password_confirmation',
                                   'openid',
                                   'access_token',
                                   'oauth_type');
        /**
         * 上传身份证状态
         */
        if (isset($inputs['name']) && isset($inputs['id_number']) && isset($inputs['id_card_frontend']) && isset($inputs['id_card_backend'])) {
            $inputs['verify_status'] = 1;
        }

        if (!$id) {
            $user = $this->user->where('cell_phone', '=', $inputs['cell_phone'])->first();
        } else {
            $user = $this->user->findOrFail($id);
            if ($request->has('openid') && $request->has('access_token') && $request->has('oauth_type')) {
                $oauthType = $request->get('oauth_type') === 'weibo' ? 'sina_weibo' : 'wechat';

                $exists = $this->oauth->where('openid', '=', $request->get('openid'))
                            ->where('oauth_type_name', '=', $oauthType)
                            ->first();
                if ($exists) {
                    $existUserId = $exists->user_id;
                    $exists->user_id = $user->id;
                    $exists->save();
                } else {
                    try {
                        $oauthDatas = $this->bindOAuth($request->get('access_token'), $request->get('oauth_type'), $request->get('openid'));
                    } catch (\Exception $e) {
                        return response()->json(['message' => config('system.error_codes.oauth_login')['message'], 'error_code' => config('system.error_codes.oauth_login')['code']], 400);
                    }
                    $oauth = $user->oauths()->where('oauth_type_name', '=', $oauthType)->first();
                    if ($oauth !== null) {
                        foreach ($oauthDatas['oauth_datas'] as $key => $value) {
                            $oauth->$key = $value;
                        }
                        $oauth->user_id = $user->id;
                        $oauth->save();
                    } else {
                        foreach ($oauthDatas['oauth_datas'] as $key => $value) {
                            $this->oauth->$key = $value;
                        }
                        $this->oauth->user_id = $user->id;
                        $this->oauth->save();
                    }
                    foreach ($oauthDatas['user_datas'] as $key => $value) {
                        if (!$user->$key) {
                            $inputs[$key] = $value;
                        }
                    }
                }
            }
        }

        if (!empty($inputs)) {
            foreach ($inputs as $key => $value) {
                if ($key === 'password' || $key === 'bind_password') {
                    $user->password = Hash::make($value);
                } else {
                    $user->$key = $value;
                }
            }
            $user->save();
        }
        return response()->json(['data' => $user], 200);
    }

    public function show($id = null)
    {
        try {
            if (!$id) {
                $id = Auth::user()->get()->id;
            }
            $user = $this->user->findOrFail($id);
        } catch (Exception $e) {
            return response()->json(['message' => config('system.error_codes.user_not_exists')['message'], 'error_code' => config('system.error_codes.user_not_exists')['code']], 404);
        }

        if (empty($user->invite_code)) {
            $user->invite_code = md5(config('system.hash_code') . $user->id);
            $user->save();
        }
        $user_data = $user->toArray();
        $user_data['invite_users'] = [];
        foreach ($user->invite_users()->get() as $invite) {
            $user_data['invite_users'][] = [
                'id' => $invite->id,
                'name' => $invite->nick_name,
                'avatar' => $invite->avatar,
            ];
        }

        return response()->json(['data' => $user_data], 200);
    }

    private function bindOAuth($accessToken, $oauthType, $openId = null)
    {
        $oauthType = $oauthType === 'weibo' ? 'weibo' : 'weixin-web';
        try {
            if ($oauthType !== 'weibo') {
                $oauthResponse = Socialite::driver($oauthType)->stateless()->user($accessToken, $openId);
            } else {
                $oauthResponse = Socialite::driver($oauthType)->stateless()->user($accessToken);
            }
        } catch (\Exception $e) {
            custom_logs('oauth_login', $e->getMessage());
            return response()->json(['message' => config('system.error_codes.oauth_login')['message'], 'error_code' => config('system.error_codes.oauth_login')['code']], 400);
        }
        if ($oauthType == 'weibo') {
            $oauthAttr['oauth_type_name'] = 'sina_weibo';
            $oauthAttr['openid'] = $oauthResponse->id;
            $oauthAttr['unionid'] = isset($oauthResponse->user['unionid']) ? $oauthResponse->user['unionid'] : null;
            $oauthAttr['avatar'] = $oauthResponse->avatar;
            $oauthAttr['oauth_nick_name'] = $oauthResponse->name ? $oauthResponse->name : $oauthResponse->nickname;
            //user datas to be store
            $userAttr['nick_name'] = isset($oauthResponse->nickname) ? $oauthResponse->nickname : $oauthResponse->name;
            // $userAttr['gender'] = $oauthResponse->user['gender'] == 'm' ? 'male' : 'female';
            // $userAttr['province'] = $oauthResponse->user['province'];
            // $userAttr['city'] = $oauthResponse->user['city'];
        } else {
             // oauth datas to be store
            $oauthAttr['oauth_type_name'] = 'wechat';
            $oauthAttr['openid'] = $oauthResponse->id;
            $oauthAttr['unionid'] = isset($oauthResponse->user['unionid']) ? $oauthResponse->user['unionid'] : null;
            $oauthAttr['avatar'] = $oauthResponse->avatar;
            $oauthAttr['oauth_nick_name'] = $oauthResponse->name ? $oauthResponse->name : $oauthResponse->nickname;
            //user datas to be store
            $userAttr['nick_name'] = isset($oauthResponse->nickname) ? $oauthResponse->nickname : $oauthResponse->name;
            // $userAttr['gender'] = $oauthResponse->user['sex'] == '0' ? 'female' : 'male';
            // $userAttr['province'] = $oauthResponse->user['province'];
            // $userAttr['city'] = $oauthResponse->user['city'];
        }
        $oauthAttr['oauth_access_token'] = $accessToken;

        return ['user_datas'=> $userAttr, 'oauth_datas' => $oauthAttr];
    }
}
