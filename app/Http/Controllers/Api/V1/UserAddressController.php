<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests\Api\AddressRequest;
use App\Http\Controllers\Controller;
use App\Address;
use App\User;
use Auth;

class UserAddressController extends Controller
{
    private $user;
    private $address;

    public function __construct(User $user, Address $address)
    {
        $this->user = $user;
        $this->address = $address;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
             return response()->json([
                'message' => config('system.error_codes.invalid_user_data')['message'], 
                'error_code' => config('system.error_codes.invalid_user_data')['code']
            ], 400);
        }
        $addresses = $this->user->find($userId)->addresses()->get();
        $data = [];
        foreach ($addresses as $address) {
            $address_data = $address->toArray();
            $address_data['city_name'] = config('system.database.city_code')[$address->city]['name'];
            $address_data['district_name'] = config('system.database.city_code')[$address->city]['district'][$address->district];
            $data[] = $address_data;
        }

        return response()->json(['data' => ['addresses' => $data]], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($userId, AddressRequest $request)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
             return response()->json([
                'message' => config('system.error_codes.invalid_user_data')['message'], 
                'error_code' => config('system.error_codes.invalid_user_data')['code']
            ], 400);
        }

        $inputs = $request->all();
        $inputs['user_id'] = $userId;
        $address = $this->address->create($inputs);
        if ($this->address->count() === 1) {
            $address->is_default = 1;
            $address->save();
        } else {
            if ($address->is_default == 1) {
                $addresses = $this->user->find($userId)->addresses()->where('id', '!=', $address->id)->get();
                foreach ($addresses as $value) {
                    $value->is_default = 0;
                    $value->save();
                }
            }
        }

        return response()->json(['data' => $address], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userId, $addressId)
    {
        $address = $this->address->whereUserIdAndId($userId, $addressId)->first();
        if (empty($address)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'], 
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        $data = $address->toArray();
        $data['city_name'] = config('system.database.city_code')[$address->city]['name'];
        $data['district_name'] = config('system.database.city_code')[$address->city]['district'][$address->district];

        return response()->json(['data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($userId, $addressId, AddressRequest $request)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
             return response()->json([
                'message' => config('system.error_codes.invalid_user_data')['message'], 
                'error_code' => config('system.error_codes.invalid_user_data')['code']
            ], 400);
        }

        $address = $this->address->whereUserIdAndId($userId, $addressId)->first();
        if (empty($address)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'], 
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }

        $inputs = $request->all();
        foreach ($inputs as $key => $value) {
            $address->$key = $value;
        }
        $address->save();

        if ($address->is_default == 1) {
            $addresses = $this->user->find($userId)->addresses()->where('id', '!=', $address->id)->get();
            foreach ($addresses as $value) {
                $value->is_default = 0;
                $value->save();
            }
        }

        return response()->json(['data' => $address], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId, $addressId)
    {
        if (Auth::user()->get()->id !== (int)$userId) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'], 
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }

        $address = $this->address->whereUserIdAndId($userId, $addressId)->first();
        if (empty($address)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'], 
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        $address->delete();

        if ($address->is_default == 1) {
            $addresses = $this->user->find($userId)->addresses()->where('id', '!=', $address->id)->first();
            if (!empty($addresses)) {
                $addresses->is_default = 1;
                $addresses->save();
            }
        }

        return response()->json(null, 200);
    }

    public function getCity()
    {
        $citys = config('system.database.city_code');
        return response()->json(['data' => $citys], 200);
    }

    public function getDistrict($cityId)
    {
        $citys = config('system.database.city_code');
        if (empty($citys[$cityId])) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'], 
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        } else {
            return response()->json(['data' => $citys[$cityId]['district']], 200);
        }
    }

}
