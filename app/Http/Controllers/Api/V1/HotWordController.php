<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\HotWord;

class HotWordController extends Controller
{
    private $hotword;

    public function __construct(HotWord $hotword)
    {
        $this->hotword = $hotword;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => ['hotwords' => $this->hotword->all()]], 200);
    }

}
