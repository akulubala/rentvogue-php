<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests\Api\RefundRequest;
use App\Http\Controllers\Controller;
use App\Refund;
use App\Order;

class OrderRefundController extends Controller
{
    private $refund;
    private $order;

    public function __construct(Refund $refund, Order $order)
    {
        $this->refund = $refund;
        $this->order = $order;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($orderId)
    {
        $order = $this->order->find($orderId);
        if (empty($order)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }

        if (!$order->refund) {
            return response()->json(['data' => null]);
        }
        $data = $order->refund->toArray();
        $data['express_company'] = config('system.database.refund_express')[$data['express_company']];
        $data['express_details'] = $this->getExpressDatas($data['express_company'], $data['express_number']);

        return response()->json(['data' => (object)$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($orderId, RefundRequest $request)
    {
        $order = $this->order->find($orderId);
        if (empty($order)) {
            return response()->json([
                'message' => config('system.error_codes.data_not_exists')['message'],
                'error_code' => config('system.error_codes.data_not_exists')['code']
            ], 404);
        }
        $inputs = $request->all();
        $inputs['order_id'] = $orderId;
        $refund = $this->refund->create($inputs);

        return response()->json(['data' => $refund], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getExpressDatas($express_company, $express_number)
    {
        $request = [
            'OrderCode' => '',
            'ShipperCode' => $express_company,
            'LogisticCode' => $express_number,
        ];
        $requestData = json_encode($request);
        $datas = [
            'EBusinessID' => config('services.express.b_id'),
            'RequestType' => '1002',
            'RequestData' => urlencode($requestData),
            'DataType' => '2',
        ];
       $datas['DataSign'] = urlencode(base64_encode(md5($requestData.config('services.express.key'))));
       return $datas;
    }
}
