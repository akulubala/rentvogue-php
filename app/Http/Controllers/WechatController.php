<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use EasyWeChat\Foundation\Application;
use App\User as SysUser;
use App\WechatMenu;
use Log;

class WechatController extends Controller
{
    private $app;

    public function __construct()
    {
        $options = [
            'debug'  => true,
            'app_id' => config('services.weixin-mp.app_id'),
            'secret' => config('services.weixin-mp.secret'),
            'token'  => config('services.weixin-mp.token'),
            // 'aes_key' => config('services.weixin-mp.aes_key', null), // 可选
            'log' => [
                'level' => 'debug',
                'file'  => storage_path('logs/easywechat.log'), // XXX: 绝对路径！！！！
            ],
        ];
        $this->app = new Application($options);   
    }

    public function accessEntrance()
    {
        return $this->app->server->serve()->send();
    }

    public function publishMenus()
    {
        $m = $this->app->menu;
        $menus = WechatMenu::where('menu_level', '=', 1)->orderBy('sort', 'asc')->get();
        $results = [];
        $valid = true;
        $i = 0;
        foreach ($menus as $menu) {
            $subMenus = $menu->subMenus()->get();
            if (!$subMenus->isEmpty()) {
                $results[$i] = [
                    'name' => $menu->menu_name
                ];
                foreach ($subMenus as $subMenu) {
                    $results[$i]['sub_button'][] = [
                        'type' => 'view',
                        'name' => $subMenu->menu_name,
                        'url'  => $subMenu->url
                    ];
                }
            } else {
                if ($menu->url != '') {
                    $results[$i] = [
                        'name' => $menu->menu_name,
                        'type' => 'view',
                        'url' => $menu->url
                    ];
                } else {
                    $valid = false;
                }
            }
            $i++;
        }
        if (!$valid) {
            return redirect()->back()->with('fails',trans('status.fails.set_wechat_menus'));
        } else {
            $m->add($results);
            return redirect()->back()->with('success',trans('status.success.set_wechat_menus'));
        }
    }

    private function messageTransfer($server)
    {
         $server->on('message', function($message) {
              return Message::make('transfer');
         });
    }

    private function subscribe($server)
    {
        $server->on('event', 'subscribe', function($event){

            $openID = $event->FromUserName;
            $user = new User($this->_wechatID, $this->_wechatSeceretKey);
            $userInfo = $user->get($openID);
            $url = route('index');
            SysUser::firstOrCreate($member);
            return Message::make('text')
                        ->content(
                            trans('global.wechat.subscribe', ['name' => $userInfo->nickname, 'url' => $url])
                        );
        });
    }
}
