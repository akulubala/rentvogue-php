<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class AuthenticationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = $this->route();
        $routeName = $route->getName();
        if ($routeName === 'auth.sign_up') {
            return [
                'cell_phone' => "required|unique:users",
                "cell_phone_verify_code" => "required|cell_phone_verify_code:{$this->request->get('cell_phone')}",
                'nick_name' => 'required',
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'same:password',
                'remember' => 'in:0,1'
            ];
        } elseif ($routeName === 'auth.app_oauth') {
            return [
                'openid' => "required",
                'access_token' => "required",
                'oauth_type' => 'required|in:weixin,weibo'
            ];
        }
    }
}
