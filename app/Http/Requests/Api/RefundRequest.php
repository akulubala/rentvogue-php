<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class RefundRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'express_company' => 'required|in:' . implode(',', array_keys(config('system.database.refund_express'))),
            'express_number' => 'required',
        ];
    }
}
