<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class UsersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->has('cell_phone')) {
            /**
             * 找回密码
             */
            $cellPhone = $this->request->get("cell_phone");
            if ($this->request->has('password')) {
                return [
                    'cell_phone' => 'required|cell_phone|exists:users',
                    'cell_phone_verify_code' => "required|cell_phone_verify_code:{$cellPhone}",
                    'password' => 'required|min:8'
                ];
            } else {
                /**
                 * 绑定手机
                 */
                return [
                    'cell_phone' => 'required|cell_phone|unique:users,cell_phone',
                    'cell_phone_verify_code' => "required|cell_phone_verify_code:{$cellPhone}",
                    'bind_password' => 'required|min:8'
                ];
            }
        } elseif ($this->request->has('name') || $this->request->has('id_number') || $this->request->has('id_card_frontend') || $this->request->has('id_card_backend')) {
            return [
                'name' => 'required',
                'id_number' => 'required|id_number',
                'id_card_frontend' => 'required',
                'id_card_backend' => 'required'
            ];
        } elseif ($this->request->has('password') && $this->request->has('password_confirmation')) {
            return [
                'old_password' => 'required|min:8|user_password_check',
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required|min:8|same:password',
            ];
        } elseif ($this->request->has('openid') || $this->request->has('access_token') || $this->request->has('oauth_type')) {
            //绑定微信 微博
            return [
                        'openid' => 'required',
                        'access_token' => 'required',
                        'oauth_type' => 'required'
                   ];
        } else {
             return [
                'body_height' => 'numeric',
                'body_weight' => 'numeric',
                'body_figure' => 'in:10,20,30,40,50',
                'clothes_size' => 'in:10,20,30,40,50',
                'body_chest' => 'numeric',
                'waistline' => 'numeric',
                'body_hipline' => 'numeric',
                'outseam' => 'numeric',
            ];
        }
    }
}
