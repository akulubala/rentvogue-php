<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class UserOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = $this->route();
        $routeName = $route->getName();
        $rentStart = $this->request->get("rent_start_date");
        $rentEnd = $this->request->get('rent_end_date');
        if ($routeName == 'api.v1.user.order.update') {
            return ['order_status' => 'in:10,5'];
        } elseif ($routeName == 'api.v1.user.order.pay' || $routeName == 'api.v1.user.order.deposit') {
            return [
                'paid_method' => 'in:upacp_pc,upacp,upacp_wap,wx,wx_pub_qr,alipay,alipay_qr,alipay_pc_direct',
            ];
        } else {
            return [
                'property_group_id' => 'required|numeric',
                'rent_days' => "required|numeric|in:4,8|rent_days_range:{$rentStart},{$rentEnd}",
                'rent_start_date' => 'required|date',
                'rent_end_date' => 'required|date',
                'address_id' => 'numeric',
                'exchange_order_id' => 'numeric',
                'coupon_id' => 'numeric',
                'deposit' => 'numeric',
                'supplier_id' => 'numeric',
            ];
        }
    }
}
