<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('order_status')) {
            return [
                "order_status" => "in:not_paid,paid_not_deliveried,exchange_up_level,delivering,recieved,returned,reurned_deposit,sales_warranty,order_canceled,cancel_apply,dealing_cancel,rejected_cancel,returned_cancel_money,exchange_order_finish"
            ];
        } else {
            return [
                'name' => 'required|in:express_company,express_number,reject_cancel_reason,address',
                'value' => 'required'
            ];
        }
    }
}
