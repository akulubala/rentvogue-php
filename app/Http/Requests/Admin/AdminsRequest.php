<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class AdminsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = $this->route();
        $routeName = $route->getName();
        if ($routeName === 'admin.admin.store') {
            return [
                'user_name' => "required|unique:admins,user_name,NULL,id,deleted_at,NULL",
                'password' => 'required|confirmed',
                'role' => 'required|in:super_admin,admin'
            ];
        } elseif ($routeName === 'admin.admin.update') {
            $id = $this->admin;
            return [
                'user_name' => "required|unique:admins,user_name,{$id}",
                'role' => 'in:super_admin,admin',
                'current_password' => "admin_password_check:{$id}",
                'new_password' => 'confirmed'
            ];
        } elseif ($routeName === 'admin.admin_password.update') {
            return [
                'current_password' => 'required|admin_password_check',
                'new_password' => 'required|confirmed'
            ];
        }
    }
}
