<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ProductPropertyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = $this->route()->getName();
        if ($route == 'admin.product.product_property.store') {
            return [
                'factory_id' => 'required|unique:products,factory_id',
                'color_id' => 'required|numeric',
                'size' => 'required|numeric',
                'images' => 'required',
                'is_available' => 'in:Y,N',
                'duplicated' => 'in:1'
            ];
        } else {
            return [
                'value' => 'unique:products,factory_id',
                'factory_id' => "unique:products,factory_id,{$this->product_property}",
                'color_id' => 'numeric',
                'size' => 'numeric',
                'is_available' => 'in:Y,N',
            ];
        }
    }
}
