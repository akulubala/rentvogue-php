<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = $this->route();
        $routeName = $route->getName();
        if ($this->request->get('is_available')) {
            return [
                'is_available' => 'in:Y,N'
            ];
        } else {
            if ($routeName === 'admin.product.store') {
                return [
                    'product_category_id' => 'required|numeric',
                    'usage_category' => 'required|numeric',
                    'occasion_id' => 'required|numeric',
                    'style_id' => 'required|numeric',
                    'body_figure' => 'required|numeric',
                    'name' => 'required',
                    'market_price' => 'required|numeric',
                    'four_day_rent_price' => 'required|numeric',
                    'eight_day_rent_price' => 'required|numeric',
                    'supplier_id' => 'required|numeric',
                    'deposit' => 'required|numeric',
                    'describe' => 'required',
                    'city' => 'required',
                ];
            } else {
                return [
                    'product_category_id' => 'required|numeric',
                    'usage_category' => 'required|numeric',
                    'occasion_id' => 'required|numeric',
                    'style_id' => 'required|numeric',
                    'body_figure' => 'required|numeric',
                    'name' => 'required',
                    'market_price' => 'required|numeric',
                    'four_day_rent_price' => 'required|numeric',
                    'eight_day_rent_price' => 'required|numeric',
                    'deposit' => 'required|numeric',
                    'describe' => 'required',
                    'city' => 'required',
                    'supplier_id' => 'required|numeric',
                ];
            }
        }
    }
}
