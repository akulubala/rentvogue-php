<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class WechatMenusRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $menuLevel = $this->get('menu_level');
        $size = $menuLevel == 1 ? 4 : 7;
        $route = $this->route();
        $routeName = $route->getName();
        if ($routeName === 'admin.wechat-menus.update' || $routeName === 'admin.wechat-menus.sub-menus.update') {
            $rules = [
                'menu_name' => "required|max:{$size}|wechat_menu_limit:{$menuLevel}",
                'menu_level' => 'required|in:1,2',
                'parent_id' => 'numeric',
                // 'url' => 'active_url',
                'sort' => 'required|numeric',
            ];
        } else {
            $rules = [
                'menu_name' => "required|max:{$size}",
                'menu_level' => 'required|in:1,2',
                'parent_id' => 'numeric',
                // 'url' => 'active_url',
                'sort' => 'required|numeric',
            ];
        }


        return $rules;
    }
}
