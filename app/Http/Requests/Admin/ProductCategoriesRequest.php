<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ProductCategoriesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = $this->route()->getName();
        if ($route == 'admin.product_category.store') {
            return [
                'name' => 'required',
                'series' => 'required',
                'order' => 'required|numeric',
                'image' => 'required',
                'recommend' => 'required',
            ];
        } elseif ($route == 'admin.product_category.update') {
            return [
                'order' => 'numeric',
            ];
        }
    }
}
