<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Middleware as JWTMiddleware;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Closure;

class JWTGetUserFromToken extends JWTMiddleware\BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //可以不传token的路由
        $isExcept = in_array($request->route()->getName(), config('jwt.except_routes'));

        if (! $token = $this->auth->setRequest($request)->getToken()) {
            if ($isExcept) {
                return $next($request);
            }
            return response()->json(['message' => config('system.error_codes.token_invalid')['message'], 'error_code' => config('system.error_codes.token_invalid')['code']], 401);
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return response()->json(['message' => config('system.error_codes.token_expire')['message'], 'error_code' => config('system.error_codes.token_expire')['code']], 401);
        } catch (JWTException $e) {
            return response()->json(['message' => config('system.error_codes.token_invalid')['message'], 'error_code' => config('system.error_codes.token_invalid')['code']], 400);
        }

        if (! $user) {
            return response()->json(['message' => config('system.error_codes.user_not_exists')['message'], 'error_code' => config('system.error_codes.user_not_exists')['code']], 404);
        }

        $this->events->fire('tymon.jwt.valid', $user);
        return $next($request);
    }
}
