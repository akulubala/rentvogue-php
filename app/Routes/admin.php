<?php
//管理员后台
Route::controller(
    'admin/login',
    'Admin\LoginController',
    [
      'getIndex' => 'admin.login.index',
      'postSignIn' => 'admin.login.sign_in',
      'getSignOut' => 'admin.login.sign_out',
    ]
);

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin.auth'], function(){
    Route::get('index', ['uses' => 'IndexController@getIndex', 'as' => 'admin.index.index']);
    Route::get('token', ['uses' => 'IndexController@getToken', 'as' => 'admin.index.token']);
    Route::get('admin/reset-password', ['uses' => 'AdminController@getResetPassword', 'as' => 'admin.admin_password.eidt']);
    Route::put('admin/reset-password', ['uses' => 'AdminController@postResetPassword', 'as' => 'admin.admin_password.update']);
    Route::resource('admin', 'AdminController');
    Route::resource('user', 'UserController', ['except' => ['show', 'create', 'store']]);
    Route::get('verify', ['as' => 'admin.verify.index', 'uses' => 'UserController@verify']);
    Route::get('user/address/{userId}', ['as' => 'admin.user.address', 'uses' => 'UserController@address']);
    Route::get('user/address', ['as' => 'admin.user.address.config', 'uses' => 'UserController@addressConfig']);
    Route::resource('product_category', 'ProductCategoryController');
    Route::post('product_category/order', ['uses' => 'ProductCategoryController@postOrder', 'as' => 'admin.product_category.order']);
    Route::resource('order', 'OrderController');
    Route::get('order/cancel-accept', ['uses' => 'OrderController@getCancelAccept', 'as' => 'admin.order.cancel_accept']);
    Route::resource('order.sale_service', 'OrderSaleServiceController');
    Route::resource('style', 'StyleController');
    Route::resource('tag', 'TagController');
    Route::resource('color', 'ColorController');
    Route::resource('product', 'ProductController');
    Route::resource('product.product_property', 'ProductPropertyController', ['except' => 'show']);
    Route::resource('product.product_unavailable', 'ProductUnavailableController', ['except' => ['show', 'edit', 'update']]);
    Route::resource('hot_word', 'HotWordController');
    Route::resource('comment', 'CommentController');
    Route::resource('coupon_config', 'CouponConfigController');
    Route::resource('feedback', 'FeedbackController');
    Route::resource('slide', 'SlideController');
    Route::post('slide/order', ['uses' => 'SlideController@postOrder', 'as' => 'admin.slide.order']);
    Route::resource('supplier', 'SupplierController');
    Route::resource('occasion', 'OccasionController');
    Route::post('occasion/order', ['uses' => 'OccasionController@postOrder', 'as' => 'admin.occasion.order']);
    Route::resource('wechat-menus', 'WechatMenusController');
    Route::resource('wechat-menus.sub-menus', 'WechatSubMenusController');
});
