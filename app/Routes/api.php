<?php
Route::group(['prefix' => 'api'], function()
{
    /**
     * api without auth
     */
    Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function()
    {
        Route::controller('authenticate',
            'AuthenticateController',
            [
                'postSignIn' => 'auth.sign_in',
                'postSignUp' => 'auth.sign_up',
                'getSignOut' => 'auth.sign_out',
                'getWebOauthDouban' => 'auth.web_oauth_douban',
                'postAppOauth' => 'auth.app_oauth'
            ]
        );
        Route::get('cell_phone_code', ['as' => 'get_cell_verify_code', function() {
            $phone_number = \Input::get('phone_number');
            if (Cache::get('limit_'.$phone_number)) {
                   return response()->json(['message' => config('system.error_codes.sms_failed')['message'], 'error_code' => config('system.error_codes.sms_failed')['code']], 500);
            } else {
                $ch = new \Curl\Curl();
                $ch->setBasicAuthentication(config('services.sms.username'), config('services.sms.password'));
                $ch->setConnectTimeout('30');
                $code = cell_verify_code();
                $response = $ch->post(config('services.sms.url'), array(
                    'mobile' => $phone_number,
                    'message' => config('services.sms.message', '你的手机验证码为：'.$code."【Rentvogue】")
                ));
                if ($response->error === 0) {
                    Cache::put($phone_number, $code, 10);
                    Cache::put('limit_'.$phone_number, 1, 1);
                    return response()->json(['verify_code' => $code], 200);
                } else {
                    custom_logs('sms', "request sms error code :" . $phone_number . $response->error);
                    return response()->json(['message' => config('system.error_codes.sms_failed')['message'], 'error_code' => config('system.error_codes.sms_failed')['code']], 500);
                }
            }
        }])->where(['id' => '[1-9][0-9]{10}']);
        Route::get(
            'city',
            [
                'as' => 'city.index',
                'uses' => 'UserAddressController@getCity'
            ]
        );

        Route::resource(
            'style.tag',
            'StyleTagController',
            ['only' => ['index', 'show']]
        );

        Route::resource(
            'hotword',
            'HotWordController',
            ['only' => ['index']]
        );

        Route::resource(
            'product.comment',
            'ProductCommentController',
            ['only' => ['index']]
        );

        Route::get(
            'product/recommend',
            [
                'as' => 'product.recommend',
                'uses' => 'ProductController@recommend'
            ]
        );

        Route::resource(
            'product.unavaliable_rent_dates',
            'ProductUnavaliableRentDateController',
            ['only' => ['index']]
        );

        Route::post(
            'coupon/get',
            [
                'as' => 'coupon.store',
                'uses' => 'CouponController@store'
            ]
        );

        Route::resource(
            'slide',
            'SlideController',
            ['only' => ['index']]
        );

        Route::resource(
            'category',
            'ProductCategoryController',
            ['only' => ['show']]
        );

        Route::get('get_express_number/{expressNumber}', 
                  [
                    'as' => 'get_express_info', 
                    'uses' => 'UserOrderController@getExpressByNumber'
                  ]
        );

        Route::get('weixin_access_token/{code}', function($code) {
            $ch = new \Curl\Curl();
            $url = "https://api.weixin.qq.com/sns/oauth2/access_token?" . http_build_query([
                                                                            'appid' => config('services.weixin-web.client_id'), 
                                                                            'secret' => config('services.weixin-web.client_secret'),
                                                                            'code' => $code, 
                                                                            'grant_type' => 'authorization_code'
                                                                            ]);
            $response = $ch->get($url);
            return response()->json(['data' => json_decode($response, true)], 200);
        });
        Route::get('weibo_access_token/{code}', function($code) {
            $ch = new \Curl\Curl();
            $url = "https://api.weibo.com/oauth2/access_token?" . http_build_query([
                                                                    'client_id' => config('services.weibo-web.client_id'), 
                                                                    'client_secret' => config('services.weibo-web.client_secret'),
                                                                    'redirect_uri' => 'http://rentvogue.com/',
                                                                    'code' => $code, 
                                                                    'grant_type' => 'authorization_code'
                                                                    ]);
            $response = $ch->post($url);
            return response()->json(['data' => json_decode($response, true)], 200);
        });
        Route::resource(
            'supplier',
            'SupplierController',
            ['only' => ['index']]
        );
    });

    /**
     * api with jwt auth
     */
    Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1', 'middleware' => 'jwt.auth'], function()
    {
        Route::resource(
            'user',
            'UserController',
            ['only' => ['show', 'update']]
        );

        Route::resource(
            'user.address',
            'UserAddressController',
            ['except' => ['create', 'edit']]
        );

        Route::get('qiniu_token', ['as' => 'get_qiniu_token', function() {
            $token = qiniu_token();
            return response()->json(['data'=> ['uptoken' => $token, 'domain' => config('services.qiniu.domain_name')]], 200);
        }]);
        Route::post('id-verify', function() {
            $user = \Auth::user()->get();
            if ($user->verify_times >=100) {
                return response()->json(['message' => config('system.error_codes.id_verify_exceed')['message'], 'error_code' => config('system.error_codes.id_verify_exceed')['code']], 400);
            } else {
                $user->verify_times += 1;
                $user->save();
            }
            $ch = new \Curl\Curl();
            $response = $ch->post(config('services.id_verify.url'),
                [
                    'cardNo' => \Input::get('cardNo', null),
                    'realName' => \Input::get('realName', null),
                    'key' => config('services.id_verify.key')
                ]
            );
            return response()->json(['data' => json_decode($response, true)], 200);
        });

        Route::resource(
            'product',
            'ProductController',
            ['only' => ['index', 'show']]
        );

        Route::post(
            'product/favorite/{group_id}',
            [
                'as' => 'product.favorite',
                'uses' => 'ProductController@favorite'
            ]
        );

        Route::resource(
            'user.favorite',
            'UserFavoriteController',
            ['only' => ['index']]
        );

        Route::resource(
            'user.sms',
            'UserSmsController',
            ['only' => ['index', 'destroy']]
        );

        Route::resource(
            'product.comment',
            'ProductCommentController',
            ['only' => ['store']]
        );

        Route::resource(
            'order.refund',
            'OrderRefundController',
            ['only' => ['index', 'store']]
        );

        Route::resource(
            'coupon',
            'CouponController',
            ['only' => ['index', 'show']]
        );

        Route::resource(
            'user',
            'UserController',
            ['only' => ['show', 'update']]
        );

        Route::resource(
            'user.order',
            'UserOrderController'
        );

        Route::post(
            'user/{userId}/order/{orderId}/pay',
            [
                'as' => 'api.v1.user.order.pay',
                'uses' => 'UserOrderController@pay'
            ]
        );

        Route::post(
            'user/{userId}/order/{orderId}/deposit',
            [
                'as' => 'api.v1.user.order.deposit',
                'uses' => 'UserOrderController@deposit'
            ]
        );

        Route::get(
            'order/{orderNumber}',
            [
                'as' => 'api.v1.order.orderNo',
                'uses' => 'UserOrderController@getOrderByOrderNo'
            ]
        );

        Route::resource(
            'feedback',
            'FeedbackController',
            ['only' => ['store']]
        );

        Route::resource(
            'category',
            'ProductCategoryController',
            ['only' => ['index']]
        );
    });
});
