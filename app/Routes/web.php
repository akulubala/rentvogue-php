<?php
Route::get('/', function() {
    return redirect()->to('/index.html');
});
/**
 * 微信相关
 */
Route::any('wechat/access-entrance', 'WechatController@accessEntrance');
Route::get('wechat/publish-menus', ['uses' => 'WechatController@publishMenus', 'as' => 'wechat.publish_menus']);

Route::group(['namespace' => 'Web'], function() {
    Route::post('pingxx', 'CallbackController@pingxxResult');
});

