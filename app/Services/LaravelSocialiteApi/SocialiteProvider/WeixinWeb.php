<?php
namespace App\Services\LaravelSocialiteApi\SocialiteProvider;

use SocialiteProviders\WeixinWeb\Provider;

class WeixinWeb extends Provider
{
    public function user($accessToken = null, $openId = null)
    {
        if ($this->hasInvalidState()) {
            throw new InvalidStateException;
        }

        if ($openId !== null) {
            $this->openId = $openId;
        }

        if ($accessToken === null) {
        	$accessToken = $this->getAccessToken($this->getCode());
        }
    	$user = $this->mapUserToObject($this->getUserByToken($accessToken));
        
        return $user->setToken($accessToken);
    }
}
