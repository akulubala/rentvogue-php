<?php

namespace App\Services\LaravelSocialiteApi\ExtendSocialite;

use SocialiteProviders\Manager\SocialiteWasCalled;

class Weibo
{
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
    	$provider = 'App\Services\LaravelSocialiteApi\SocialiteProvider\Weibo';
        $socialiteWasCalled->extendSocialite(snake_case('Weibo', '-'), $provider);
    }
}

