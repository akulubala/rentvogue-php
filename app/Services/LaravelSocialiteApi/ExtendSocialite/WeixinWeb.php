<?php

namespace App\Services\LaravelSocialiteApi\ExtendSocialite;

use SocialiteProviders\Manager\SocialiteWasCalled;

class WeixinWeb
{
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
    	$provider = 'App\Services\LaravelSocialiteApi\SocialiteProvider\WeixinWeb';
        $socialiteWasCalled->extendSocialite(snake_case('WeixinWeb', '-'), $provider);
    }
}

