<?php

namespace App\Services\LaravelSocialiteApi\ExtendSocialite;

use SocialiteProviders\Manager\SocialiteWasCalled;

class Weixin
{
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
    	$provider = 'App\Services\LaravelSocialiteApi\SocialiteProvider\Weixin';
        $socialiteWasCalled->extendSocialite(snake_case('Weixin', '-'), $provider);
    }
}

