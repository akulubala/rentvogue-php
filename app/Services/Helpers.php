<?php
use Pingpp\Pingpp;
use Pingpp\Charge;
use Carbon\Carbon;

/**
 * 根据route name判断nav_bar是否展开
 *
 */
if (!function_exists('nav_class_routes')) {
    function nav_class_routes() {
        $routes = func_get_args();
        $currRoute = Route::currentRouteName();
        $querys = Request::getQueryString() === null ? null : explode('=', Request::getQueryString());
        $params = $querys === null ? null : [$querys[0] => $querys[1]];
        $active = '';
        foreach ($routes as $route) {
            if (is_string($route)) {
                if ($route === $currRoute && empty($params)) {
                    $active = 'active';
                    break;
                }
            } elseif (is_array($route)) {
                if($route['route'] === $currRoute && $route['params'] === $params) {
                    $active = 'active';
                    break;
                }
            }
        }

        return $active;
    }
}

if (!function_exists('menu_open_routes')) {
    function menu_open_routes($route_name) {
        $currRoute = Route::currentRouteName();
        $menu_open = '';
        $pos = strpos($currRoute, $route_name);
        if ($pos !== false) {
            $menu_open = 'menu-open';
        }

        return $menu_open;
    }
}

if (!function_exists('frontend_nav_class')) {
    function frontend_nav_class() {
        if (in_array(Route::currentRouteName(), func_get_args())) {
            return 'current';
        }

        return null;
    }
}

if (!function_exists('frontend_page_links')) {
    function frontend_page_links($model)
    {
        return $model->appends(Request::except('page'))->render(
            new \App\Extensions\FrontendPresenter($model)
        );
    }
}
/**
 * 手机验证码
 */
if (!function_exists('cell_verify_code')) {
    function cell_verify_code()
    {
        $numbers = "0123456789987654321001234567899876543210012345678901234567899876543210012345678998765432100123456789";
        $shuffledNumber = str_shuffle($numbers);
        return substr($shuffledNumber, mt_rand(0, 94), 6);
    }
}
/**
 * 自定义log
 */
 if (!function_exists('custom_logs')) {
    function custom_logs($fileName, $message)
    {
        $log = new Monolog\Logger($fileName);
        $log->pushHandler(new Monolog\Handler\StreamHandler(storage_path("logs/{$fileName}.log"), \Monolog\Logger::INFO));
        $log->addInfo($message);
    }
 }
 /**
  * 七牛
  * upload
  * token
  */
 if (!function_exists('qiniu_token')) {
    function qiniu_token()
    {
        $auth = new \Qiniu\Auth(
            env('QINIU_ACCESS_KEY'),
            env('QINIU_SECRET_KEY')
        );
        return $auth->uploadToken(env('QINIU_BUCKET_NAME'));
    }
 }
 /**
  * 获取日期段交集
  */
 if (!function_exists('date_intersects')) {
    function date_intersects($r1start, $r1end, $r2start, $r2end)
    {
        $r1start = strtotime($r1start);
        $r1end = strtotime($r1end);
        $r2start = strtotime($r2start);
        $r2end = strtotime($r2end);
        $intersect = ($r1start == $r2start) || ($r1start > $r2start ? $r1start <= $r2end : $r2start <= $r1end);
        if ($intersect) {
            $start = $r1start > $r2start ? $r1start : $r2start;
            $end = $r1end > $r2end ? $r2end : $r1end;
            return ['start_date' => date('Y-m-d', $start), 'end_date' => date('Y-m-d', $end)];
        }
        return [];
    }
 }
  /**
  * 获取日期段并集
  */
 if (!function_exists('date_unions')) {
    function date_unions($first, $second)
    {
        $r1start = strtotime($first['start_date']);
        $r1end = strtotime($first['end_date']);
        $r2start = strtotime($second['start_date']);
        $r2end = strtotime($second['end_date']);
        $intersect = ($r1start == $r2start) || ($r1start > $r2start ? $r1start <= $r2end : $r2start <= $r1end);
        if ($intersect) {
            $start = $r1start < $r2start ? $r1start : $r2start;
            $end = $r1end < $r2end ? $r2end : $r1end;
            return ['start_date' => date('Y-m-d', $start), 'end_date' => date('Y-m-d', $end)];
        }
        return [];
    }
 }
 /**
  *
  */
 if (!function_exists('get_unavaliable_rent_dates')) {
    function get_unavaliable_rent_dates($propertyGroupId, $isRefresh = false)
    {
        $key = 'unavaliable_rent_dates' . '_' . $propertyGroupId;
        $output = Cache::get($key);
        if (!empty($output) && $isRefresh === false) {
            return json_decode($output, true);
        }

        if (empty($productIds)) { //这里是 和下订单 那块 为省去一次查询，本来这个参数是不需要的
            $productIds = App\Product::where('property_group_id', $propertyGroupId)->lists('id');
        }

        $rentDatas = App\UnavaliableRentDate::whereIn('product_id', $productIds)
                      ->where('end_date', '>=', date('Y-m-d', time()))
                      ->orderBy('start_date')
                      ->get(['product_id', 'start_date', 'end_date'])
                      ->groupBy('product_id')->toArray();

        if (count($productIds) > count($rentDatas)) {
            $output = [
                'four_days' => [],
                'eight_days' => [],
            ];
            Cache::put($key, json_encode($output), Carbon::tomorrow());
            return $output;
        }

        //换成4天租期的并集再求交集
        $four_day_rentDatas = union_near_date($rentDatas, config('system.4_rent_days'));
        $four_day_output = [];
        $firstSet = array_shift($four_day_rentDatas);
        foreach ($firstSet as $rentData) {
            $result = get_intersect_date($rentData, $four_day_rentDatas);
            if (!empty($result)) {
                unset($result['product_id']);
                $four_day_output[] = $result;
            }
        }

        //换成8天租期的并集再求交集
        $eight_day_rentDatas = union_near_date($rentDatas, config('system.8_rent_days'));
        $eight_day_output = [];
        $firstSet = array_shift($eight_day_rentDatas);
        foreach ($firstSet as $rentData) {
            $result = get_intersect_date($rentData, $eight_day_rentDatas);
            if (!empty($result)) {
                unset($result['product_id']);
                $eight_day_output[] = $result;
            }
        }

        $output = [
            'four_days' => $four_day_output,
            'eight_days' => $eight_day_output,
        ];
        Cache::put($key, json_encode($output), Carbon::tomorrow());
        return $output;
    }
 }

if (!function_exists('get_intersect_date')) {
    function get_intersect_date($set1, $allset)
    {
        $set2 = array_shift($allset);
        if (empty($set2)) {
            return $set1;
        } else {
            foreach ($set2 as $date) {
                $intersect = date_intersects($set1['start_date'], $set1['end_date'], $date['start_date'], $date['end_date']);
                if (!empty($intersect)) {
                    return get_intersect_date($intersect, $allset);
                }
            }
            return [];
        }
    }
}

if (!function_exists('union_near_date')) {
    function union_near_date($all_rentDatas, $days)
    {
        $output = [];
        foreach ($all_rentDatas as $rentDatas) {
            $firstData = array_shift($rentDatas);
            $firstData['start_date'] = Carbon::parse($firstData['start_date'])->subDays($days)->toDateString('Y-m-d');
            $result = [];
            foreach ($rentDatas as $rentData) {
                $rentData['start_date'] = Carbon::parse($rentData['start_date'])->subDays($days)->toDateString('Y-m-d');
                $unions = date_unions($firstData, $rentData);
                if (empty($unions)) {
                    $result[] = $firstData;
                    $firstData = $rentData;
                } else {
                    $firstData = $unions;
                }
            }
            $result[] = $firstData;
            $output[] = $result;
        }
        return $output;
    }
}

 /*
  * 单系统20位唯一ID
  */
 if (!function_exists('unique_string_number')) {
    function unique_string_number()
    {
        return date('ymd') . substr(time(), -5) . str_pad(mt_rand(0, 99), 2, '0', STR_PAD_LEFT);
    }
}
/*
 * ping++创建charge
 */
if (!function_exists('get_pingxx_charge')) {
    function pingxx_charge($channel, $orderNumber, $amount, $productName = null, $subject = null, $call_back = null) {
        switch ($channel) {
            case 'upacp':
                $extra = [];
                break;
            case 'upacp_pc':
                $extra = [
                    'result_url' => empty($call_back)? config('services.pingxx.call_back') : $call_back
                ];
                break;
            case 'upacp_wap':
                $extra = [
                    'result_url' => empty($call_back)? config('services.pingxx.call_back') : $call_back
                ];
                break;
            case 'wx':
                $extra = [];
                break;
            case 'wx_pub_qr':
                $extra = [
                        'product_id' => '1'
                    ];
                break;
            case 'alipay':
                $extra = [];
                break;
            case 'alipay_qr':
                $extra = [];
                break;
            case 'alipay_pc_direct':
                $extra = [
                    'success_url' => empty($call_back)? config('services.pingxx.call_back') : $call_back
                ];
                break;
        }
        try {
            Pingpp::setApiKey(config('services.pingxx.api_key'));
            $ch = Charge::create([
                    'order_no'  => $orderNumber,
                    // 'amount'    => $amount,
                    'amount' => 1,
                    'app'       => ['id' => config('services.pingxx.app_key')],
                    'channel'   => $channel,
                    'currency'  => 'cny',
                    'extra'     => $extra,
                    'client_ip' => Request::getClientIp(),
                    'subject'   => $subject ? $subject : config('services.pingxx.subject'),
                    'body'      => $productName ? $productName : config('services.pingxx.body')
            ]);
            return $ch;
        } catch (Exception $e) {
            custom_logs('pingxx_charge', $e->getMessage(). '|' . 'order_number:' . $orderNumber);
            throw new Exception($e->getMessage());
        }
    }
}
/**
 * 退款
 */
if (!function_exists('pingxx_refund')) {
    function pingxx_refund($chId, $amount, $description = 'rentvogue refund') {
        try {
            Pingpp::setApiKey(config('services.pingxx.api_key'));
            $ch = Charge::retrieve($chId);
            $ch->refunds->create(
                  array(
                      'amount' => $amount * 100,
                      'description' => $description
                  )
              );
            return $ch;
        } catch (Exception $e) {
            custom_logs('pingxx_refund', $e->getMessage(). '|'. 'ch_id:' . $chId);
            throw new Exception($e->getMessage());
        }
    }
}
/*
 * sms
 */
if (!function_exists('luosimao_sms')) {
    function luosimao_sms($cellphone, $message) {
        $ch = new \Curl\Curl();
        $ch->setBasicAuthentication(config('services.sms.username'), config('services.sms.password'));
        $ch->setConnectTimeout('30');
        $code = cell_verify_code();
        $response = $ch->post(config('services.sms.url'), array(
            'mobile' => $cellphone,
            'message' => $message
        ));
        if ($response->error === 0) {
            return true;
        } else {
           custom_logs('sms', "request sms error code :" . $response->error);
           return false;
        }
    }
}
/**
 * 二维数组去重复
 */
if (!function_exists('unique_arr')) {
function unique_arr($array2D, $stkeep=false, $ndformat=true)
{
    // 判断是否保留一级数组键 (一级数组键可以为非数字)
    if($stkeep) $stArr = array_keys($array2D);

    // 判断是否保留二级数组键 (所有二级数组键必须相同)
    if($ndformat) $ndArr = array_keys(end($array2D));

    //降维,也可以用implode,将一维数组转换为用逗号连接的字符串
    foreach ($array2D as $v){
        $v = join(",",$v);
        $temp[] = $v;
    }

    //去掉重复的字符串,也就是重复的一维数组
    $temp = array_unique($temp);

    //再将拆开的数组重新组装
    foreach ($temp as $k => $v)
    {
        if($stkeep) $k = $stArr[$k];
        if($ndformat)
        {
            $tempArr = explode(",",$v);
            foreach($tempArr as $ndkey => $ndval) $output[$k][$ndArr[$ndkey]] = $ndval;
        }
        else $output[$k] = explode(",",$v);
    }

    return $output;
}
}


