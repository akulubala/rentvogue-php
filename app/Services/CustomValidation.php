<?php
namespace App\Services;

use Illuminate\Validation\Validator;
use Auth;
use Hash;
use App\Admin;
use App\WechatMenu;
use Image;
use Input;
use App\User;
use Cache;
use Exception;
use Carbon\Carbon;

class CustomValidation extends Validator
{

    public function validateAdminLogin()
    {
       return Auth::admin()->attempt([
            'user_name' => Input::get('user_name', null),
            'password' => Input::get('password', null)
        ], Input::get('remember', false) === 'on');
    }

    public function validateCounselorLogin()
    {
        return Auth::counselor()->attempt([
            'cell_phone' => Input::get('cell_phone', null),
            'password' => Input::get('password', null)
        ], Input::get('remember', false) === 'on');
    }

    public function validateAdminPasswordCheck($attribute, $value, $parameters)
    {
        if (empty($value)) {
            return true;
        }

        if (empty($parameters[0])) {
            return Auth::admin()->check() && Hash::check($value, Auth::admin()->get()->password);
        }

        $administrator = Admin::find($parameters[0]);
        if ($administrator) {
            return Hash::check($value, $administrator->password);
        }

        return false;
    }

    public function validateUserPasswordCheck($attribute, $value, $parameters)
    {
        if(!empty($parameters)) {
             return Hash::check($value, User::find($parameters[0])->password);
        } else {
            return Auth::user()->check() && Hash::check($value, Auth::user()->get()->password);
        }

    }

    public function validateCellPhone($attribute, $value, $parameters)
    {
        if(preg_match("/^[0-9]{1,11}$/", $value))
        {
            return true;
        }
        return false;
    }

    public function validateIdNumber($attribute, $value, $parameters)
    {

        if (preg_match("/\d{17}[\d|X]|\d{15}/", $value)) {
            return true;
        }
        return false;
    }

    public function validateCellPhoneVerifyCode($attribute, $value, $parameters)
    {
        return Cache::get(head($parameters)) === $value;
    }

    public function validateImageResolution($attribute, $value, $parameters)
    {
        try {
            if (Input::hasFile($attribute)) {
                $image = Image::make(Input::file($attribute));
                return intval($image->width()) === intval($parameters[0]) &&
                    intval($image->height()) === intval($parameters[1]);
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function validateGreaterThen($attribute, $value, $parameters)
    {
        return floatval($value) > floatval($parameters[0]);
    }

    public function validateRentDaysRange($attribute, $value, $parameters)
    {
        if ($parameters[0] < date('Y-m-d H:i:s', time()) || $parameters[1] < date('Y-m-d H:i:s', time())) {
            return false;
        }
        $compare = Carbon::createFromFormat('Y-m-d', $parameters[0])->addDays($value - 1)->toDateString();
        return $compare === $parameters[1];
    }

}
