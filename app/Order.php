<?php
namespace App;

use App\Coupon;
use App\Product;
use App\SaleService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends BaseModel
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'parent_order_id',
        'user_id',
        'product_id',
        'product_name_group_id',
        'contact_user_name',
        'address',
        'phone',
        'rent_days',
        'rent_start_date',
        'rent_end_date',
        'rent_price',
        'order_number',
        'order_status',
        'paid_method',
        'cancel_apply_date',
        'reject_cancel_reason',
        'express_company',
        'express_number',
        'coupon_id',
        'pingxx_ch',
        'remarks',
        'paid_deposit',
        'deposit_pingxx_ch',
        'deposit_method',
    ];

    public function refund()
    {
        return $this->hasOne(Refund::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function parentOrder()
    {
        return $this->find($this->parent_order_id);
    }

    public function saleServices()
    {
        return $this->hasMany(SaleService::class);
    }

    public function getOrderStatusAttribute($value)
    {
        $mutators = config('system.database.order_status');
        return (int)array_flip($mutators)[$value] + 1;
    }

    public function getPingxxChAttribute($value)
    {
        return json_decode($value, true);
    }

    public function getDepositPingxxChAttribute($value)
    {
        return json_decode($value, true);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id', 'id');
    }

    public function scopeSixMonthsCounts()
    {
        $results = array();
        $startDay = Carbon::now()->addMonths(-6);
        $endDay = Carbon::now();
        $i = 0;
        while ($endDay->diffInDays($startDay, false) < 0) {
            $endWeekDay = $startDay->addDay(7);
            $startWeekDay = Carbon::parse($endWeekDay)->addDay(-7);
            $k = $startWeekDay->format('m-d') . ' ~ ' . $endWeekDay->format('m-d');
            $results['x_line'][$i] = $k;
            $results['y_line'][$i] = $this->whereIn('order_status', array('paid_not_deliveried','exchange_up_level','delivering','recieved','returned','reurned_deposit','sales_warranty','cancel_apply','dealing_cancel','rejected_cancel','exchange_order_finish'))->whereBetween('updated_at', [$startWeekDay, $endWeekDay])->count();
            $i++;
        }
        return $results;
    }

    public function scopeSixMonthsAmountCounts()
    {
        $results = array();
        $startDay = Carbon::now()->addMonths(-6);
        $endDay = Carbon::now();
        $i = 0;
        while ($endDay->diffInDays($startDay, false) < 0) {
            $endWeekDay = $startDay->addDay(7);
            $startWeekDay = Carbon::parse($endWeekDay)->addDay(-7);
            $k = $startWeekDay->format('m-d') . ' ~ ' . $endWeekDay->format('m-d');
            $results['x_line'][$i] = $k;
            $results['y_line'][$i] = $this->whereIn('order_status', array('paid_not_deliveried','exchange_up_level','delivering','recieved','returned','reurned_deposit','sales_warranty','cancel_apply','dealing_cancel','rejected_cancel','exchange_order_finish'))->whereBetween('updated_at', [$startWeekDay, $endWeekDay])->sum('rent_price');
            $i++;
        }
        return $results;
    }

    public function scopeDeliveryNextWeek($query)
    {
        return $query->where('order_status', '=', 'paid_not_deliveried')
              ->whereBetween('rent_start_date', [Carbon::now()->toDateString(),Carbon::now()->addDays(8)->toDateString()]);
    }

    public function express_company()
    {
        switch ($this->express_company) {
            case 'shunfeng':
                return '顺丰';
            case 'direct':
                return '商家直送';
            case 'self':
                return '上门自提';
            default:
                return '';
        }
    }

    public function pay_channel()
    {
        switch ($this->paid_method) {
            case 'upacp_pc':
            case 'upacp':
            case 'upacp_wap':
                return '银联支付';
            case 'wx':
                return '微信支付';
            case 'wx_pub_qr':
                return '微信公众账号扫码支付';
            case 'alipay':
                return '支付宝手机支付';
            case 'alipay_qr':
                return '支付宝扫码支付';
            case 'alipay_pc_direct':
                return '支付宝PC网页支付';
            default:
                return '';
        }
    }

    public function deposit_channel()
    {
        switch ($this->deposit_method) {
            case 'upacp_pc':
            case 'upacp':
            case 'upacp_wap':
                return '银联支付';
            case 'wx':
                return '微信支付';
            case 'wx_pub_qr':
                return '微信公众账号扫码支付';
            case 'alipay':
                return '支付宝手机支付';
            case 'alipay_qr':
                return '支付宝扫码支付';
            case 'alipay_pc_direct':
                return '支付宝PC网页支付';
            default:
                return '';
        }
    }
}
