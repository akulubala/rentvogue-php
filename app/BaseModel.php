<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    public function scopeSearch($query, $filter)
    {
        $query = $query->select("{$this->getTable()}.*");
        if (empty($filter)) {
            return $query;
        }

        $conditions = array();
        foreach ($filter as $key => $value) {
            if (empty($value) && $value !== '0') {
                continue;
            }

            array_push($conditions, "{$this->getTable()}.{$key} LIKE binary '%{$value}%'");
        }
        if (empty($conditions)) {
            return $query;
        }

        return $query->whereRaw(implode(' AND ', $conditions));
    }
}
