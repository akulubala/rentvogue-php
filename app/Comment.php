<?php 

namespace App;

class Comment extends BaseModel {

	protected $fillable = [
		'comment_user_id',
        'order_id',
        'product_id',
		'product_name_group_id',
		'stars',
		'comment',
		'tags',
		'images',
	];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'comment_user_id', 'id');
    }

    public function setImagesAttribute($value)
    {
        if (!$value) {
            $this->attributes['images'] = null;
        } else {
             $images = [];
            foreach (explode('|', $value) as $v) {
                $images[] = rtrim(config('services.qiniu.domain_name'), '/') . '/' . $v;
            }
            $this->attributes['images'] = json_encode($images);
        }
    }

    public function scopeVerified($query)
    {
        return $query->where('is_verify', 'Y'); 
    }

    public function scopeNameGroup($query, $group_id)
    {
        return $query->where('product_name_group_id', $group_id); 
    }

    public function getTagsAttribute($value)
    {
        return explode("|", $value);
    }
}
