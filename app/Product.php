<?php

namespace App;

use App\Order;
use App\UnavaliableRentDate;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	protected static $unguarded = true;
    protected $fillable = [
        'factory_id',
        'property_group_id',
        'name_group_id',
        'product_category_id',
        'style_id',
        'usage_category',
        'occasion_id',
        'name',
        'color_id',
        'size',
        'city',
        'body_figure',
        'describe',
        'deposit',
        'market_price',
        'four_day_rent_price',
        'eight_day_rent_price',
        'images',
        'supplier_id',
        'is_available',
        'intro',
    ];

    public function scopeSearch($query, $filter)
    {
        $query = $query->select("{$this->getTable()}.*");
        if (empty($filter)) {
            return $query;
        }

        $conditions = array();
        foreach ($filter as $key => $value) {
            if (empty($value) && $value !== '0') {
                continue;
            }
            $upCaseValue = strtoupper($value);
            $lowerCaseValue = strtolower($value);

            array_push($conditions, "lower({$this->getTable()}.{$key}) LIKE binary lower('%{$value}%') ");
        }
        if (empty($conditions)) {
            return $query;
        }

        return $query->whereRaw(implode(' AND ', $conditions));
    }

    public function brand()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function occasion()
    {
        return $this->belongsTo(Occasion::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function style()
    {
        return $this->belongsTo(Style::class);
    }

    public function unavailable()
    {
        return $this->hasMany(UnavaliableRentDate::class);
    }

    public function setImagesAttribute($value)
    {
        if (!$value) {
            $this->attributes['images'] = null;
        } else {
             $images = [];
            foreach ($value as $v) {
                $images[] = rtrim(config('services.qiniu.domain_name'), '/') . '/' . $v;
            }
            $this->attributes['images'] = json_encode($images);
        }
    }

    public function getImagesAttribute($value)
    {
        $value = json_decode($value, true);
        if ($value) {
            foreach ($value as &$v) {
                $v .= config('services.qiniu.water_mask');
            }
        }

        return json_encode($value);
    }

    public function scopePrimaryCity($query, $city)
    {
        return $query->where('city', $city);
    }

    public function scopeSize($query, $size)
    {
        return $query->where('size', $size);
    }

    public function scopeColor($query, $color)
    {
        return $query->where('color_id', $color);
    }

    public function scopeStyle($query, $style)
    {
        return $query->where('style_id', $style);
    }

    public function scopeOccasion($query, $occasion)
    {
        return $query->where('occasion_id', $occasion);
    }

    public function scopeCategory($query, $category)
    {
        return $query->where('product_category_id', $category);
    }

    public function scopeUsage($query, $usage)
    {
        return $query->where('usage_category', $usage);
    }

    public function scopeBody($query, $body)
    {
        return $query->where('body_figure', $body);
    }

    public function scopeStartPrice($query, $start_price)
    {
        return $query->where('four_day_rent_price', '>=', $start_price);
    }

    public function scopeEndPrice($query, $end_price)
    {
        return $query->where('four_day_rent_price', '<=', $end_price);
    }

    public function scopePricesRange($query, $prices)
    {
        if (strpos($prices, '-') === false) {
            return $query->where('four_day_rent_price', '>=', (int)$prices);
        } else {
            return $query->whereBetween('four_day_rent_price', explode('-', $prices));
        }
    }

    public function scopeRentRange($query)
    {
        return $query->whereNotIn('id', function($query){
            $query->select('products.id')
                ->from('products')
                ->leftJoin('unavaliable_rent_dates', 'products.id', '=', 'unavaliable_rent_dates.product_id')
                ->whereBetween('unavaliable_rent_dates.start_date', [\Input::get('rent_start', null), \Input::get('rent_end', null)])
                ->orWhereBetween('unavaliable_rent_dates.end_date', [\Input::get('rent_start', null), \Input::get('rent_end', null)]);
        });
    }

    public function scopePropertyGroup($query, $group_id)
    {
        return $query->where('property_group_id', $group_id);
    }

    public function scopeName($query, $name)
    {
        return $query->whereIn('id', function($query) use($name) {
            $query->select('products.id')
                ->from('products')
                ->leftJoin('product_categories', 'products.product_category_id', '=', 'product_categories.id')
                ->where('products.name', 'LIKE', "%{$name}%")
                ->orWhere('intro', 'LIKE', "%{$name}%")
                ->orWhere('describe', 'LIKE', "%{$name}%")
                ->orWhere('product_categories.name', 'LIKE', "%{$name}%");
        });
    }

    public function scopeNameGroup($query, $group_id)
    {
        return $query->where('name_group_id', $group_id)->groupBy('property_group_id');
    }

    public function scopeNameColor($query, $group_id, $size)
    {
        return $query->whereRaw("name_group_id = {$group_id} AND size = {$size}")->groupBy('property_group_id');;
    }

    public function scopeUpLevelProducts($query, $order)
    {

        $rentDay = $order->rent_days;
        $orderStart = $order->rent_start_date;
        $orderEnd = $order->rent_end_date;
        $currentPrice = $rentDay == 4 ? $order->product->four_day_rent_price : $order->product->eight_day_rent_price;
        if ($currentPrice <= 500) {
            $nextLevel = 500;
        } elseif ($currentPrice > 500 && $currentPrice <= 2000) {
            $nextLevel = 2000;
        } elseif ($currentPrice > 2000) {
            $nextLevel = 5000;
        }
        return $query->where(function($query) use($rentDay, $nextLevel) {
                    if ($rentDay == 4) {
                        $query->where('products.four_day_rent_price', '>', $nextLevel);
                    }
                    $query->where('products.eight_day_rent_price', '>', $nextLevel);
                })->whereNotIn('id', function($query) use ($orderStart, $orderEnd){
                    $query->select('products.id')
                        ->from('products')
                        ->leftJoin('unavaliable_rent_dates', 'products.id', '=', 'unavaliable_rent_dates.product_id')
                        ->whereBetween('unavaliable_rent_dates.start_date', [$orderStart, $orderEnd])
                        ->orWhereBetween('unavaliable_rent_dates.end_date', [$orderStart, $orderEnd]);
                });
    }

    public function defaultImage()
    {
        $images = json_decode($this->images, true);
        return trim($images[0], '/');
    }

    public function scopeAvaliable($query)
    {
        return $query->where('is_available', 'Y');
    }

    public function original_images()
    {
        $images = json_decode($this->images, true);
        foreach ($images as &$image) {
            $image = str_replace(config('services.qiniu.water_mask'), '', $image);
            $image = str_replace(config('services.qiniu.domain_name') . '/', '', $image);
        }
        return $images;
    }
}
