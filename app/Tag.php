<?php 

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends BaseModel
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	
    protected $fillable = [
    	'name',
        'style_id',
    ];

    public function style()
    {
        return $this->belongsTo(Style::class);
    }
}
