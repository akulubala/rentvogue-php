<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WechatMenu extends Model
{
    protected $fillable = ['menu_name', 'menu_level','parent_id', 'url', 'is_active', 'sort'];

    public function scopeSubMenus($query)
    {
        $id = $this->id;
        $query->where('parent_id', '=', $id)->orderBy('sort', 'asc');
        return $query;
    }
}
