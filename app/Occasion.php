<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occasion extends BaseModel
{
    protected $fillable = [
    	'name',
    	'image',
        'english_name',
        'intro',
        'recommend',
        'order',
    ];

    public function products()
    {
        return $this->HasMany(Product::class);
    }

    public function image()
    {
        return $this->image ? env('QINIU_DOMAIN_NAME') . '/' . $this->image : null;
    }

    public function scopeRecommend($query)
    {
        return $query->where('recommend', 1);
    }
}
