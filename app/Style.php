<?php

namespace App;

class Style extends BaseModel
{
    protected $fillable = ['name'];

    public function tags()
    {
        return $this->HasMany(Tag::class);
    }
}
