<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'user_id',
        'contact_user_name', 
        'contact_user_cell_phone', 
        'city',
        'district',
        'details',
        'is_default',
    ];
}
