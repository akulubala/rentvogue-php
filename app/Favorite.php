<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model 
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	
	protected $fillable = [
		'user_id',
		'product_name_group_id',
	];

    public function product()
    {
    	return $this->belongsTo(Product::class, 'product_name_group_id', 'name_group_id');
    }

}
