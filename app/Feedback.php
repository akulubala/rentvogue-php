<?php 

namespace App;

class Feedback extends BaseModel {

	protected $fillable = [
		'user_id',
		'details',
	];

	public function user()
    {
        return $this->belongsTo(User::class);
    }
}
