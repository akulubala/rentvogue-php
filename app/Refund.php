<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model {

	protected $fillable = [
		'order_id',
		'express_company',
		'express_number'
	];

}
