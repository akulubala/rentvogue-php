<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OAuth extends Model
{
    protected $table = 'oauths';
    protected $fillable = [
                            'user_id',
                            'oauth_type_name',
                            'openid',
                            'unionid',
                            'oauth_access_token',
                            'oauth_nick_name',
                            'avatar'
                           ];
}
