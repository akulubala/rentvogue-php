<?php

namespace App;

use App\OAuth;
use App\OrderSmsLog;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;


class User extends BaseModel implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'name',
                            'nick_name',
                            'cell_phone',
                            'id_number',
                            'body_height',
                            'body_weight',
                            'body_figure',
                            'clothes_size',
                            'body_chest',
                            'waistline',
                            'body_hipline',
                            'outseam',
                            'is_active',
                            'verify_status',
                            'user_device_id',
                            'password',
                            'primary_city',
                            'avatar',
                            'id_card_frontend',
                            'id_card_backend',
                            'invite_user',
                            'invite_code',
                          ];
    protected $appends = ['weixin_avatar', 'weibo_avatar'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'is_active'];


    public function getAvatarAttribute($value)
    {
        if ($value) {
            return env('QINIU_DOMAIN_NAME') . '/' . $value;
        } else {
            if ($this->weixinAvatar) {
                return $this->weixinAvatar;
            }
            if ($this->weiboAvatar) {
                return $this->weiboAvatar;
            }
            return null;
        }
    }

    public function getIdCardFrontendAttribute($value)
    {
        return $value? env('QINIU_DOMAIN_NAME') . '/' . $value : null;
    }

    public function getIdCardBackendAttribute($value)
    {
        return $value? env('QINIU_DOMAIN_NAME') . '/' . $value : null;
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function addresses()
    {
        return $this->HasMany(Address::class);
    }
    public function favorites()
    {
        return $this->HasMany(Favorite::class);
    }
    public function invite_users()
    {
        return $this->HasMany(User::class, 'invite_user', 'id');
    }
    public function getIsActive()
    {
        return $this->is_active == 'Y' ? '正常' : '禁用';
    }
    public function verify_status()
    {
        switch ($this->verify_status) {
            case 1:
                return '已上传';
            case 2:
                return '未通过';
            case 3:
                return '已通过';
            default:
                return '未上传';
        }
    }
    public function coupons()
    {
        return $this->hasMany(Coupon::class, 'cell_phone', 'cell_phone');
    }

    public function sms()
    {
        return $this->hasMany(OrderSmsLog::class);
    }

    public function oauths()
    {
        return $this->hasMany(OAuth::class, 'user_id', 'id');
    }

    public function getWeixinAvatarAttribute()
    {

        $weixin = $this->oauths()
                    ->where('oauth_type_name', 'wechat')
                    ->first();
        return $weixin ? $weixin->avatar : null;
    }

    public function getWeiboAvatarAttribute()
    {
        $weibo = $this->oauths()
                    ->where('oauth_type_name', 'sina_weibo')
                    ->first();
        return $weibo ? $weibo->avatar : null;
    }

    public function scopeSixMonthsCounts()
    {
        $results = array();
        $startDay = Carbon::now()->addMonths(-6);
        $endDay = Carbon::now();
        $i = 0;
        while ($endDay->diffInDays($startDay, false) < 0) {
            $endWeekDay = $startDay->addDay(7);
            $startWeekDay = Carbon::parse($endWeekDay)->addDay(-7);
            $k = $startWeekDay->format('m-d') . ' ~ ' . $endWeekDay->format('m-d');
            $results['x_line'][$i] = $k;
            $results['y_line'][$i] = $this->whereBetween('updated_at', [$startWeekDay, $endWeekDay])->count();
            $i++;
        }
        return $results;
    }
}
