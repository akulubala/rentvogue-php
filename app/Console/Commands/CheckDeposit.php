<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\OrderSmsLog;

class CheckDeposit extends Command
{
    private $order;
    private $orderSmsLog;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkDeposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '检查未支付押金的订单，三天内需要发货的订单发送提醒支付信息';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Order $order, OrderSmsLog $orderSmsLog)
    {
        parent::__construct();
        $this->order = $order;
        $this->orderSmsLog = $orderSmsLog;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unpaid_deposits = $order->where('rent_start_date', '<=', Carbon::now()->addDay(3))
                                ->where('rent_start_date', '>', Carbon::now())
                                ->get();

        foreach ($unpaid_deposits as $unpaid_deposit) {
            $cellPhone = $unpaid_deposit->user->cell_phone;
            $messageTitle = "尊敬{$unpaid_deposit->user->name}会员您好，您{$unpaid_deposit->order_number}订单的服装已经准备完毕即将发货，为了您的正常使用，请务必在72小时内支付押金，我们收到您押金后将准时发出。如有任何疑问请及时与我们联系。";
            // send sms
            $message = $messageTitle. '【Rentvogue】';
            luosimao_sms($cellPhone, $message);
            // send push notice
            $deviceId = $unpaid_deposit->user->user_device_id;
            if ($deviceId !== null) {
                try {
                    $client = new JPush(config('services.jpush.app_key'), config('services.jpush.master_secret'), storage_path('logs/jpush.log'));
                    $result = $client->push()
                                ->setPlatform('ios')
                                ->addAllAudience()
                                ->addIosNotification($message, 'iOS sound', JPush::DISABLE_BADGE, true, 'iOS category', array("type"=>"order_change", "order_id"=>$unpaid_deposit->id))
                                ->send();
                } catch(\Exception $e) {
                    custom_logs('jpush', $e->getMessage());
                }

            }
            $orderSmsLog = $this->orderSmsLog->create([
                'user_id' => $unpaid_deposit->user_id,
                'order_id' => $unpaid_deposit->id,
                'title' => $messageTitle,
                'type' => $unpaid_deposit->order_status,
                'content' => $message,
            ]);
        }
    }
}
