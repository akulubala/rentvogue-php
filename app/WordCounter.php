<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordCounter extends Model {

    protected $table = 'word_counter';

	protected $fillable = [
		'word'
	];

}
