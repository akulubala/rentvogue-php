var elixir = require('laravel-elixir');
require('laravel-elixir-clean');
require('laravel-elixir-styles-with-url-replace');
elixir(function(mix) {
    //admin.css
    mix.styles_with_url_replace([
        'vendor/admin_tpl/assets/skin/default_skin/css/theme.css',
        'vendor/admin_tpl/assets/admin-tools/admin-forms/css/admin-forms.css',
        'vendor/admin_tpl/assets/fonts/glyphicons-pro/glyphicons-pro.css',
        'vendor/admin_tpl/assets/fonts/icomoon/icomoon.css',
        'vendor/admin_tpl/assets/fonts/iconsweets/iconsweets.css',
        'vendor/admin_tpl/assets/fonts/octicons/octicons.css',
        'vendor/admin_tpl/assets/fonts/stateface/stateface.css',
        'vendor/admin_tpl/vendor/plugins/magnific/magnific-popup.css',
        'vendor/admin_tpl/vendor/plugins/xeditable/css/bootstrap-editable.css',
        'vendor/admin_tpl/vendor/plugins/select2/css/core.css',
        'vendor/admin_tpl/vendor/plugins/select2/css/theme/default/layout.css',
        'vendor/bower_components/cropper/dist/cropper.min.css',
        'css/admin.css',
        'css/crop.css',
    ], 'complied/admin.css', 'public');
    //admin.js
    mix.scripts([
        'vendor/admin_tpl/vendor/jquery/jquery-1.11.1.min.js',
        'vendor/admin_tpl/vendor/jquery/jquery_ui/jquery-ui.min.js',
        'vendor/bower_components/json2/json2.js',
        'vendor/admin_tpl/assets/js/utility/utility.js',
        'vendor/admin_tpl/vendor/plugins/xeditable/js/bootstrap-editable.min.js',
        'vendor/admin_tpl/vendor/plugins/magnific/jquery.magnific-popup.js',
        'vendor/admin_tpl/vendor/plugins/select2/select2.min.js',
        'vendor/admin_tpl/assets/js/main.js',
        'vendor/bower_components/underscore/underscore-min.js',
        'js/jquery.custom.js',
        'js/admin.js',
    ], 'public/complied/admin.js', 'public');

    mix.clean('public/build/tmp');

    mix.version([
        'public/complied/admin.css',
        'public/complied/admin.js'
    ]);
});
