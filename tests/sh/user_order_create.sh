#!/bin/bash
if [[ $1 == 'local' ]]
then
    host='my.rentvogue.com'
else
    host='121.201.14.60:8080'
fi
token=$(cat token)
http --json --form POST http://${host}/api/v1/user/6/order "Authorization: Bearer ${token}" \
address_id='3' \
exchange_order_id='20' \
property_group_id='8' \
rent_days='4' \
rent_end_date='2016-01-23' \
rent_start_date='2016-01-20'

