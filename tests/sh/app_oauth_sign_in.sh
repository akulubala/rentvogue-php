#!/bin/bash
if [[ $1 == 'local' ]]
then
    host='my.rentvogue.com'
else
    host='121.201.14.60:8080'
fi
http --form POST http://${host}/api/v1/authenticate/app-oauth \
access_token='token' \
openid='xxxxxx' \
unionid='xxxx' \
oauth_type='weixin'
