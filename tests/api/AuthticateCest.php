<?php
class AuthticateCest
{
    protected $cell_phone;
    protected $endpoint = '/api/v1';
    private $verfiyCode;

    public function __construct(){
        $this->cell_phone = '22222222222';
    }
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {

    }

    public function sendSms(ApiTester $I)
    {
        $I->wantTo('get sms code');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGET(route('get_cell_verify_code', ['phone_number' => $this->cell_phone]));
        $I->seeResponseCodeIs(200);
        $this->verfiyCode = $I->grabDataFromResponseByJsonPath("")[0]['verify_code'];
    }

    public function createNewUser(ApiTester $I)
    {
        $I->wantTo('create a new user via api');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPOST(route('auth.sign_up'),
            [
                'nick_name' => 'test',
                 'cell_phone' => $this->cell_phone,
                'cell_phone_verify_code' => $this->verfiyCode,
                'password' => '11111111',
                'password_confirmation' => '11111111'
            ]
        );
        $I->seeResponseCodeIs(201);
    }

    public function signIn(ApiTester $I)
    {
        $I->wantTo('sign in');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendPOST(route('auth.sign_in'),
            [
                'cell_phone' => '13323643838',
                'password' => '11111111',
            ]
        );
        $I->seeResponseCodeIs(200);
    }

}
