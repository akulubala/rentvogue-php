<?php
return [
    'wechat' => [
        'subscribe' => "
        你太有眼光了！与百年顾世家一起开启你的定制护肤之旅吧
        <a href=':home' >（马上开启你的定制之旅吧）</a>
        即可为您免费定制专属护肤方案，体验革命性的护肤新体验！
        <a href=':invitation' >（马上输入邀请码，获得专属福利！）</a>",
        'source' => [
            'subscribe' => '关注'
        ]
    ],
    'login' => ':name后台登陆',
    'question_solution' => [
        'none' => '没有',
        'little' => '很少',
        'sometimes' => '有时候',
        'frequently' => '经常',
        'always' => '总是'
    ]
];
