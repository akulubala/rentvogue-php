<?php

return [
    "token_invalid" => "token不正确",
    "token_expired" => "token已过期",
    "token_missing" => "token不能为空",
    "user_not_found" => "用户不存在或已被清除"
];
