<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    "accepted"         => ":attribute 必须接受。",
    "active_url"       => ":attribute 不是一个有效的网址。",
    "after"            => ":attribute 必须是一个在 :date 之后的日期。",
    "alpha"            => ":attribute 只能由字母组成。",
    "alpha_dash"       => ":attribute 只能由字母、数字和斜杠组成。",
    "alpha_num"        => ":attribute 只能由字母和数字组成。",
    "array"            => ":attribute 必须是一个数组。",
    "before"           => ":attribute 必须是一个在 :date 之前的日期。",
    "between"          => [
        "numeric" => ":attribute 必须介于 :min - :max 之间。",
        "file"    => ":attribute 必须介于 :min - :max kb 之间。",
        "string"  => ":attribute 必须介于 :min - :max 个字符之间。",
        "array"   => ":attribute 必须只有 :min - :max 个单元。",
    ],
    "boolean"          => ":attribute 必须为布尔值。",
    "confirmed"        => ":attribute 两次输入不一致。",
    "date"             => ":attribute 不是一个有效的日期。",
    "date_format"      => ":attribute 的格式必须为 :format。",
    "different"        => ":attribute 和 :other 必须不同。",
    "digits"           => ":attribute 必须是 :digits 位的数字。",
    "digits_between"   => ":attribute 必须是介于 :min 和 :max 位的数字。",
    "email"            => ":attribute 不是一个合法的邮箱。",
    "exists"           => ":attribute 不存在。",
    "filled"           => ":attribute 不能为空。",
    "image"            => ":attribute 必须是图片。",
    "in"               => "已选的属性 :attribute 非法。",
    "integer"          => ":attribute 必须是整数。",
    "ip"               => ":attribute 必须是有效的 IP 地址。",
    "max"              => [
        "numeric" => ":attribute 不能大于 :max。",
        "file"    => ":attribute 不能大于 :max kb。",
        "string"  => ":attribute 不能大于 :max 个字符。",
        "array"   => ":attribute 最多只有 :max 个单元。",
    ],
    "mimes"            => ":attribute 必须是一个 :values 类型的文件。",
    "min"              => [
        "numeric" => ":attribute 必须大于等于 :min。",
        "file"    => ":attribute 大小不能小于 :min kb。",
        "string"  => ":attribute 至少为 :min 个字符。",
        "array"   => ":attribute 至少有 :min 个单元。",
    ],
    "not_in"           => "已选的属性 :attribute 非法。",
    "numeric"          => ":attribute 必须是一个数字。",
    "regex"            => ":attribute 格式不正确。",
    "required"         => ":attribute 不能为空。",
    "required_if"      => "当 :other 为 :value 时 :attribute 不能为空。",
    "required_with"    => "当 :values 存在时 :attribute 不能为空。",
    "required_with_all" => "当 :values 存在时 :attribute 不能为空。",
    "required_without" => "当 :values 不存在时 :attribute 不能为空。",
    "required_without_all" => "当 :values 都不存在时 :attribute 不能为空。",
    "same"             => ":attribute 和 :other 必须相同。",
    "size"             => [
        "numeric" => ":attribute 大小必须为 :size。",
        "file"    => ":attribute 大小必须为 :size kb。",
        "string"  => ":attribute 必须是 :size 个字符。",
        "array"   => ":attribute 必须为 :size 个单元。",
    ],
    "string"           => ":attribute 必须是一个字符串。",
    "timezone"         => ":attribute 必须是一个合法的时区值。",
    "unique"           => "该:attribute已被注册了。",
    "url"              => ":attribute 格式不正确。",
    "admin_login" => "用户名或者密码错误！",
    'counselor_login' => "手机号或密码错误!",
    "admin_password_check" => ":attribute 验证失败。",
    "user_password_check" => ":attribute 不正确。",
    "admin_user_name_unique" => ":attribute 已经存在。",
    "image_resolution" => ":attribute 尺寸应为 :width x :height 像素。",
    "cell_phone" => ":attribute 格式不正确。",
    "cell_phone_verify_code" => ":attribute 错误。",
    "greater_then" => ":attribute 必须大于 :min。",
    "wechat_menu_limit" => "微信一级菜单最多三个，二级最多5个",
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => [
        'rent_days' => [
            'rent_days_range' => '所选日期时间不合法',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [
        'password' => '密码',
        'old_password' => '原密码',
        'current_password' => '原始密码',
        'new_password' => '新密码',
        're_password' => '重复密码',
        'user_name' => '用户名',
        'age' => '年龄',
        'gender' => '性别',
        'title' => '标题',
        'content' => '详情',
        'state' => '状态',
        'thumbnail' => '缩略图',
        'cell_phone' => '手机号',
        'email' => '邮箱',
        'name' => '姓名',
        'introduction' => '简介',
        'avatar' => '头像',
        'image' => '图片',
        'series' => '系列',
        'url' => '链接',
        'sort' => '排序',
        'order' => '次序',
        'menu_name' => '菜单名称',
        'product_category_id' => '分类',
        'product_id' => '产品',
        'product' => '产品',
        'amount' => '产品数量',
        'detect_result_id' => '诊断结果',
        'tongue_describe' => '舌苔描述',
        'member_name' => '收货人',
        'contact_phone' => '联系电话',
        'province' => '省',
        'city' => '市',
        'district' => '区',
        'address_detail' => '详细地址',
        'address_id' => '收货地址',
        'price' => '价格',
        'picture' => '产品图片',
        'product_name' => '产品名称',
        'component' => '成分',
        'efficacy' => '功效',
        'express' => '快递公司',
        'express_company' => '快递公司',
        'express_number' => '快递单号',
        'courier_number' => '快递单号',
        'copies' => '份数',
        'reuse_time' => '可重用次数',
        'dead_date' => '截止日期',
        'usage_category' => '分类',
        'color' => '颜色',
        'four_day_rent_price' => '四天租金',
        'eight_day_rent_price' => '八天租金',
        'color_id' => '颜色',
        'deposit' => '押金',
        'describe' => '描述',
        'images' => '图片',
        'suit_occasion' => '适用场合',
        'contact_user_name' => '收件人',
        'contact_user_cell_phone' => '联系方式',
        'details' => '详细地址',
        'size' => '尺寸',
        'occasion' => '场合',
        'cell_phone_verify_code' => '手机验证码',
        'nick_name' => '昵称',
        'password_confirmation' => '确认密码',
        'supplier' => '供应商',
        'supplier_phone' => '供应商电话',
        'supplier_address' => '供应商地址',
        'style_id' => '款式',
        'market_price' => '市场价格',
        'factory_id' => '产品编码',
        'body_figure' => '体型',
        'start_date' => '起始日期',
        'end_date' => '结束日期',
    ],
];
