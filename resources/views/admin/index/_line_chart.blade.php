<div class="panel">
    <div class="panel-heading ui-sortable-handle">
        <span class="panel-title">数据统计图</span>
    </div>
    <div class="panel-menu pn bg-white">
        <ul class="nav nav-justified text-center fw600 chart-legend" id='chart_nav_tab'>
          <li>
            <a href="javascript:void()" class="legend-item active" >半年用户注册增量统计</a>
          </li>
          <li class="br-l">
            <a  href="javascript:void()" class="legend-item" >半年产品销量统计</a>
          </li>
          <li class="br-l">
            <a href="javascript:void()" class="legend-item" >半年产品销量金额统计</a>
          </li>
        </ul>
    </div>
    <div class="panel-body pn" id="chart_nav_content">
        <div id="users" style="width: 100%;height: 400px;margin: 0 auto;"></div>
        <div id="orders" style="width: 100%; height: 400px; margin: 0 auto;"></div>
        <div id="amounts" style="width: 100%;height: 400px;margin: 0 auto;"></div>
    </div>

</div>
@section('js')
<script src="/vendor/admin_tpl/vendor/plugins/highcharts/highcharts.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#chart_nav_content").children('div').eq(0).siblings().css('display', 'none');
    $("#chart_nav_tab li a").click(function() {
        var indexLi = $(this).parent('li').index();
        $("#chart_nav_content").children('div').eq(indexLi).css('display', 'block').siblings().css('display', 'none');
    });
});
</script>
@include('admin.index._users')
@include('admin.index._orders');
@include('admin.index._amounts');
@stop
