@extends('layouts.admin')
@section('sub_title')
后台管理
@stop
@section('content')
<div id="content">
        @include('admin.index._table_datas')
        @include('admin.index.delivery_next_week')
        @include('admin.index._line_chart')
    </div>
@stop
