<div class="panel">
    <div class="panel-heading">
        <div class="panel-title">
            <span class="glyphicon glyphicon-tasks"></span>
            数据统计
        </div>
    </div>
 <div class="panel-body pn">
    <div class="table-responsive">
      <div class="bs-component">
        <table class="table table-bordered mbn">
          <tbody>
            <tr>
              <td class="col-md-2">产品总数:</td>
              <td> <a href="{{ route('admin.product.index') }}" target="_blank">{{ $productCounts }}</a> </td>
            </tr>
            <tr>
              <td class="col-md-2">订单总数:</td>
              <td><a target="_blank" href="{{ route('admin.order.index') }}">{{ $orderCounts }}</a></td>
            </tr>
            <tr>
              <td class="col-md-2">用户总数:</td>
              <td><a href="{{ route('admin.user.index') }}" target="_black">{{ $userCounts }}</a></td>
            </tr>
          {{--   <tr>
              <td class="col-md-2">
                    Top 3 最受欢迎产品:
              </td>
              <td>
                @if (!$favorite3Products->isEmpty())
                    @foreach ($favorite3Products as $favorite)
                      <a target="_blank" href="{{ config('app.url') . '/index.html#!/goods?id=' . $favorite->product->name_group_id }}">{{ $favorite->product ? $favorite->product->name : '' }}</a>,
                    @endforeach
                @endif
              </td>
            </tr> --}}
            <tr>
                <td class="col-md-2">最新付款订单</td>
                <td>
                    <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">订单编号</th>
                                    <th class="text-center">产品名称</th>
                                    <th class="text-center">用户</th>
                                    <th class="text-center">租用时间</th>
                                    <th class="text-center">租金</th>
                                    <th class="text-center">押金</th>
                                    <th class="text-center">优惠券</th>
                                    <td class="text-center">订单详情</td>
                                    <td class="text-center">订单状态</td>
                                </tr>
                            </thead>
                        @if (!$fivePaidOrders->isEmpty())
                            @foreach ($fivePaidOrders as $order)
                                <tr>
                                    <td class='text-center'>{{ $order->id }}</td>
                                    <td class='text-center'>
                                        <a href="{{ route('admin.order.show', $order->id) }}">{{ $order->order_number }}</a>
                                    </td>
                                    <td class='text-center'>{{ $order->product->name }}</td>
                                    <td class='text-center'>{{ $order->user ? $order->user->nick_name : '' }}</td>
                                    <td class='text-center' style="width:200px">
                                        {{ $order->rent_start_date }}&nbsp; ~ &nbsp;{{ $order->rent_end_date }}
                                    </td>
                                    <td class="text-center">{{ $order->rent_days == 4 ? $order->product->four_day_rent_price : $order->product->eight_day_rent_price }}</td>
                                    <td class="text-center">{{ $order->product->deposit }}</td>
                                    <td class="text-center">{{ $order->coupon_id ? $order->coupon->config->cut : '' }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.order.show', $order->id) }}">查看</a>
                                    </td>
                                    <td class="text-center">
                                    {{ config('system.database.order_status_exchange')[config('system.database.order_status')[$order->order_status - 1]] }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>
          </tbody>
        </table>
      <div id="source-button" class="btn btn-primary btn-xs" style="display: none;">&lt; &gt;</div></div>
    </div>
  </div>
</div>
