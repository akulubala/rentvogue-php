<div class="panel">
    <div class="panel-heading">
        <div class="panel-title">
            <span class="glyphicon glyphicon-tasks"></span>
            近一周需要发货订单
        </div>
    </div>
 <div class="panel-body pn">
    <div class="table-responsive">
      <div class="bs-component">
      <table class="table table-bordered table-striped">
          <thead>
              <tr>
                  <th class="text-center">#</th>
                  <th class="text-center">订单编号</th>
                  <th class="text-center">产品名称</th>
                  <th class="text-center">用户</th>
                  <th class="text-center">租用时间</th>
                  <th class="text-center">收货人电话</th>
                  <th class="text-center">收货人地址</th>
                  <th class="text-center">租金</th>
                  <th class="text-center">押金</th>
                  <th class="text-center">优惠券</th>
                  <td class="text-center">订单状态</td>
              </tr>
          </thead>
          @if (!$currentDeliveryOrders->isEmpty())
              @foreach ($currentDeliveryOrders as $order)
                  <tr>
                      <td class='text-center'>{{ $order->id }}</td>
                      <td class='text-center'>
                          <a href="{{ route('admin.order.show', $order->id) }}">{{ $order->order_number }}</a>
                      </td>
                      <td class='text-center'>{{ $order->product->name }}</td>
                      <td class='text-center'>{{ $order->user ? $order->user->nick_name : '' }}</td>
                      <td class='text-center' style="width:200px">
                          {{ $order->rent_start_date }}&nbsp; ~ &nbsp;{{ $order->rent_end_date }}
                      </td>
                      <td class='text-center'>{{ $order->phone }}</td>
                      <td class='text-center'>{{ $order->address }}</td>
                      <td class="text-center">{{ $order->rent_days == 4 ? $order->product->four_day_rent_price : $order->product->eight_day_rent_price }}</td>
                      <td class="text-center">{{ $order->product->deposit }}</td>
                      <td class="text-center">{{ $order->coupon_id ? $order->coupon->config->cut : '' }}</td>
                      <td class="text-center">
                      {{ config('system.database.order_status_exchange')[config('system.database.order_status')[$order->order_status - 1]] }}
                      </td>
                  </tr>
              @endforeach
          @endif
      </table>
      <div id="source-button" class="btn btn-primary btn-xs" style="display: none;">&lt; &gt;</div></div>
    </div>
  </div>
   <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $currentDeliveryOrders->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
</div>
