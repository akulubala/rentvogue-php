<script>
var HighChartLine = $('#users');
if (HighChartLine.length) {

    HighChartLine.highcharts({
        credits: true,
        colors: ['#5BC24C', '#8362D6', '#F5B025', '#3078D7'],
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true,
                },
                enableMouseTracking: true,
            },
        },
        xAxis: {
                gridLineColor: '#EEE',
                lineColor: '#EEE',
                tickColor: '#EEE',
                categories: [{!! '"'.implode($users['x_line'], '","').'"' !!}],
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
        },
        yAxis: {
            gridLineColor: '#EEE',
            title: {
                text: '数量',
            }
        },
        legend: {
            enabled: true,
        },
        series: [{
            name: '注册用户',
            data: [{{ implode($users['y_line'], ',') }}]
        }]
    });
}
</script>
