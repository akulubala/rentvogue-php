@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    颜色列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">颜色RGB值</th>
                                <th class="text-center">颜色示例</th>
                                <th class="text-center">排序</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$colors->isEmpty())
                                @foreach($colors as $color)
                                    <tr class="text-center">
                                        <td>{{ $color->id }}</td>
                                        <td>{{ $color->rgb }}</td>
                                        <td><div style="margin:0 auto;width:20px;height:20px;background-color:{{ $color->rgb }};"></div></td>
                                        <td>{{ $color->order_sequence }}</td>
                                        <td>{{ $color->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.color.edit', $color) }}">编辑</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.color.destroy', $color) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $colors->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
