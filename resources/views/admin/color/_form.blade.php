@if (Route::currentRouteName() === 'admin.color.create')
  {!! Form::open(['route' => 'admin.color.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($color, ['route' => ['admin.color.update', $color->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="rgb" class="control-label col-sm-2">颜色RGB值</label>
        <div class="col-sm-10">
            {!! Form::text('rgb', null, ['id' => 'mycolor', 'placeholder' => '请输入颜色RGB值']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="rgb" class="control-label col-sm-2">排序</label>
        <div class="col-sm-10">
            {!! Form::text('order_sequence', null, ['placeholder' => '请输入排序']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.color.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('vendor/admin_tpl/vendor/jquery/jquery_ui/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('vendor/bower_components/evol.colorpicker/css/evol.colorpicker.min.css') }}">
@stop
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/evol.colorpicker/js/evol.colorpicker.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#mycolor').colorpicker({
        strings: '主题颜色,标准颜色,网页颜色,主题颜色,返回调色板,使用过的,无使用过的'
    });
});
</script>
@stop
