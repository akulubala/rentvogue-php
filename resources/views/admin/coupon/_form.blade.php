@if (Route::currentRouteName() === 'admin.coupon_config.create')
  {!! Form::open(['route' => 'admin.coupon_config.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($coupon, ['route' => ['admin.coupon_config.update', $coupon->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="group" class="control-label col-sm-2">group</label>
        <div class="col-sm-10">
            {!! Form::text('group', $group, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-sm-2">优惠券名称</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入优惠券名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="need" class="control-label col-sm-2">满</label>
        <div class="col-sm-10">
            {!! Form::text('need', null, ['class' => 'form-control', 'placeholder' => '请输入满金额']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="cut" class="control-label col-sm-2">减</label>
        <div class="col-sm-10">
            {!! Form::text('cut', null, ['class' => 'form-control', 'placeholder' => '请输入减金额']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="available_days" class="control-label col-sm-2">有效天数</label>
        <div class="col-sm-10">
            {!! Form::text('available_days', null, ['class' => 'form-control', 'placeholder' => '请输入有效天数']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="amount" class="control-label col-sm-2">数量</label>
        <div class="col-sm-10">
            {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => '请输入数量']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.coupon_config.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
