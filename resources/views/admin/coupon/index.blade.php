@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    套券列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn admin-form theme-info">
                            <thead>
                            <tr>
                                <th class="text-center">套券名称</th>
                                <th class="text-center">group</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr class="text-center">
                                    <td>{{ $coupon->name }}</td>
                                    <td class="w200">{{ $coupon->group }}</td>
                                    <td>{{ $coupon->created_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.coupon_config.show', $coupon->group) }}">查看</a>
                                        <span class="separator"></span>
                                        <a href="javascript:void(0);" class="btn-option" option='delete' data-key="group" data-v="{{ $coupon->group }}" data-url="{{ route('admin.coupon_config.destroy', $coupon->group) }}">删除</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $coupons->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $( ".magnific-container" ).each(function(){
        $(this).magnificPopup({
          delegate: 'a', // child items selector, by clicking on it popup will open
          type: 'image',
          gallery:{enabled:true}
        });
    });
});
</script>
@stop
