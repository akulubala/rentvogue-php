@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    套券内优惠券列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="panel-body bg-light admin-form">
                    <div class="col-md-4">
                        <a href="{{ route('admin.coupon_config.create') . '?group=' . $group }}" class="btn btn-hover btn-primary btn">
                            套券新建优惠券
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn admin-form theme-info">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">优惠券名称</th>
                                <th class="text-center">满</th>
                                <th class="text-center">减</th>
                                <th class="text-center">有效天数</th>
                                <th class="text-center">数量</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">更新时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr class="text-center">
                                    <td>{{ $coupon->id }}</td>
                                    <td>{{ $coupon->name }}</td>
                                    <td>{{ $coupon->need }}</td>
                                    <td>{{ $coupon->cut }}</td>
                                    <td>{{ $coupon->available_days }}</td>
                                    <td>{{ $coupon->amount }}</td>
                                    <td>{{ $coupon->created_at }}</td>
                                    <td>{{ $coupon->updated_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.coupon_config.edit', $coupon) }}">编辑</a>
                                        <span class="separator"></span>
                                        <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.coupon_config.destroy', $coupon) }}">删除</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $coupons->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $( ".magnific-container" ).each(function(){
        $(this).magnificPopup({
          delegate: 'a', // child items selector, by clicking on it popup will open
          type: 'image',
          gallery:{enabled:true}
        });
    });
});
</script>
@stop
