<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>{{ trans('global.login', ['name' => '婚纱租赁']) }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{ elixir('complied/admin.css') }}">
</head>

<body class="external-page external-alt sb-l-c sb-r-c">
  <div id="main" class="animated fadeIn">
    <section id="content_wrapper">
      <section id="content">
        <div class="admin-form theme-info mw500" id="login">
          @include('partials.admin.errors')
          <div class="panel mt30 mb25">
            {!! Form::open(['route' => 'admin.login.sign_in', 'id' => 'contact']) !!}
              <div class="panel-body bg-light p25 pb15">
                <div class="section">
                  <label for="username" class="field-label text-muted fs18 mb10">用户名</label>
                  <label for="username" class="field prepend-icon">
                    <input type="text" name="user_name" id="username" class="gui-input" placeholder="请输入用户名">
                    <label for="username" class="field-icon">
                      <i class="fa fa-user"></i>
                    </label>
                  </label>
                </div>
                <div class="section">
                  <label for="password" class="field-label text-muted fs18 mb10">密码</label>
                  <label for="password" class="field prepend-icon">
                    <input type="password" name="password" id="password" class="gui-input" placeholder="请输入登录密码">
                    <label for="password" class="field-icon">
                      <i class="fa fa-lock"></i>
                    </label>
                  </label>
                </div>
              </div>
              <div class="panel-footer clearfix">
                <button type="submit" class="button btn-primary mr10 pull-right">登录</button>
                <label class="switch block switch-primary mt10" style="width:150px;">
                  <input type="checkbox" name="remember" id="remember" checked>
                  <label for="remember" data-on="是" data-off="否"></label>
                  <span>记住我</span>
                </label>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </section>
    </section>
  </div>
  <script type="text/javascript" src="{{ elixir('complied/admin.js') }}"></script>
</body>
</html>
