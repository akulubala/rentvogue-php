<td class="text-center">
  <div class="btn-group text-right">
    <button type="button" class="btn
    @if ($occasion->recommend == 1) btn-success @else btn-warning @endif
    br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      @if ($occasion->recommend == 1) 推荐 @else 不推荐 @endif
      <span class="caret ml5"></span>
    </button>
    <ul class="dropdown-menu" role="menu" style="left: inherit;right:0">
        <li>
          <a href="{{ route('admin.occasion.edit', $occasion) }}">编辑</a>
        </li>
        <li>
          <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.occasion.destroy', $occasion) }}">删除</a>
        </li>
        <li class="divider"></li>
        <li @if ($occasion->recommend == 1) class="active" @endif>
             <a href="javascript:void(0);" class="btn-option" option='put' data-key="recommend" data-v="1"  data-url="{{ route('admin.occasion.update', $occasion) }}">
                推荐
             </a>
        </li>
        <li @if ($occasion->recommend == 0) class="active" @endif>
            <a href="javascript:void(0);" class="btn-option" option='put' data-key="recommend" data-v="0" data-url="{{ route('admin.occasion.update', $occasion) }}">
                不推荐
            </a>
        </li>
    </ul>
  </div>
</td>
