@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    编辑场合
                </div>
            </div>
            @include ('admin.occasion._form')
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
</script>
@stop
