@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    场合列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::open(['route' => 'admin.occasion.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                                <div class="form-group mr15">
                                    <label>场合名称：</label>
                                    <span>
                                        @if (!empty($filter['name']))
                                            <input type="search" class="form-control input-sm ml5" name="filter[name]" value="{{ $filter['name'] }}"/>
                                        @else
                                            <input type="search" class="form-control input-sm ml5" name="filter[name]"/>
                                        @endif
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">筛选</button>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-hover btn-primary btn-default save_order btn-sm">保存次序</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered sorted_table table-hover mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">场合名</th>
                                <th class="text-center">英文名</th>
                                <th class="text-center">简介</th>
                                <th class="text-center">封面图片</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$occasions->isEmpty())
                                @foreach($occasions as $occasion)
                                    <tr class="text-center">
                                        <td class="identify">{{ $occasion->id }}</td>
                                        <td>{{ $occasion->name }}</td>
                                        <td>{{ $occasion->english_name }}</td>
                                        <td>{{ $occasion->intro }}</td>
                                        <td>
                                            <a class="magnific-link" href="{{ $occasion->image() }}"><img src="{{ $occasion->image() }}" width="62" height="33"></a>
                                        </td>
                                        <td>{{ $occasion->created_at }}</td>
                                        @include('admin.occasion._option')
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $occasions->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/jquery-sortable/source/js/jquery-sortable-min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.magnific-link').magnificPopup({ 
      type: 'image'
      // other options
      // gallery:{enabled:true}
    });
    $('.sorted_table').sortable({
      containerSelector: 'table',
      itemPath: '> tbody',
      itemSelector: 'tr',
      placeholder: '<tr class="placeholder"></tr>'
    });
    $('.save_order').click(function() {
        var order_value = Array();
        var order = 0;
        $("table.sorted_table td.identify").each(function() {
            order_value[$(this).text()] = order++;
        });
        // console.log(order_value);
        $.ajax({
            type: "POST",
            url: "{{ route('admin.occasion.order') }}",
            data: { order_value : order_value },
        }).done(function() {
            location.reload();
        }).fail(function() {
            alert('操作失败，请稍后重试');
        });
    });
});
</script>
@stop