@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.order.index'),
            'text' => '订单管理'
        ],
        'second_level' => [
            'text' => '订单详情',
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    订单详情
                </div>
            </div>
            <div class="panel-body pn">
               <div class="table-responsive">
                  <div class="bs-component">
                    <table class="table table-bordered mbn">
                      <tbody>
                        <tr>
                          <td class="col-md-2">订单号</td>
                          <td>{{ $order->order_number }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">换货订单编号</td>
                          <td><a href="{{ route('admin.order.show', $order->parentOrder() === null ? '' : $order->parentOrder()->id) }}">{{ $order->parentOrder() === null ? '' : $order->parentOrder()->order_number }}</a></td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单状态</td>
                          <td>{{ config('system.database.order_status_exchange')[config('system.database.order_status')[$order->order_status - 1]] }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单创建时间</td>
                          <td>{{ $order->created_at }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单最后更新时间</td>
                          <td>{{ $order->updated_at }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">产品名称</td>
                          <td><a target="_blank" href="{{ config('app.url') . '/index.html#!/goods?id=' . $order->product->name_group_id }}">{{ $order->product->name }}</a></td>
                        </tr>
                        <tr>
                          <td class="col-md-2">产品颜色</td>
                          <td><div style="float:left;margin:0 auto;width:20px;height:20px;background-color:{{ $order->product->color->rgb }};"></div></td>
                        </tr>
                        <tr>
                          <td class="col-md-2">产品尺寸</td>
                          <td>{{ config('system.database.sizes')[$order->product->size] }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单用户</td>
                          <td>{{ $order->user->name }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单联系人</td>
                          <td>{{ $order->contact_user_name }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单联系地址</td>
                          <td>{{ $order->address }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单联系电话</td>
                          <td>{{ $order->phone }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">租用天数</td>
                          <td>{{ $order->rent_days }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">租用时间</td>
                          <td>{{ $order->rent_start_date . ' ~ ' . $order->rent_end_date}}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">租金</td>
                          <td>
                            {{ $order->rent_days == 4 ? $order->product->four_day_rent_price : $order->product->eight_day_rent_price }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">押金</td>
                          <td>
                            {{ $order->product->deposit }}
                          </td>
                        </tr>
                        <tr>
                          <td class="col-md-2">优惠券</td>
                          <td>
                            {{ $order->coupon_id ? $order->coupon->config->cut : '' }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">订单备注</td>
                          <td>{{ $order->remarks }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">租金Ping++支付渠道</td>
                          <td>{{ $order->pay_channel() }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">租金Ping++支付单号</td>
                          <td>{{ $order->transaction_no }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">押金Ping++支付渠道</td>
                          <td>{{ $order->deposit_channel() }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">快递公司</td>
                          <td>{{ $order->express_company() }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">快递单号</td>
                          <td><a target="_blank" href="{{ 'http://www.kuaidi100.com/chaxun?com=shunfeng&nu=' . $order->express_number}}">{{ $order->express_number }}</a></td>
                        </tr>
                        <tr>
                          <td class="col-md-2">退单拒绝原因</td>
                          <td>{{ $order->reject_cancel_reason }}</td>
                        </tr>
                        <tr>
                          <td class="col-md-2">申请订单取消时间</td>
                          <td>{{ $order->cancel_apply_date }}</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
