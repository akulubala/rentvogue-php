<td class="text-right">
@if(in_array($order->order_status, [2, 4, 5, 6, 8, 10, 16]))
  <div class="btn-group text-right">
    <button type="button"
    class="btn btn-success br2 btn-xs fs12 dropdown-toggle" style="width:148px" data-toggle="dropdown" aria-expanded="false">
        更新订单状态
      <span class="caret mr0"></span>
    </button>
    <ul class="dropdown-menu" role="menu" style="left: inherit;right:0">
        @foreach ($status as $k => $s)
            {{-- 订单已支付可进行 更换高级货 ，送货中操作 --}}
            @if ($order->order_status == 2 || $order->order_status == 16)
                @if (in_array((int)($k+1), [3, 4]))
                    <li>
                        <a class="btn-option" option='put' data-key="order_status" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @endif
            {{-- 已发货订单，可签收，可售后服务 --}}
            @elseif($order->order_status == 4)
                @if (in_array($k + 1, [5, 8]))
                    <li>
                        <a class="btn-option" option='put' data-key="order_status" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @endif
            {{-- 签收 可确认返还,可售后 --}}
            @elseif($order->order_status == 5)
                @if (in_array($k + 1, [6, 8]))
                    <li>
                        <a class="btn-option" option='put' data-key="order_status" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @endif
            {{-- 已返还 可退押金 --}}
            @elseif($order->order_status == 6)
                @if (in_array($k + 1, [7, 8]))
                    <li>
                        <a class="btn-option" option='put' data-key="order_status" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @endif
            {{-- 退单申请 可受理 及 拒绝 --}}
            @elseif($order->order_status == 10)
                @if ($k + 1 == 11)
                    <li>
                        <a class="cancel_accept" option='put' data-key="order_status" order_id="{{ $order->id }}" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @elseif ($k + 1 == 12)
                    <li>
                        <a class="btn-option" option='put' data-key="order_status" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @endif
            @elseif($order->order_status == 8)
                @if (in_array($k + 1, [5, 6]))
                    <li>
                        <a class="btn-option" option='put' data-key="order_status" data-v="{{ $s }}" data-url="{{ route('admin.order.update', $order->id) }}">
                            {{ config('system.database.order_status_exchange')[$s] }}
                        </a>
                    </li>
                @endif
            @endif
        @endforeach
    </ul>
  </div>
@endif
</td>
