@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.order.index'),
            'text' => '订单管理'
        ],
        'second_level' => [
            'text' => '订单列表',
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    订单列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">订单编号</th>
                                    <th class="text-center">产品名称</th>
                                    <th class="text-center">用户</th>
                                    <th class="text-center">租用时间</th>
                                    <th class="text-center">总租金</th>
                                    <th class="text-center">收货人电话</th>
                                    <th class="text-center">收货人地址</th>
                                    <th class="text-center">快递公司</th>
                                    <th class="text-center">快递编号</th>
                                    <td class="text-center">退单拒绝原因</td>
                                    <td class="text-center">订单状态</td>
                                    <th class="text-center">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td class='text-center'>{{ $order->id }}</td>
                                    <td class='text-center'>
                                        <a href="{{ route('admin.order.show', $order->id) }}">{{ $order->order_number }}</a>
                                    </td>
                                    <td class='text-center'>{{ $order->product->name }}</td>
                                    <td class='text-center'>{{ $order->user ? $order->user->nick_name : '' }}</td>
                                    <td class='text-center w110'>
                                        起&nbsp;{{ $order->rent_start_date }}止&nbsp;{{ $order->rent_end_date }}
                                    </td>
                                    <td class="text-center">{{ $order->rent_price }}</td>
                                    <td class="text-center">{{ $order->phone }}</td>
                                    <td class="text-center w150">{{ $order->address }}</td>
                                    <td class="text-center">
                                        @if (!$order->express_company)
                                            <a href="#" class='express_company' data-company="{{ $order->express_company }}" data-type="select" data-pk="{{ $order->id }}" data-url="{{ route('admin.order.update', $order->id) }}" data-name="express_company" data-title="录入快递公司">
                                                点击录入
                                            </a>
                                        @else
                                            <a href="#" class='express_company' data-company="{{ $order->express_company }}" data-type="select" data-pk="{{ $order->id }}" data-url="{{ route('admin.order.update', $order->id) }}" data-name="express_company" data-title="修改快递公司">
                                                {{ $order->express_company() }}
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if (!$order->express_number)
                                            <a href="#" class='express_number' data-number="{{ $order->express_number }}" data-type="text" data-pk="{{ $order->id }}" data-url="{{ route('admin.order.update', $order->id) }}" data-name="express_number" data-title="录入快递编号">
                                                点击录入
                                            </a>
                                        @else
                                            <a href="#" class='express_number' data-number="{{ $order->express_number }}" data-type="text" data-pk="{{ $order->id }}" data-url="{{ route('admin.order.update', $order->id) }}" data-name="express_number" data-title="修改快递编号">
                                                {{ $order->express_number }}
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                         @if (!$order->reject_cancel_reason)
                                            <a href="#" class='reject_cancel_reason' data-reason="N" data-type="text" data-pk="{{ $order->id }}" data-url="{{ route('admin.order.update', $order->id) }}" data-name="reject_cancel_reason" data-title="录入拒绝退单原因">
                                                退单拒绝原因
                                            </a>
                                        @else
                                            <a href="#" class='reject_cancel_reason' data-reason="Y" data-type="text" data-pk="{{ $order->id }}" data-url="{{ route('admin.order.update', $order->id) }}" data-name="reject_cancel_reason" data-title="录入拒绝退单原因">
                                                {{ $order->reject_cancel_reason }}
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                    {{ config('system.database.order_status_exchange')[config('system.database.order_status')[$order->order_status - 1]] }}
                                    </td>
                                    @include('admin.order._option')
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $orders->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
    $('.express_company').each(function() {
        $(this).editable({
        value: $(this).data('company'),
        source: [
              {value: 'shunfeng', text: '顺丰'},
              {value: 'direct', text: '商家直送'},
           ]
       });
    });
    $('.express_number').each(function() {
        $(this).editable({
        value: $(this).data('number')
       });
    });
    $('.reject_cancel_reason').each(function() {
        if ($(this).data('reason') == 'N') {
            $(this).editable({
                value: ''
            });
        } else {
            $(this).editable();
        }
    });
});
</script>
@stop
