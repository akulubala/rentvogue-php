<div class="panel-body bg-light admin-form">
    {!! Form::open(['route' => 'admin.order.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
    <div class="col-md-3">
            <span>
                <input type="search" @if(isset(Request::get("filter")['order_number'])) value="{{ Request::get('filter')['order_number'] }}" @endif class="gui-input" name="filter[order_number]" placeholder='搜索订单编号'/>
            </span>
    </div>
    <div class="col-md-2">
          <label class="field select">
            <select name="filter[order_status]">
              <option value='0'>搜索订单状态</option>
                @foreach ($status as $k => $s)
                    <option value="{{ $s }}" @if(Request::get("filter")['order_status'] == $s) selected="selected" @endif>{{ config('system.database.order_status_exchange')[$s] }}</option>
                @endforeach
            </select>
            <i class="arrow"></i>
          </label>
    </div>
    <button type="submit" class="button button-default">筛选</button>
    {!! Form::close() !!}
</div>
