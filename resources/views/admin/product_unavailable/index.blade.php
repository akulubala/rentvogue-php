@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.product.index'),
            'text' => '产品管理'
        ],
        'second_level' => [
            'text' => '产品详情',
            'link' => route('admin.product.product_property.index', $nameGroupId)
        ],
        'third_level' => [
            'text' => '不可租日期列表'
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        @include('partials.admin.errors')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    不可租日期列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="panel-body bg-light admin-form">
                    @include('admin.product_unavailable._date_picker')
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">产品名称</th>
                                <th class="text-center">产品编号</th>
                                <th class="text-center">颜色</th>
                                <th class="text-center">尺寸</th>
                                <th class="text-center">不可租起始日期</th>
                                <th class="text-center">不可租结束日期</th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$unavailables->isEmpty())
                                @foreach($unavailables as $key => $unavailable)
                                    <tr class="text-center">
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td class="text-center">{{ $unavailable->product->name }}</td>
                                        <td class="text-center">{{ $product->factory_id }}</td>
                                        <td class="text-center">
                                            <span style="display:inline-block;width:10px;height:10px;background-color:{{ $unavailable->product->color->rgb }}"></span>
                                            {{ $unavailable->product->color->rgb }}
                                        </td>
                                        <td class="text-center">{{ config('system.database.sizes')[$unavailable->product->size] }}</td>
                                        <td class="text-center">{{ $unavailable->start_date }}</td>
                                        <td class="text-center">{{ $unavailable->end_date }}</td>
                                        <td class="text-center">
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.product.product_unavailable.destroy', [$unavailable->product->id, $unavailable->id]) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $unavailables->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
$.datepicker.regional['zh-CN'] = {
    closeText: '关闭',
    prevText: '<<',
    nextText: '>>',
    currentText: '今天',
    monthNames: ['一月','二月','三月','四月','五月','六月',
    '七月','八月','九月','十月','十一月','十二月'],
    monthNamesShort: ['一','二','三','四','五','六',
    '七','八','九','十','十一','十二'],
    dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
    dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
    dayNamesMin: ['日','一','二','三','四','五','六'],
    weekHeader: '周',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: '年'};
$.datepicker.setDefaults($.datepicker.regional['zh-CN']);
$.datepicker.setDefaults( $.datepicker.regional[ "zh-CN" ] );
     $(function() {
    $( "#start_date" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      minDate: new Date({{ $startDay->year }}, {{ $startDay->month -1 }} , {{ $startDay->day }}),
      onClose: function( selectedDate ) {
        $( "#start_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#end_date" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      minDate: new Date({{ $startDay->year }}, {{ $startDay->month -1 }} , {{ $startDay->day }}),
      onClose: function( selectedDate ) {
        $( "#end_date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
});
</script>
@stop

