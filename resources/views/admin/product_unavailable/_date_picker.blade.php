<div class="panel-body bg-light admin-form">
    {!! Form::open(['route' => ['admin.product.product_unavailable.store', $productId], 'method' => 'POST', 'class' => 'form-inline form-listtablefilter']) !!}
    <div class="col-md-3">
            <span>
                <input type="text" id="start_date"  class="gui-input" name="start_date" placeholder='起始日期'/>
            </span>
    </div>
    <div class="col-md-3">
            <span>
                <input type="text" id="end_date" class="gui-input"  name="end_date" placeholder='结束日期'/>
            </span>
    </div>
    <button type="submit" class="button button-default">添加</button>
    {!! Form::close() !!}
</div>
