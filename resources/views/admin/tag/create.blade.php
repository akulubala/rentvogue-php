@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    新建标签
                </div>
            </div>
            @include ('admin.tag._form')
        </div>
    </div>
@stop
