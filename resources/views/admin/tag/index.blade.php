@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    标签列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="panel-body bg-light admin-form">
                    {!! Form::open(['route' => 'admin.tag.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                        <div class="col-md-4">
                            <label>标签名称：</label>
                            <span>
                                @if (!empty($filter['name']))
                                    <input type="search" class="form-control gui-input" name="filter[name]" value="{{ $filter['name'] }}"/>
                                @else
                                    <input type="search" class="form-control gui-input" name="filter[name]"/>
                                @endif
                            </span>
                        </div>
                        <div class="col-md-2">
                            <label class="field select" for="filter[style_id]">
                                {!! Form::select('filter[style_id]', $styles, $filter['style_id']) !!}
                                <i class="arrow"></i>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default">筛选</button>
                    {!! Form::close() !!}
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">标签名称</th>
                                <th class="text-center">对应款式</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$tags->isEmpty())
                                @foreach($tags as $tag)
                                    <tr class="text-center">
                                        <td>{{ $tag->id }}</td>
                                        <td>{{ $tag->name }}</td>
                                        <td>{{ $tag->style->name }}</td>
                                        <td>{{ $tag->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.tag.edit', $tag) }}">编辑</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.tag.destroy', $tag) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $tags->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
