@if (Route::currentRouteName() === 'admin.tag.create')
  {!! Form::open(['route' => 'admin.tag.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($tag, ['route' => ['admin.tag.update', $tag->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="name" class="control-label col-sm-2">标签名称</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入标签名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="style_id">对应款式</label>
        <div class="col-sm-10 w300">
            {!! Form::select('style_id', $styles, null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.tag.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
