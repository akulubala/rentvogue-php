@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    幻灯片列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-hover btn-primary btn-default save_order">保存次序</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-hover mbn sorted_table">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">名称</th>
                                <th class="text-center">类型</th>
                                <th class="text-center">对应名称</th>
                                <th class="text-center">图片</th>
                                <th class="text-center">更新时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$slides->isEmpty())
                                @foreach($slides as $slide)
                                    <tr class="text-center">
                                        <td class="identify">{{ $slide->id }}</td>
                                        <td>{{ $slide->name }}</td>
                                        <td>{{ $slide->type() }}</td>
                                        <td>{{ $slide->related_name() }}</td>
                                        <td>
                                            <a class="magnific-link" href="{{ $slide->image() }}"><img src="{{ $slide->image() }}" width="51" height="38"></a>
                                        </td>
                                        <td>{{ $slide->updated_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.slide.edit', $slide) }}">编辑</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.slide.destroy', $slide) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $slides->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/jquery-sortable/source/js/jquery-sortable-min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.magnific-link').magnificPopup({ 
      type: 'image'
      // other options
      // gallery:{enabled:true}
    });
    $('.sorted_table').sortable({
      containerSelector: 'table',
      itemPath: '> tbody',
      itemSelector: 'tr',
      placeholder: '<tr class="placeholder"></tr>'
    });
    $('.save_order').click(function() {
        var order_value = Array();
        var order = 0;
        $("table.sorted_table td.identify").each(function() {
            order_value[$(this).text()] = order++;
        });
        // console.log(order_value);
        $.ajax({
            type: "POST",
            url: "{{ route('admin.slide.order') }}",
            data: { order_value : order_value },
        }).done(function() {
            location.reload();
        }).fail(function() {
            alert('操作失败，请稍后重试');
        });
    });
});
</script>
@stop