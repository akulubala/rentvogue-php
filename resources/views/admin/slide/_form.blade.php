@if (Route::currentRouteName() === 'admin.slide.create')
  {!! Form::open(['route' => 'admin.slide.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($slide, ['route' => ['admin.slide.update', $slide->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="name" class="control-label col-sm-2">幻灯片名称</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入类别名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="order" class="control-label col-sm-2">次序</label>
        <div class="col-sm-10">
            {!! Form::text('order', null, ['class' => 'form-control ui-spinner-input spinner-basic', 'placeholder' => '请输入次序数字']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="type">幻灯片类型</label>
        <div class="col-sm-10">
            <select name="type" class="form-control w150">
                <option value="0" @if(isset($slide->type) && $slide->type == 0) selected='selected' @endif>系列</option>
                <option value="1" @if(isset($slide->type) && $slide->type == 1) selected='selected' @endif>单品</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="related_id" class="control-label col-sm-2">对应名称</label>
        <div class="col-sm-10">
            <select name="related_id" class="basic-single form-control w400">
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">图片</label>
        <div class="col-sm-10">
            <a class="magnific-link" href="{{ $image or '' }}"><img src="{{ $image or '' }}" width="320" height="200"></a>
        </div>
    </div>
    <div class="form-group" id='add_image_div'>
        <label for="image" class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            {!! Form::hidden('image', null, ['class' => 'form-control']) !!}
            <a class="btn btn-default" id="pickfiles">
                <i class="glyphicon glyphicon-plus"></i>
                <span>添加图片</span>
            </a>
            <span>（推荐分辨率：640 * 400）</span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.slide.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('vendor/admin_tpl/vendor/jquery/jquery_ui/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('vendor/bower_components/select2/dist/css/select2.min.css') }}">
@stop
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/plupload/js/plupload.full.min.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bower_components/plupload/js/i18n/zh_CN.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bower_components/js-sdk/src/qiniu.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/image_upload.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".spinner-basic").spinner();
    $.categories = '';
    @foreach($categories as $key => $value)
        @if ($type == 0 && $related_id == $key)
            $.categories += '<option value="' + '{{ $key }}' + '" selected>' + '{{ $value }}' + '</option>';
        @else
            $.categories += '<option value="' + '{{ $key }}' + '">' + '{{ $value }}' + '</option>';
        @endif
    @endforeach
    $.products = '';
    @foreach($products as $key => $value)
        @if ($type == 1 && $related_id == $key)
            $.products += '<option value="' + '{{ $key }}' + '" selected>' + '{{ $value }}' + '</option>';
        @else
            $.products += '<option value="' + '{{ $key }}' + '">' + '{{ $value }}' + '</option>';
        @endif
    @endforeach
    @if ($type == 0)
        $("select[name=related_id]").html($.categories);
    @else
        $("select[name=related_id]").html($.products);
    @endif

    $(".basic-single").select2();
    $('.magnific-link').magnificPopup({ 
      type: 'image'
    });
    $("select[name=type]").change(function () {
        if ($(this).val() == 0) {
            $("select[name=related_id]").html($.categories);
        } else {
            $("select[name=related_id]").html($.products);
        }
        $(".basic-single").select2();
    });
});
</script>
@stop
