@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    反馈信息列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::open(['route' => 'admin.feedback.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                                <div class="form-group mr15">
                                    <label>详细信息：</label>
                                    <span>
                                        @if (!empty($filter['details']))
                                            <input type="search" class="form-control input-sm ml5" name="filter[details]" value="{{ $filter['details'] }}"/>
                                        @else
                                            <input type="search" class="form-control input-sm ml5" name="filter[details]"/>
                                        @endif
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">筛选</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">用户名称</th>
                                <th class="text-center">详细信息</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$feedbacks->isEmpty())
                                @foreach($feedbacks as $feedback)
                                    <tr class="text-center">
                                        <td class="w100">{{ $feedback->id }}</td>
                                        <td class="w200">{{ $feedback->user->name }}</td>
                                        <td class="w300">{{ strlen($feedback->details) > 20 ? mb_substr($feedback->details, 0, 20) . '...' : $feedback->details }}</td>
                                        <td class="w200">{{ $feedback->created_at }}</td>
                                        <td>
                                            <a class="magnific-link" href="#details-popup" data-url="{{ route('admin.feedback.show', $feedback) }}">查看详情</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.feedback.destroy', $feedback) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $feedbacks->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
                <div id="details-popup" class="white-popup mfp-hide w450"><p></p></div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('.magnific-link').click(function(){
        $.ajax({
            type: 'GET',
            url: $(this).data('url')
        }).done(function(response) {
            console.log(response);
            $('#details-popup p').html(response.details);
            $.magnificPopup.open({
              items: {
                src: '#details-popup'
              },
              type: 'inline'
            }, 0);
        }).fail(function() {
            alert('操作失败，请稍后重试');
        });
    });
});
</script>
@stop
