@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    查看意见反馈详细
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name" class="control-label col-sm-2">用户名称</label>
                    <div class="col-sm-10">
                        <p>{{ $feedback->user->name }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="control-label col-sm-2">详细信息</label>
                    <div class="col-sm-10">
                        <p>{{ $feedback->details }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="control-label col-sm-2">创建时间</label>
                    <div class="col-sm-10">
                        <p>{{ $feedback->created_at }}</p>
                    </div>
                </div>
<!--                 <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a class="btn btn-hover btn-warning btn" href="{{ route('admin.feedback.destroy', $feedback) }}">删除</a>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
@stop
