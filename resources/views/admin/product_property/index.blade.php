@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.product.index'),
            'text' => '产品管理'
        ],
        'second_level' => [
            'text' => '产品详情'
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    产品详情
                </div>
            </div>
            <div class="panel-body pn">
                <div class="panel-body bg-light admin-form">
                    <div class="col-md-4">
                        <a href="{{ route('admin.product.product_property.create', $nameGroupId) }}" class="btn btn-hover btn-primary btn">
                            新建详情
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">产品编码</th>
                                <th class="text-center">颜色</th>
                                <th class="text-center">尺寸</th>
                                <th class="text-center">图片</th>
                                <th class="text-center">不可租日期</th>
                                <th class="text-center">复制当前产品</th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$properties->isEmpty())
                                @foreach($properties as $key => $detail)
                                    <tr class="text-center">
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td class="text-center">{{ $detail->factory_id }}</td>
                                        <td class="text-center">
                                            <span style="display:inline-block;width:10px;height:10px;background-color:{{ $detail->color->rgb }}"></span>
                                            {{ $detail->color->rgb }}
                                        </td>
                                        <td class="text-center">{{ config('system.database.sizes')[$detail->size] }}</td>
                                        <td class="text-center">
                                            <div class='magnific-container'>
                                                @if(json_decode($detail->images, true))
                                                    @foreach (json_decode($detail->images, true) as $k => $image)
                                                        @if($k == 0)
                                                          <a href="{{ $image }}" title=""><img src="{{ $image }}" width="80" height="120"></a>
                                                        @else
                                                        <a style='position:absolute;z-index:-1' href="{{ $image }}" title=""><img src="{{ $image }}" width="80" height="120"></a>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.product.product_unavailable.index', $detail->id) }}">
                                                @if ($detail->unavailable->count() > 0)
                                                    查看
                                                @else
                                                    添加
                                                @endif
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" class='duplicated_product' data-type="text" data-pk="{{ $detail->id }}"  data-name="duplicated" data-title="输入复制产品编码" data-url="{{ route('admin.product.product_property.update', [$detail->name_group_id, $detail->id]) }}">输入新产品编码</a>
                                        </td>
                                        @include('admin.product_property._option', $detail)
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $properties->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
    $('.duplicated_product').editable({
        success:function(response) {
                window.location.reload(true);
        },
        error: function(response) {
            if(response.status === 422) {
                alert(response.responseJSON.value[0]);
            }
        }
    });
    $( ".magnific-container" ).each(function(){
        $(this).magnificPopup({
          delegate: 'a', // child items selector, by clicking on it popup will open
          type: 'image',
          gallery:{enabled:true}
        });
    });
});
</script>
@stop

