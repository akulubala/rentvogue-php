<td class="text-right">
  <div class="btn-group text-right">
    <button type="button" class="btn
    @if ($detail->is_available === 'Y') btn-success @else btn-warning @endif
    br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      @if ($detail->is_available === 'Y') 可出租 @else 不可租 @endif
      <span class="caret ml5"></span>
    </button>
    <ul class="dropdown-menu" role="menu" style="left: inherit;right:0">
        <li>
          <a href="{{ route('admin.product.product_property.edit', [$detail->name_group_id, $detail->id]) }}">编辑</a>
        </li>
        <li>
          <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.product.product_property.destroy', [$detail->name_group_id, $detail->id]) }}">删除</a>
        </li>
        <li class="divider"></li>
        <li @if ($detail->is_available === 'Y') class="active" @endif>
             <a href="javascript:void(0);" class="btn-option" option='put' data-key="is_available" data-v="Y"  data-url="{{ route('admin.product.product_property.destroy', [$detail->name_group_id, $detail->id]) }}">
                    可出租
             </a>
        </li>
        <li @if ($detail->is_available === 'N') class="active" @endif>
            <a href="javascript:void(0);" class="btn-option" option='put' data-key="is_available" data-v="N" data-url="{{ route('admin.product.product_property.destroy', [$detail->name_group_id, $detail->id]) }}">
                    不可租
            </a>
        </li>
    </ul>
  </div>
</td>
