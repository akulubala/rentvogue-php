@if (Route::currentRouteName() === 'admin.product.product_property.create')
  {!! Form::open(['route' => ['admin.product.product_property.store', $nameGroupId], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($property, ['route' => ['admin.product.product_property.update', $property->name_group_id, $property->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="factory_id" class="control-label col-sm-2">产品编码</label>
        <div class="col-sm-10">
            {!! Form::text('factory_id', null, ['class' => 'form-control', 'placeholder' => '请输入产品编码']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="rgb" class="control-label col-sm-2">颜色RGB值</label>
        @if(isset($property))
        <div class="col-sm-1" style="height:38px; width:84px;border:1px solid #dddddd;margin-left:10px;background-color:{{ $property->color->rgb }}">&nbsp;</div>
        @else
        <div class="col-sm-1" style="height:38px; width:84px;border:1px solid #dddddd;margin-left:10px;">&nbsp;</div>
        @endif
        <div class="col-sm-2">
            <select id='color-picker' class="form-control colors required" data-msg-required='颜色不能为空' name='color_id'>
                    <option value=''>---请选择颜色---</option>
                    @foreach ($colors as $key => $color)
                        @if (isset($property))
                            <option value="{{ $key }}" @if ($color == $property->color->rgb) selected="selected" @endif >{{ $color }}</option>
                        @else
                            <option value="{{ $key }}">{{ $color }}</option>
                        @endif
                    @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="size" class="control-label col-sm-2">产品尺码</label>
        <div class="col-sm-10">
            {!! Form::select('size', [null => '---请选择尺码---'] + $sizes, null, ['class' => 'form-control sizes required', 'data-msg-required' => '尺码不能为空']) !!}
        </div>
    </div>
    <div class="form-group product_preview">
            <div class="col-sm-2"></div>
            @if (Route::currentRouteName() === 'admin.product.product_property.edit')
                    @foreach (json_decode($property->images, true) as $p)
                        <div class="gallery-box ng-scope col-sm-2">
                              <div class="thumbnail" style='height:200px;width:133px;'>
                                <img src="{{ $p }}">
                              </div>
                              <div class="btn-group">
                                <a class="btn btn-xs btn-danger remove_picture">
                                  移除
                                </a>
                              </div>
                        </div>
                    @endforeach
            @endif

    </div>
    <div class="form-group" id="pickfiles">
        <div class="col-sm-2">
            <a style="position: relative;float:right; z-index: 1;cursor:pointer">
                <i class="glyphicon glyphicon-plus"></i>
                <span>添加图片</span>
            </a>
        </div>
        <div class="col-sm-3">（注：图片推荐分辨率为836*1250）</div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.product.product_property.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
<div class="hidden" id='images'>
@if (Route::currentRouteName() === 'admin.product.product_property.edit')
    @foreach ($original_images as $image)
        <input type='hidden'  name="images[]" value="{{ $image }}" />
    @endforeach
@endif
</div>
{!! Form::close() !!}
{!! Form::hidden('qiniu_token', $token) !!}
{!! Form::hidden('domain', $domain) !!}
@include('admin.product_property._product_preview')
@include('partials.admin.js_errors_template')
<div class="container" id="crop-avatar">
    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form class="avatar-form">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">上传图片</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Upload image and data -->
                <div class="avatar-upload">
                  <input type="hidden" class="avatar-src" name="avatar_src">
                  <input type="hidden" class="avatar-data" name="avatar_data">
                  <label for="avatarInput">本地上传</label>
                  <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                </div>

                <!-- Crop and preview -->
                <div class="row">
                  <div class="col-md-9">
                    <div class="avatar-wrapper"></div>
                  </div>
                  <div class="col-md-3">
                    <div class="avatar-preview preview-large"></div>
                  </div>
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-9">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">左旋转</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">右旋转</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
                      <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <button type="button" class="btn btn-primary btn-block avatar-save" id="uploadBtn">确定上传</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
          </form>
        </div>
      </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
</div>
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/cropper/dist/cropper.min.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/crop.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    function formatState (state) {
      console.log(state);
      if (!state.id) { return state.text; }
      var $state = $(
        "<div style='display:inline-block;width:100%;height100%;background-color:" + state.text + "'>"+state.text+"</div>"
      );
      return $state;
    };

    $("#color-picker").select2({
      templateResult: formatState,
      placeholder: "--请选择颜色--",
    });
    $('#color-picker').change(function(){
        $(this).parent('div')
              .prev('div')
              .css('background-color', $(this).children('option:selected').val() ? $(this).children('option:selected').text() : '');
    });
    $('.product_preview').delegate('.remove_picture', 'click', function() {
        var gallery = $(this).parents('.gallery-box'),
            productIdx = parseInt($(this).parents('.gallery-box').index()) - 1;

        gallery.remove();
        $('#images').find("input").eq(productIdx).remove()
    });
    $('form').submit(function() {
        return true;
    });
});
</script>
@stop
