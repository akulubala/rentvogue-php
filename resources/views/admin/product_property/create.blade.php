@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.product.index'),
            'text' => '产品管理'
        ],
        'second_level' => [
            'text' => '添加产品详情'
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    添加产品详情
                </div>
            </div>
            @include ('admin.product_property._form')
        </div>
    </div>
@stop
