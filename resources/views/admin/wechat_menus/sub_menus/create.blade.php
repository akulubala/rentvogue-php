@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    新建微信二级菜单 | 一级菜单名称 ({{ $menu->menu_name }})
                </div>
            </div>
            @include('admin.wechat_menus.sub_menus._form')
        </div>
    </div>
@stop

