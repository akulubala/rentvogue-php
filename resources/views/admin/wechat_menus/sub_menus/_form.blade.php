
@if (Route::currentRouteName() === 'admin.wechat-menus.sub-menus.create')
  {!! Form::open(['route' => ['admin.wechat-menus.sub-menus.store', $menu->id], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($subMenu, ['route' => ['admin.wechat-menus.sub-menus.update', $menu->id, $subMenu->id], 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}
@endif
  <div class="panel-body">
   {!! Form::hidden('menu_level', 2); !!}
   {!! Form::hidden('parent_id', $menu->id); !!}
    <div class="form-group">
      {!! Form::label('menu_name', '菜单名称', ['class' => 'control-label col-sm-2']) !!}
      <div class="col-sm-10">
        {!! Form::text('menu_name', null, ['class' => 'form-control', 'placeholder' => '请输入标题']) !!}
      </div>
    </div>
    <div class="form-group">
        {!! Form::label('url', '链接', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => '请输入带http://连接']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('sort', '排序值', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::text('sort', null, ['class' => 'form-control', 'placeholder' => '请输入带排序值']) !!}
        </div>
    </div>
     <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="button btn-primary btn">
              @if (Route::currentRouteName() === 'admin.wechat-menus.sub-menus.create')
                添加
              @else
                更新
              @endif
            </button>
        </div>
    </div>
  </div>
{!! Form::close() !!}
