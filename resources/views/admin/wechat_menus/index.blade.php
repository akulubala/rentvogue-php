@extends('layouts.admin')
@section('content')
<div id="content">
    <div class="panel">
        @include('partials.admin.status_success')
        @include('partials.admin.errors')
        <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    微信菜单列表
                </div>
        </div>
        <div class="panel-body pn">
          <div class="table-responsive">
            <div class="bs-component">
              <table class="table table-bordered table-striped mbn">
                <thead>
                  <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">菜单名称</th>
                    <th class="text-center">连接url</th>
                    <th class="text-center">排序</th>
                    <th class="text-center">最后更新时间</th>
                    <th class="text-center">添加二级菜单</th>
                    <th class="text-center">操作</th>
                  </tr>
                </thead>
                <tbody>
                    @if (!$wechatMenus->isEmpty())
                        @foreach ($wechatMenus as $key => $menu)
                            <tr>
                                <td class="text-center">{{ $key + 1 }}</td>
                                <td class="text-center">{{ $menu->menu_name }}</td>
                                <td class="text-center"><a href="{{ $menu->url }}">{{ $menu->url }}</a></td>
                                <td class="text-center">{{ $menu->sort }}</td>
                                <td class="text-center">{{ $menu->updated_at }}</td>
                                <td class="text-center"><a href="{{ route('admin.wechat-menus.sub-menus.create', $menu) }}">添加</a></td>
                                <td class="text-center">
                                    <a href="{{ route('admin.wechat-menus.show', $menu) }}">查看</a>
                                    <a href="{{ route('admin.wechat-menus.edit', $menu) }}">编辑</a>
                                    <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.wechat-menus.destroy', [$menu->id]) }}">删除</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
            <div class="col-xs-2 m10 pull-right">
              <div class="bs-component">
                <a href="{{ route('wechat.publish_menus') }}" class="btn btn-primary btn-block" type="button">发布菜单</a>
              </div>
            </div>
    </div>
</div>
@stop
