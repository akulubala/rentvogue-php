@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    更新微信菜单
                </div>
            </div>
            @include ('admin.wechat_menus._form')
        </div>
    </div>
@stop

