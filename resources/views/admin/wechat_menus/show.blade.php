@extends('layouts.admin')
@section('content')
<div id="content">
    <div class="panel">
        @include('partials.admin.status_success')
        <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    一级菜单
                </div>
        </div>
        <div class="panel-body pn">
          <div class="table-responsive">
            <div class="bs-component">
              <table class="table table-bordered table-striped mbn">
                <thead>
                  <tr>
                    <th class="text-center">菜单名称</th>
                    <th class="text-center">连接url</th>
                    <th class="text-center">排序</th>
                    <th class="text-center">最后更新时间</th>
                    <th class="text-center">添加二级菜单</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">{{ $menu->menu_name }}</td>
                        <td class="text-center"><a href="{{ $menu->url }}">{{ $menu->url }}</a></td>
                        <td class="text-center">{{ $menu->sort }}</td>
                        <td class="text-center">{{ $menu->updated_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('admin.wechat-menus.sub-menus.create', $menu) }}">添加</a>
                        </td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    二级菜单
                </div>
        </div>
        <div class="panel-body pn">
          <div class="table-responsive">
            <div class="bs-component">
              <table class="table table-bordered table-striped mbn">
                <thead>
                  <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">菜单名称</th>
                    <th class="text-center">连接url</th>
                    <th class="text-center">排序</th>
                    <th class="text-center">最后更新时间</th>
                    <th class="text-center">操作</th>
                  </tr>
                </thead>
                <tbody>
                    @if (!$menu->SubMenus()->get()->isEmpty())
                        @foreach ($menu->SubMenus()->get() as $key => $subMenu)
                            <tr>
                                <td class="text-center">{{ $key + 1 }}</td>
                                <td class="text-center">{{ $subMenu->menu_name }}</td>
                                <td class="text-center"><a href="{{ $subMenu->url }}">{{ $subMenu->url }}</a></td>
                                <td class="text-center">{{ $subMenu->sort }}</td>
                                <td class="text-center">{{ $subMenu->updated_at }}</td>
                                <td class="text-center">
                                    <a href="{{ route('admin.wechat-menus.sub-menus.edit', [$menu->id, $subMenu->id]) }}">编辑</a>
                                    <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.wechat-menus.sub-menus.destroy', [$menu->id, $subMenu->id]) }}">删除</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
</div>
@stop
