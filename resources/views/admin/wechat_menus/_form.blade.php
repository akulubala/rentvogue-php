
@if (Route::currentRouteName() === 'admin.wechat-menus.create')
  {!! Form::open(['route' => 'admin.wechat-menus.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($menu, ['route' => ['admin.wechat-menus.update', $menu->id], 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}
@endif
   {!! Form::hidden('menu_level', 1); !!}
   {!! Form::hidden('parent_id'); !!}
  <div class="panel-body">
    <div class="form-group">
      {!! Form::label('menu_name', '菜单名称', ['class' => 'control-label col-sm-2']) !!}
      <div class="col-sm-10">
        {!! Form::text('menu_name', null, ['class' => 'form-control', 'placeholder' => '请输入标题']) !!}
      </div>
    </div>

    <div class="form-group">
        {!! Form::label('url', '链接', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => '请输入带http://连接']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('sort', '排序值', ['class' => 'control-label col-sm-2']) !!}
        <div class="col-sm-10">
            {!! Form::text('sort', null, ['class' => 'form-control', 'placeholder' => '请输入带排序值']) !!}
        </div>
    </div>
     <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="button btn-primary btn">
              @if (Route::currentRouteName() === 'admin.wechat-menus.create')
                添加
              @else
                更新
              @endif
            </button>
        </div>
    </div>
  </div>
{!! Form::close() !!}
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#menu_level').change(function() {
        if($(this).val() == 2) {
            $('#parent_id').parents('.form-group').removeClass('hide');
        } else {
            $('#parent_id').parents('.form-group').addClass('hide');
        }
    })
});
</script>
@stop
