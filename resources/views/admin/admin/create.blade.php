@extends('layouts.admin')
@section('content')
    <div id="content">
      <div class="panel">
        @include('partials.admin.errors')
         <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span>
                新建系统用户
            </div>
        </div>
        {!! Form::open(['route' => 'admin.admin.store', 'method' => 'POST' ,'class' => 'form-horizontal']) !!}
          <div class="panel-body">
            <div class="form-group">
              <label for="user_name" class="control-label col-sm-2">用户名</label>
              <div class="col-sm-10">
                <input type="text" name="user_name" id="user_name" class="form-control" placeholder="请输入用户名" value="{{ old('user_name') }}">
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="role">管理员角色</label>
                <div class="col-sm-10">
                    <select name="role" class="form-control">
                        <option selected="selected" value="">--请选择--</option>
                        <option value="super_admin">超级管理员</option>
                        <option value="admin">管理员</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
              <label for="password" class="control-label col-sm-2">密码</label>
              <div class="col-sm-10">
                <input type="password" name="password" id="password" class="form-control" placeholder="请输入密码">
              </div>
            </div>
            <div class="form-group">
              <label for="password_confirmation" class="control-label col-sm-2">再次输入密码</label>
              <div class="col-sm-10">
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="请再次输入密码">
              </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                 <button type="submit" class="btn btn-hover btn-primary btn">添加</button>
                </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
@stop
