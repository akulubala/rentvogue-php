@extends('layouts.admin')
@section('content')
    <div id="content">
      <div class="panel">
        @include('partials.admin.errors')
         <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span>
                编辑系统用户
            </div>
        </div>
        {!! Form::open(['route' => ['admin.admin.update', $admin], 'method' => 'PUT' ,'class' => 'form-horizontal']) !!}
          <div class="panel-body">
            <div class="form-group">
              <label for="user_name" class="control-label col-sm-2">用户名</label>
              <div class="col-sm-10">
                <input type="text" name="user_name" id="user_name" class="form-control" placeholder="请输入用户名" value="{{ old('user_name', $admin->user_name) }}">
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="role">管理员角色</label>
                <div class="col-sm-10">
                    <select name="role" class="form-control">
                        <option  value="">--请选择--</option>
                        <option value="super_admin" @if($admin->role == 'super_admin') selected='selected' @endif>超级管理员</option>
                        <option value="admin" @if($admin->role == 'admin') selected='selected' @endif>管理员</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
              <label for="current_password" class="control-label col-sm-2">原始密码</label>
              <div class="col-sm-10">
                <input type="password" name="current_password" id="current_password" class="form-control" placeholder="请输入原始密码">
              </div>
            </div>
            <div class="form-group">
              <label for="new_password" class="control-label col-sm-2">新密码</label>
              <div class="col-sm-10">
                <input type="password" name="new_password" id="new_password" class="form-control" placeholder="请输入新密码">
              </div>
            </div>
            <div class="form-group">
              <label for="new_password_confirmation" class="control-label col-sm-2">再次输入新密码</label>
              <div class="col-sm-10">
                <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" placeholder="请再次输入新密码">
              </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                 <button type="submit" class="btn btn-hover btn-primary btn">更新</button>
                </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
@stop
