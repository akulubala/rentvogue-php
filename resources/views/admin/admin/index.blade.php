@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    管理员列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::open(['route' => 'admin.admin.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                                <div class="form-group mr15">
                                    <label>用户名：</label>
                                    <span>
                                        @if (!empty($filter['user_name']))
                                            <input type="search" class="form-control input-sm ml5" name="filter[user_name]" value="{{ $filter['user_name'] }}"/>
                                        @else
                                            <input type="search" class="form-control input-sm ml5" name="filter[user_name]"/>
                                        @endif
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">筛选</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">用户名</th>
                                    <th class="text-center">角色</th>
                                    <th class="text-center">创建时间</th>
                                    <th class="text-center">最后登录时间</th>
                                    <th class="text-center">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($admins as $admin)
                                    <tr class="text-center">
                                        <td>{{ $admin->id }}</td>
                                        <td>{{ $admin->user_name }}</td>
                                        <td>
                                            @if ($admin->role == 'super_admin')
                                                超级管理员
                                            @else
                                                管理员
                                            @endif
                                        </td>
                                        <td>{{ $admin->created_at }}</td>
                                        <td>{{ $admin->last_logined_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.admin.edit', $admin) }}">编辑</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option="delete" data-url="{{ route('admin.admin.destroy', $admin) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $admins->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
