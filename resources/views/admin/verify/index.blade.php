@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    待验证身份用户列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::open(['route' => 'admin.verify.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                                <div class="form-group mr15">
                                    <label>用户名：</label>
                                    <span>
                                        @if (!empty($filter['name']))
                                            <input type="search" class="form-control input-sm ml5" name="filter[name]" value="{{ $filter['name'] }}"/>
                                        @else
                                            <input type="search" class="form-control input-sm ml5" name="filter[name]"/>
                                        @endif
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">筛选</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                                <tr>
                                    <th class="text-center">用户id</th>
                                    <th class="text-center">用户名</th>
                                    <th class="text-center">昵称</th>
                                    <th class="text-center">手机号码</th>
                                    <th class="text-center">身份证号码</th>
                                    <th class="text-center">身份证正面</th>
                                    <th class="text-center">身份证反面</th>
                                    <th class="text-center">注册时间</th>
                                    <th class="text-center">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr class="text-center">
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->nick_name }}</td>
                                        <td>{{ $user->cell_phone }}</td>
                                        <td>{{ $user->id_number }}</td>
                                        <td>
                                            <a class="magnific-link" href="{{ $user->id_card_frontend }}"><img src="{{ $user->id_card_frontend }}" width="51" height="38"></a>
                                        </td>
                                        <td>
                                            <a class="magnific-link" href="{{ $user->id_card_backend }}"><img src="{{ $user->id_card_backend }}" width="51" height="38"></a>
                                        </td>
                                        <td>{{ $user->created_at }}</td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn-option btn btn-success btn-xs" option='put' data-key="verify_status" data-v="3" data-url="{{ route('admin.user.update', $user) }}">通过</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option btn btn-warning btn-xs" option='put' data-key="verify_status" data-v="2" data-url="{{ route('admin.user.update', $user) }}">拒绝</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $users->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('.magnific-link').magnificPopup({ 
      type: 'image'
      // other options
      // gallery:{enabled:true}
    });
});
</script>
@stop