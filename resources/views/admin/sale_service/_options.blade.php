<td class="text-right">
@if ($service->acceptance_status == $lastStatus)
  <div class="btn-group text-right">
    <button type="button"
    class="btn btn-success br2 btn-xs fs12 dropdown-toggle" style="width:148px" data-toggle="dropdown" aria-expanded="false">
        更新售后状态
      <span class="caret mr0"></span>
    </button>
    <ul class="dropdown-menu" role="menu" style="left: inherit;right:0">
        @foreach ($serviceStatus as $key => $value)
            @if ($key == $lastStatus)
                @if ($lastStatus != 'reject' && $key != 'finished')
                    <li>
                        <a class="btn-option" option='put' data-key="acceptance_status" data-v="{{ array_flip($serviceStatus)[current($serviceStatus)] }}" data-url="{{ route('admin.order.sale_service.update', [$orderId, $service->id]) }}">
                            {{ current($serviceStatus) }}
                        </a>
                    </li>
                @endif
            @endif
        @endforeach
        <li>
            <a class="btn-option" option='put' data-key="acceptance_status" data-v="reject" data-url="{{ route('admin.order.sale_service.update', [$orderId, $service->id]) }}">
                已拒绝
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a class="btn-option" option='delete' data-url="{{ route('admin.order.sale_service.destroy', [$orderId, $service->id]) }}">
                删除
            </a>
        </li>
    </ul>
  </div>
@endif
</td>
