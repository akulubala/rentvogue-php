@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.order.index'),
            'text' => '订单管理'
        ],
        'second_level' => [
            'text' => '售后服务',
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    售后服务
                </div>
            </div>
            <div class="panel-body pn">
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">处理人员</th>
                                <th class="text-center">服务类别</th>
                                <th class="text-center">服务原因</th>
                                <th class="text-center">处理状态</th>
                                <th class="text-center">备注</th>
                                <th class="text-center">时间</th>
                                <th class="text-center">更新状态</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$services->isEmpty())
                                @foreach($services as $key => $service)
                                    <tr class="text-center">
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td class="text-center">{{ $service->admin->user_name }}</td>
                                        <td class="text-center">
                                            @if ($service->service_type == 'returns')
                                                退货
                                            @elseif ($service->service_type == 'exchanges')
                                                换货
                                            @else
                                            <a href="#" class='choose_service_type' data-name='service_type' data-type="select" data-pk="{{ $service->id }}" data-url="{{ route('admin.order.sale_service.update', [$orderId, $service->id]) }}">选择服务类别</a>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($service->reason_for_service)
                                              {{ $service->reason_for_service }}
                                            @else
                                            <a href="#" class='editable_key' data-name='reason_for_service' data-type="text" data-pk="{{ $service->id }}" data-url="{{ route('admin.order.sale_service.update', [$orderId, $service->id]) }}">录入原因</a>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($service->acceptance_status == 'waiting_acceptance')
                                                初始化
                                            @elseif ($service->acceptance_status == 'accepted')
                                                已接受
                                            @elseif ($service->acceptance_status == 'delivering')
                                                处理中
                                            @elseif ($service->acceptance_status == 'finished')
                                                已完成
                                            @elseif ($service->acceptance_status == 'reject')
                                                已拒绝
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if ($service->descriptions)
                                              {{ $service->descriptions }}
                                            @else
                                            <a href="#" class='editable_key' data-name='descriptions' data-type="text" data-pk="{{ $service->id }}" data-url="{{ route('admin.order.sale_service.update', [$orderId, $service->id]) }}">录入备注</a>
                                            @endif
                                        </td>
                                        <td class="text-center">{{ $service->updated_at }}</td>
                                        @include('admin.sale_service._options')
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
    $('.choose_service_type').editable({
        value: '--请选择--',
        source: [
              {value: null , text: '--请选择--'},
              {value: 'returns', text: '退货'},
              {value: 'exchanges', text: '换货'},
           ]
    });
    $('.editable_key').editable();
});
</script>
@stop
