@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    产品款式列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::open(['route' => 'admin.style.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                                <div class="form-group mr15">
                                    <label>款式名称：</label>
                                    <span>
                                        @if (!empty($filter['name']))
                                            <input type="search" class="form-control input-sm ml5" name="filter[name]" value="{{ $filter['name'] }}"/>
                                        @else
                                            <input type="search" class="form-control input-sm ml5" name="filter[name]"/>
                                        @endif
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">筛选</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">款式名称</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$styles->isEmpty())
                                @foreach($styles as $style)
                                    <tr class="text-center">
                                        <td>{{ $style->id }}</td>
                                        <td>{{ $style->name }}</td>
                                        <td>{{ $style->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.style.edit', $style) }}">编辑</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.style.destroy', $style) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $styles->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
