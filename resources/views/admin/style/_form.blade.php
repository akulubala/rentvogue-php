@if (Route::currentRouteName() === 'admin.style.create')
  {!! Form::open(['route' => 'admin.style.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($style, ['route' => ['admin.style.update', $style->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="name" class="control-label col-sm-2">款式名称</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入款式名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.style.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
