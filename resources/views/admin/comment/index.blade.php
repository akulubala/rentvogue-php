@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    评论列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="panel-body bg-light admin-form">
                    {!! Form::open(['route' => 'admin.comment.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                    <div class="col-md-2">
                        <label class="field select" for="filter[is_verify]">
                        <select name="filter[is_verify]">
                            <option value="" @if(empty($filter['is_verify'])) selected='selected' @endif>全部</option>
                            <option value="Y" @if(!empty($filter['is_verify']) && $filter['is_verify'] == 'Y') selected='selected' @endif>正常</option>
                            <option value="N" @if(!empty($filter['is_verify']) && $filter['is_verify'] == 'N') selected='selected' @endif>禁用</option>
                        </select>
                        <i class="arrow"></i>
                        </label>
                    </div>
                    <button type="submit" class="button button-default">筛选</button>
                    {!! Form::close() !!}
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn admin-form theme-info">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">产品名称</th>
                                <th class="text-center">评星</th>
                                <th class="text-center">详细评论</th>
                                <th class="text-center">标签</th>
                                <th class="text-center">晒图</th>
                                <th class="text-center">尺寸</th>
                                <th class="text-center">场合</th>
                                <th class="text-center">评论时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($comments as $comment)
                                <tr class="text-center">
                                    <td>{{ $comment->id }}</td>
                                    <td class="w100">{{ $comment->product->name }}</td>
                                    <td>{{ $comment->stars }}</td>
                                    <td class="w200 text-left">{{ $comment->comment }}</td>
                                    <td class="w150">{{ implode(',', $comment->tags) }}</td>
                                    <td>
                                        <div class='magnific-container'>
                                            @foreach (json_decode($comment->images) as $key => $image)
                                                @if($key === 0)
                                                    <a href="{{ $image }}" title=""><img src="{{ $image }}" width="65" height="44"></a>
                                                @else
                                                    <a style='display:none' href="{{ $image }}" title=""></a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                    <td>{!! config('system.database.sizes')[$comment->product->size] !!}</td>
                                    <td>{!! $comment->product->occasion->name !!}</td>
                                    <td>{{ $comment->created_at }}</td>
                                    @include('admin.comment._option')
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $comments->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $( ".magnific-container" ).each(function(){
        $(this).magnificPopup({
          delegate: 'a', // child items selector, by clicking on it popup will open
          type: 'image',
          gallery:{enabled:true}
        });
    });
});
</script>
@stop
