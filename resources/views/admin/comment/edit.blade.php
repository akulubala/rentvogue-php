@extends('layouts.admin')
@section('content')
    <div id="content">
      <div class="panel">
        @include('partials.admin.errors')
         <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span>
                编辑评论
            </div>
        </div>
        {!! Form::open(['route' => ['admin.comment.update', $comment], 'method' => 'PUT' ,'class' => 'form-horizontal']) !!}
          <div class="panel-body">
            <div class="form-group">
              <label for="name" class="control-label col-sm-2">产品名称</label>
              <div class="col-sm-10">
                <input type="text" name="name" id="name" class="form-control" placeholder="" readonly value="{{ $comment->product->name }}">
              </div>
            </div>
            <div class="form-group">
              <label for="stars" class="control-label col-sm-2">评星</label>
              <div class="col-sm-10">
                <input type="text" name="stars" id="stars" class="form-control" placeholder="请输入评星" value="{{ $comment->stars }}">
              </div>
            </div>
            <div class="form-group">
              <label for="comment" class="control-label col-sm-2">详细评论</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="comment" id="comment" rows="3">{{ $comment->comment }}</textarea>
              </div>
            </div>
            <div class="form-group">
                <label for="tags[]" class="control-label col-sm-2">标签</label>
                <div class="col-sm-10">
                    <select name="tags[]" class="basic-tags form-control w400" multiple="multiple">
                        @foreach($tags as $id => $tag)
                            @if (in_array($id, $comment->tags))
                                <option value="{{ $id }}" selected>{{ $tag }}</option>
                            @else
                                <option value="{{ $id }}">{{ $tag }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="is_verify">状态</label>
              <div class="col-sm-10">
                  <select name="is_verify" class="form-control w100">
                      <option value="Y" @if($comment->is_verify == 'Y') selected='selected' @endif>已通过</option>
                      <option value="N" @if($comment->is_verify == 'N') selected='selected' @endif>未通过</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="is_verify">晒图</label>
              <div class="col-sm-10">
                <div class='magnific-container'>
                    @foreach (json_decode($comment->images) as $key => $image)
                        <a href="{{ $image }}" title=""><img src="{{ $image }}" width="65" height="44"></a>
                    @endforeach
                </div>
              </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                 <button type="submit" class="btn btn-hover btn-primary btn">更新</button>
                </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
@stop
@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('vendor/bower_components/select2/dist/css/select2.min.css') }}">
@stop
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/select2/dist/js/select2.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".basic-tags").select2({
        tags: true,
    });
    $(".magnific-container").magnificPopup({
      delegate: 'a', // child items selector, by clicking on it popup will open
      type: 'image',
      gallery:{enabled:true}
    });
});
</script>
@stop
