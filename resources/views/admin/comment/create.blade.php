@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    新建产品
                </div>
            </div>
            @include ('admin.product._form')
        </div>
    </div>
@stop
