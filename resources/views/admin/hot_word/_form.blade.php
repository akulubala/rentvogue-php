@if (Route::currentRouteName() === 'admin.hot_word.create')
  {!! Form::open(['route' => 'admin.hot_word.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($hot_word, ['route' => ['admin.hot_word.update', $hot_word->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="word" class="control-label col-sm-2">热词名称</label>
        <div class="col-sm-10">
            {!! Form::text('word', null, ['class' => 'form-control', 'placeholder' => '请输入热词名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.hot_word.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
