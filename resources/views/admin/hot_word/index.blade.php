@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    热词列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="dt-panelmenu clearfix">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Form::open(['route' => 'admin.hot_word.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                                <div class="form-group mr15">
                                    <label>热词名称：</label>
                                    <span>
                                        @if (!empty($filter['word']))
                                            <input type="search" class="form-control input-sm ml5" name="filter[word]" value="{{ $filter['word'] }}"/>
                                        @else
                                            <input type="search" class="form-control input-sm ml5" name="filter[word]"/>
                                        @endif
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default btn-sm">筛选</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">热词</th>
                                <th class="text-center">创建时间</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (!$hot_words->isEmpty())
                                @foreach($hot_words as $hot_word)
                                    <tr class="text-center">
                                        <td>{{ $hot_word->id }}</td>
                                        <td>{{ $hot_word->word }}</td>
                                        <td>{{ $hot_word->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.hot_word.edit', $hot_word) }}">编辑</a>
                                            <span class="separator"></span>
                                            <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.hot_word.destroy', $hot_word) }}">删除</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $hot_words->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
