@extends('layouts.admin')
@section('breadcrumb')
@include('partials.admin.breadcrumbs',
    [
        'first_level' => [
            'link' => route('admin.product.index'),
            'text' => '产品管理'
        ],
        'second_level' => [
            'text' => '产品列表'
        ]
    ]
)
@stop
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    产品列表
                </div>
            </div>
            <div class="panel-body pn">
                @include('admin.product._search')
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">产品品牌</th>
                                <th class="text-center">服务城市</th>
                                <th class="text-center">使用场合</th>
                                <th class="text-center">类别</th>
                                <th class="text-center">款式</th>
                                <th class="text-center">体型</th>
                                <th class="text-center">名称</th>
                                <th class="text-center">市场价</th>
                                <th class="text-center">4天租价格</th>
                                <th class="text-center">8天租价格</th>
                                <th class="text-center">押金</th>
                                <th class="text-center">供货商</th>
                                <th class="text-center">供应商电话</th>
                                <th class="text-center">供货地址</th>
                                <th class="text-center">描述 </th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--*/ $s = 0; /*--}}
                            @foreach($results as $key => $product)
                                <tr class="text-center" style='background-color:white'>
                                        <td>{{ $s+ 1 }}</td>
                                        <td>{{ $product->brand->name }}</td>
                                        <td>{{ config('system.database.city_code')[$product->city]['name'] }}</td>
                                        <td>{!! $product->occasion->name !!}</td>
                                        <td>{!! config('system.database.usage_categories')[$product->usage_category] !!}</td>
                                        <td>{{ $product->style->name }}</td>
                                        <td>{{ config('system.database.body_figures')[$product->body_figure] }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->market_price }}</td>
                                        <td>{{ $product->four_day_rent_price }}</td>
                                        <td>{{ $product->eight_day_rent_price }}</td>
                                        <td>{{ $product->deposit }}</td>
                                        <td>{{ $product->supplier->name }}</td>
                                        <td>{{ $product->supplier->phone }}</td>
                                        <td>{{ $product->supplier->address }}</td>
                                        <td class="w250">{{ $product->describe }}</td>
                                        <td class="text-center w100">
                                            <a style="display:inline-block" href="{{ route('admin.product.product_property.index',['name_group_id' => $product->name_group_id]) }}">
                                                @if ($product->factory_id)
                                                查看产品图
                                                @else
                                                添加产品图
                                                @endif
                                            </a>
                                            <a style="display:inline-block" href="{{ route('admin.product.edit', $product->name_group_id) }}">编辑属性</a>
                                            <a style="display:inline-block" href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.product.destroy', $product->id) }}">删除属性</a>
                                        </td>
                                </tr>
                                {{--*/ $s++; /*--}}
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $results->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
