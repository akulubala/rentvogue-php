<div class="dt-panelmenu clearfix">
    {!! Form::open(['route' => 'admin.product.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
    <div class="form-group mr15">
        <label>产品名称：</label>
            <span>
                <input type="search" class="form-control input-sm ml5" @if(isset(\Input::get('filter')['name'])) value="{{ \Input::get('filter')['name'] }}" @endif name="filter[name]"/>
            </span>
    </div>
    <button type="submit" class="btn btn-default btn-sm">筛选</button>
    {!! Form::close() !!}
</div>
