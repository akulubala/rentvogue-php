@if (Route::currentRouteName() === 'admin.product.create')
    {!! Form::open(['route' => 'admin.product.store', 'method' => 'POST' ,'class' => 'form-horizontal', 'id' => 'product_form']) !!}
@else
  {!! Form::Model($product, ['route' => ['admin.product.update', $product->name_group_id], 'method' => 'PUT' ,'class' => 'form-horizontal', 'id' => 'product_form']) !!}
@endif
<div class="panel-body">
    <div class="admin-form">
        <div class="form-group">
            <label for="user_name" class="control-label col-sm-2">请选择城市</label>
            <div class="col-sm-8">
                {!! Form::select('city', [null => '---请选择---'] + $cities, null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="user_name" class="control-label col-sm-2">请选择品牌</label>
            <div class="col-sm-8">
                {!! Form::select('product_category_id', [null => '---请选择---'] + $categories->toArray(), null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="occasion_id" class="control-label col-sm-2">选择使用场合</label>
            <div class="col-sm-8">
                {!! Form::select('occasion_id', [null => '---请选择---'] + $occasions, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="style_id" class="control-label col-sm-2">款式</label>
            <div class="col-sm-8">
                {!! Form::select('style_id', [null => '---请选择---'] + $styles, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="usage_category" class="control-label col-sm-2">选择分类</label>
            <div class="col-sm-8">
                {!! Form::select('usage_category', [null => '---请选择---'] + $usageCategories, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="body_figure" class="control-label col-sm-2">选择体型</label>
            <div class="col-sm-8">
                {!! Form::select('body_figure', [null => '---请选择体型---'] + $bodyFigures, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label col-sm-2">产品名称</label>
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入产品名称']); !!}
            </div>
        </div>
        <div class="form-group">
            <label for="intro" class="control-label col-sm-2">产品简介</label>
            <div class="col-sm-8">
                {!! Form::text('intro', null, ['class' => 'form-control', 'placeholder' => '请输入产品简介']); !!}
            </div>
        </div>
        <div class="form-group" id='market_price'>
            <label for="market_price" class="control-label col-sm-2">请输入市场价格</label>
            <div class="col-sm-8">
                {!! Form::text('market_price', null, ['class' => 'form-control', 'placeholder' => '请输入市场价格']); !!}
            </div>
        </div>
        <div class="form-group" id='four_day_rent_price'>
            <label for="four_day_rent_price" class="control-label col-sm-2">请输入商品4天租金</label>
            <div class="col-sm-8">
                {!! Form::text('four_day_rent_price', null, ['class' => 'form-control', 'placeholder' => '请输入婚纱四天租金（元）']); !!}
            </div>
        </div>
        <div class="form-group">
            <label for="eight_day_rent_price" class="control-label col-sm-2">请输入商品8天租金</label>
            <div class="col-sm-8">
                {!! Form::text('eight_day_rent_price', null, ['class' => 'form-control', 'placeholder' => '请输入婚纱八天租金（元）']); !!}
            </div>
        </div>
        <div class="form-group">
            <label for="deposit" class="control-label col-sm-2">请输入押金</label>
            <div class="col-sm-8">
                {!! Form::text('deposit', null, ['class' => 'form-control', 'placeholder' => '请输入商品租金（元）']); !!}
            </div>
        </div>
        <div class="form-group">
            <label for="supplier_id" class="control-label col-sm-2">供应商</label>
            <div class="col-sm-8">
                {!! Form::select('supplier_id', [null => '---请选择---'] + $suppliers, null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="describe" class="control-label col-sm-2">请输入商品描述</label>
            <div class="col-sm-8">
                {!! Form::textarea('describe', null, ['class' => 'form-control', 'placeholder' => '请输入商品描述']); !!}
            </div>
        </div>
        @if (Route::currentRouteName() === 'admin.product.edit')
        <div class="form-group">
            <label class="control-label col-sm-2">商品图片</label>
            <div class="col-sm-10">
                <div class='magnific-container'>
                    @foreach ($imagesList as $image)
                        <a href="{{ $image }}" title=""><img src="{{ $image }}" width="84" height="125"></a>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" class="btn btn-hover btn-primary btn">
                    @if (Route::currentRouteName() === 'admin.product.create')
                    添加
                    @else
                    更新
                    @endif
                </button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $( ".magnific-container" ).each(function(){
        $(this).magnificPopup({
          delegate: 'a', // child items selector, by clicking on it popup will open
          type: 'image',
          gallery:{enabled:true}
        });
    });
});
</script>
@stop
