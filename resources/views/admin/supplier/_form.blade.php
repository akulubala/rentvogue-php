@if (Route::currentRouteName() === 'admin.supplier.create')
  {!! Form::open(['route' => 'admin.supplier.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($supplier, ['route' => ['admin.supplier.update', $supplier->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="name" class="control-label col-sm-2">供应商名称</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入供应商名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="phone" class="control-label col-sm-2">供应商电话</label>
        <div class="col-sm-10">
            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '请输入供应商电话']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="control-label col-sm-2">供应商地址</label>
        <div class="col-sm-10">
            {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => '请输入供应商地址']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="detail" class="control-label col-sm-2">供应商简介</label>
        <div class="col-sm-10">
            {!! Form::textarea('detail', null, ['class' => 'form-control', 'placeholder' => '请输入供应商简介']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.supplier.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
