@extends('layouts.admin')
@section('content')
    <div id="content">
        <div class="panel">
            @include('partials.admin.errors')
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    编辑产品分类
                </div>
            </div>
            @include ('admin.product_category._form')
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
</script>
@stop
