@if (Route::currentRouteName() === 'admin.product_category.create')
  {!! Form::open(['route' => 'admin.product_category.store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
@else
  {!! Form::Model($category, ['route' => ['admin.product_category.update', $category->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
@endif
<div class="panel-body">
    <div class="form-group">
        <label for="name" class="control-label col-sm-2">分类名称</label>
        <div class="col-sm-10">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '请输入类别名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="series" class="control-label col-sm-2">系列名称</label>
        <div class="col-sm-10">
            {!! Form::text('series', null, ['class' => 'form-control', 'placeholder' => '请输入系列名称']) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="order" class="control-label col-sm-2">次序</label>
        <div class="col-sm-10">
            {!! Form::text('order', null, ['class' => 'form-control ui-spinner-input spinner-basic', 'placeholder' => '请输入系列排列次序']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">封面图片</label>
        <div class="col-sm-10">
            <a class="magnific-link" href="{{ $image or '' }}"><img src="{{ $image or '' }}" width="207" height="110"></a>
        </div>
    </div>
    <div class="form-group" id='add_image_div'>
        <label for="image" class="control-label col-sm-2"></label>
        <div class="col-sm-10">
            {!! Form::hidden('image', null, ['class' => 'form-control']) !!}
            <a class="btn btn-default" id="pickfiles">
                <i class="glyphicon glyphicon-plus"></i>
                <span>添加图片</span>
            </a>
            <span>（推荐分辨率：1242 * 660）</span>
        </div>
    </div>
    <div class="form-group">
        <label for="detail" class="control-label col-sm-2">品牌介绍</label>
        <div class="col-sm-8">
            {!! Form::textarea('detail', null, ['class' => 'form-control', 'placeholder' => '请输入品牌介绍']); !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="recommend">推荐状态</label>
        <div class="col-sm-10">
            <select name="recommend" class="form-control w150">
                <option value="1" @if(isset($category->recommend) && $category->recommend == 1) selected='selected' @endif>推荐</option>
                <option value="0" @if(isset($category->recommend) && $category->recommend == 0) selected='selected' @endif>不推荐</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-hover btn-primary btn">
                @if (Route::currentRouteName() === 'admin.product_category.create')
                    添加
                @else
                    更新
                @endif
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}
@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('vendor/admin_tpl/vendor/jquery/jquery_ui/jquery-ui.min.css') }}">
@stop
@section('js')
<script type="text/javascript" src="{{ url('vendor/bower_components/plupload/js/plupload.full.min.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bower_components/plupload/js/i18n/zh_CN.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bower_components/js-sdk/src/qiniu.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/image_upload.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.magnific-link').magnificPopup({ 
      type: 'image'
      // other options
      // gallery:{enabled:true}
    });
    $(".spinner-basic").spinner();
});
</script>
@stop
