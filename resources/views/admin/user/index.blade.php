@extends('layouts.admin')
@section('content')
    <div id="content">
        @include('partials.admin.status_success')
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">
                    <span class="glyphicon glyphicon-tasks"></span>
                    用户列表
                </div>
            </div>
            <div class="panel-body pn">
                <div class="panel-body bg-light admin-form">
                    {!! Form::open(['route' => 'admin.user.index', 'method' => 'GET', 'class' => 'form-inline form-listtablefilter']) !!}
                        <div class="col-md-4">
                            <label>用户名：</label>
                            <span>
                                @if (!empty($filter['name']))
                                    <input type="search" class="form-control gui-input" name="filter[name]" value="{{ $filter['name'] }}"/>
                                @else
                                    <input type="search" class="form-control gui-input" name="filter[name]"/>
                                @endif
                            </span>
                        </div>
                        <div class="col-md-2">
                            <label class="field select" for="filter[verify_status]">
                                {!! Form::select('filter[verify_status]', $verify_status, $filter['verify_status']) !!}
                                <i class="arrow"></i>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default">筛选</button>
                    {!! Form::close() !!}
                </div>
                <div class="table-responsive">
                    <div class="bs-component">
                        <table class="table table-bordered table-striped mbn">
                            <thead>
                                <tr>
                                    <th class="text-center">用户id</th>
                                    <th class="text-center">用户名</th>
                                    <th class="text-center">昵称</th>
                                    <th class="text-center">手机号码</th>
                                    <th class="text-center">状态</th>
                                    <th class="text-center">验证状态</th>
                                    <th class="text-center">服务城市</th>
                                    <th class="text-center">注册时间</th>
                                    <th class="text-center">用户地址</th>
                                    <th class="text-center">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr class="text-center">
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->nick_name }}</td>
                                        <td>{{ $user->cell_phone }}</td>
                                        <td>{{ $user->getIsActive() }}</td>
                                        <td>{{ $user->verify_status() }}</td>
                                        <td>{{ config('system.database.city_code')[$user->primary_city]['name'] }}</td>
                                        <td>{{ $user->created_at }}</td>
                                        <td><a class="magnific-link" href="#address-popup" data-url="{{ route('admin.user.address', $user) }}">点击查看</a></td>
                                        @include('admin.user._option')
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class='dt-panelfooter clearfix'>
                    <div class="dataTables_paginate">
                        {!! $users->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
                <div id="address-popup" class="white-popup mfp-hide">
                    <div class="table-responsive mt20">
                        <div class="bs-component">
                            <table class="table table-bordered table-striped mbn">
                                <thead>
                                    <tr>
                                        <th class="text-center">地址id</th>
                                        <th class="text-center">联系人</th>
                                        <th class="text-center">联系电话</th>
                                        <th class="text-center">城市</th>
                                        <th class="text-center">地区</th>
                                        <th class="text-center">详细地址</th>
                                        <th class="text-center">是否默认地址</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $.ajax({
        type: 'GET',
        url: "{{ route('admin.user.address.config') }}"
    }).done(function(response) {
        $.address_config = response.data;
    }).fail(function() {
        alert('操作失败，请稍后重试');
    });
    $('.magnific-link').click(function(){
        $.ajax({
            type: 'GET',
            url: $(this).data('url')
        }).done(function(response) {
            var html = '';
            console.log(response);
            for (var i = 0; i < response.data.length; i++) {
                var is_default = response.data[i].is_default == 1 ? '是':'否';
                html += '<tr class="text-center">' + 
                        '<td>' + response.data[i].id + '</td>' + 
                        '<td>' + response.data[i].contact_user_name + '</td>' + 
                        '<td>' + response.data[i].contact_user_cell_phone + '</td>' + 
                        '<td>' + $.address_config[response.data[i].city]['name'] + '</td>' + 
                        '<td>' + $.address_config[response.data[i].city]['district'][response.data[i].district] + '</td>' + 
                        '<td>' + response.data[i].details + '</td>' + 
                        '<td>' + is_default + '</td></tr>';
            };
            $('#address-popup tbody').html(html);
            $.magnificPopup.open({
              items: {
                src: '#address-popup'
              },
              type: 'inline'
            }, 0);
        }).fail(function() {
            alert('操作失败，请稍后重试');
        });
    });
});
</script>
@stop
