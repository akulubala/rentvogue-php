<td class="text-center">
  <div class="btn-group text-center">
    <button type="button" class="btn
    @if ($user->is_active === 'Y') btn-success @else btn-warning @endif
    br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      @if ($user->is_active === 'Y') 正常 @else 禁用 @endif
      <span class="caret ml5"></span>
    </button>
    <ul class="dropdown-menu" role="menu" style="left: inherit;right:0">
        <li>
          <a href="{{ route('admin.user.edit', $user) }}">编辑</a>
        </li>
        <li>
          <a href="javascript:void(0);" class="btn-option" option='delete' data-url="{{ route('admin.user.destroy', $user) }}">删除</a>
        </li>
        <li class="divider"></li>
        <li @if ($user->is_active === 'Y') class="active" @endif>
             <a href="javascript:void(0);" class="btn-option" option='put' data-key="is_active" data-v="Y"  data-url="{{ route('admin.user.update', $user) }}">
                正常
             </a>
        </li>
        <li @if ($user->is_active === 'N') class="active" @endif>
            <a href="javascript:void(0);" class="btn-option" option='put' data-key="is_active" data-v="N" data-url="{{ route('admin.user.update', $user) }}">
                禁用
            </a>
        </li>
    </ul>
  </div>
</td>
