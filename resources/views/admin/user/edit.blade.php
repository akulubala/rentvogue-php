@extends('layouts.admin')
@section('content')
    <div id="content">
      <div class="panel">
        @include('partials.admin.errors')
         <div class="panel-heading">
            <div class="panel-title">
                <span class="glyphicon glyphicon-tasks"></span>
                编辑用户
            </div>
        </div>
        {!! Form::open(['route' => ['admin.user.update', $user], 'method' => 'PUT' ,'class' => 'form-horizontal']) !!}
          <div class="panel-body">
            <div class="form-group">
              <label for="name" class="control-label col-sm-2">用户名</label>
              <div class="col-sm-10">
                <input type="text" name="name" id="name" class="form-control" placeholder="请输入用户名" value="{{ $user->name }}">
              </div>
            </div>
            <div class="form-group">
              <label for="nick_name" class="control-label col-sm-2">昵称</label>
              <div class="col-sm-10">
                <input type="text" name="nick_name" id="nick_name" class="form-control" placeholder="请输入昵称" value="{{ $user->nick_name }}">
              </div>
            </div>
            <div class="form-group">
              <label for="cell_phone" class="control-label col-sm-2">手机</label>
              <div class="col-sm-10">
                <input type="text" name="cell_phone" id="cell_phone" class="form-control" readonly value="{{ $user->cell_phone }}">
              </div>
            </div>
            <div class="form-group">
              <label for="id_number" class="control-label col-sm-2">身份证号码</label>
              <div class="col-sm-10">
                <input type="text" name="id_number" id="id_number" class="form-control" placeholder="请输入身份证号码" value="{{ $user->id_number }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="body_figure">体型</label>
              <div class="col-sm-10">
                  <select name="body_figure" class="form-control">
                      <option value="10" @if($user->body_figure == '10') selected='selected' @endif>超级瘦</option>
                      <option value="20" @if($user->body_figure == '20') selected='selected' @endif>瘦小</option>
                      <option value="30" @if($user->body_figure == '30') selected='selected' @endif>健美</option>
                      <option value="40" @if($user->body_figure == '40') selected='selected' @endif>可爱胖</option>
                      <option value="50" @if($user->body_figure == '50') selected='selected' @endif>不好意思说</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="body_figure">穿衣尺寸</label>
              <div class="col-sm-10">
                  <select name="clothes_size" class="form-control">
                      <option value="10" @if($user->clothes_size == '10') selected='selected' @endif>S</option>
                      <option value="20" @if($user->clothes_size == '20') selected='selected' @endif>M</option>
                      <option value="30" @if($user->clothes_size == '30') selected='selected' @endif>L</option>
                      <option value="40" @if($user->clothes_size == '40') selected='selected' @endif>XL</option>
                      <option value="50" @if($user->clothes_size == '50') selected='selected' @endif>XXL</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label for="body_height" class="control-label col-sm-2">身高</label>
              <div class="col-sm-10">
                <input type="text" name="body_height" id="body_height" class="form-control" placeholder="请输入身高" value="{{ $user->body_height }}">
              </div>
            </div>
            <div class="form-group">
              <label for="body_weight" class="control-label col-sm-2">体重</label>
              <div class="col-sm-10">
                <input type="text" name="body_weight" id="body_weight" class="form-control" placeholder="请输入体重" value="{{ $user->body_weight }}">
              </div>
            </div>
            <div class="form-group">
              <label for="body_chest" class="control-label col-sm-2">胸围</label>
              <div class="col-sm-10">
                <input type="text" name="body_chest" id="body_chest" class="form-control" placeholder="请输入胸围" value="{{ $user->body_chest }}">
              </div>
            </div>
            <div class="form-group">
              <label for="waistline" class="control-label col-sm-2">腰围</label>
              <div class="col-sm-10">
                <input type="text" name="waistline" id="waistline" class="form-control" placeholder="请输入腰围" value="{{ $user->waistline }}">
              </div>
            </div>
            <div class="form-group">
              <label for="body_hipline" class="control-label col-sm-2">臀围</label>
              <div class="col-sm-10">
                <input type="text" name="body_hipline" id="body_hipline" class="form-control" placeholder="请输入臀围" value="{{ $user->body_hipline }}">
              </div>
            </div>
            <div class="form-group">
              <label for="outseam" class="control-label col-sm-2">裤长</label>
              <div class="col-sm-10">
                <input type="text" name="outseam" id="outseam" class="form-control" placeholder="请输入裤长" value="{{ $user->outseam }}">
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="is_active">状态</label>
                <div class="col-sm-10">
                    <select name="is_active" class="form-control">
                        <option value="Y" @if($user->is_active == 'Y') selected='selected' @endif>正常</option>
                        <option value="N" @if($user->is_active == 'N') selected='selected' @endif>禁止</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="verify_status">验证状态</label>
                <div class="col-sm-10">
                    <select name="verify_status" class="form-control">
                        <option value="0" @if($user->verify_status == 0) selected='selected' @endif>未上传</option>
                        <option value="1" @if($user->verify_status == 1) selected='selected' @endif>已上传</option>
                        <option value="2" @if($user->verify_status == 2) selected='selected' @endif>未通过</option>
                        <option value="3" @if($user->verify_status == 3) selected='selected' @endif>已通过</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
              <label for="user_device_id" class="control-label col-sm-2">用户设备ID</label>
              <div class="col-sm-10">
                <input type="text" name="user_device_id" id="user_device_id" class="form-control" placeholder="请输入用户设备ID" value="{{ $user->user_device_id }}">
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="primary_city">服务城市</label>
                <div class="col-sm-10">
                    <select name="primary_city" class="form-control">
                        <option value="310000" @if($user->primary_city == '310000') selected='selected' @endif>上海市</option>
                        <option value="110000" @if($user->primary_city == '110000') selected='selected' @endif>北京市</option>
                        <option value="350200" @if($user->primary_city == '350200') selected='selected' @endif>厦门市</option>
                        <option value="440300" @if($user->primary_city == '440300') selected='selected' @endif>深圳市</option>
                        <option value="510100" @if($user->primary_city == '510100') selected='selected' @endif>成都市</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                 <button type="submit" class="btn btn-hover btn-primary btn">更新</button>
                </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
@stop
