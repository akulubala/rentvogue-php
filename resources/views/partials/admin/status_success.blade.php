@if (session('success'))
<div class="alert alert-success alert-dismissable">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<i class="fa fa-check pr10"></i>
	<strong>{{ session('success') }}</strong>
</div>
@endif