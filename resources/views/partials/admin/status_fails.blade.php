@if (session('fails'))
<div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <strong>{{ session('fails') }}</strong>
</div>
@endif
