<aside id="sidebar_left" class="nano nano-primary affix">
  <div class="sidebar-left-content nano-content">

    <ul class="nav sidebar-menu">
      <li class="sidebar-label pt15">后台管理中心</li>
      <li>
        <a href="{{ route('admin.index.index') }}" >
          <span class="glyphicon glyphicon-home"></span>
          <span class="sidebar-title">首页</span>
        </a>
      </li>
      @if (Auth::admin()->get()->role == 'super_admin')
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.admin') }}" href="#">
          <span class="glyphicons glyphicons-old_man"></span>
          <span class="sidebar-title">管理员管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.admin.index') }}">
                <a href="{{ route('admin.admin.index') }}">
                    管理员列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.admin.create') }}">
                <a href="{{ route('admin.admin.create') }}">
                    新建管理员
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.user') }}" href="#">
          <span class="fa fa-gear"></span>
          <span class="sidebar-title">微信菜单管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
                <li class="{{ nav_class_routes('admin.wechat-menus.index') }}">
                    <a href="{{ route('admin.wechat-menus.index') }}">
                        微信菜单列表
                    </a>
                </li>
                <li class="{{ nav_class_routes('admin.wechat-menus.create') }}">
                    <a href="{{ route('admin.wechat-menus.create') }}" >
                        新建微信菜单
                    </a>
                </li>
        </ul>
      </li>
      @endif
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.user') }}" href="#">
          <span class="glyphicons glyphicons-user"></span>
          <span class="sidebar-title">用户管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.user.index') }}">
                <a href="{{ route('admin.user.index') }}">
                    用户列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.verify.index') }}">
                <a href="{{ route('admin.verify.index') }}">
                    待验证用户列表
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('product.admin') }}" href="#">
          <span class="glyphicons glyphicons-cargo"></span>
          <span class="sidebar-title">产品管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.product.index', 'admin.product.product_property.index', 'admin.product.product_unavailable.index') }}">
                <a href="{{ route('admin.product.index') }}">
                    产品列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.product.create') }}">
                <a href="{{ route('admin.product.create') }}" >
                    新建产品
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.product_category') }}" href="#">
          <span class="glyphicons glyphicons-shopping_bag"></span>
          <span class="sidebar-title">产品类别管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.product_category.index') }}">
                <a href="{{ route('admin.product_category.index') }}">
                    产品类别（系列）列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.product_category.create') }}">
                <a href="{{ route('admin.product_category.create') }}" >
                    新建产品类别（系列）
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.slide') }}" href="#">
          <span class="glyphicons glyphicons-film"></span>
          <span class="sidebar-title">幻灯片管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.slide.index') }}">
                <a href="{{ route('admin.slide.index') }}">
                    幻灯片列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.slide.create') }}">
                <a href="{{ route('admin.slide.create') }}" >
                    新建幻灯片
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.style') }}" href="#">
          <span class="glyphicons glyphicons-t-shirt"></span>
          <span class="sidebar-title">款式管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.style.index') }}">
                <a href="{{ route('admin.style.index') }}">
                    款式列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.style.create') }}">
                <a href="{{ route('admin.style.create') }}" >
                    新建款式
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.occasion') }}" href="#">
          <span class="glyphicons glyphicon-glass"></span>
          <span class="sidebar-title">场合管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.occasion.index') }}">
                <a href="{{ route('admin.occasion.index') }}">
                    场合列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.occasion.create') }}">
                <a href="{{ route('admin.occasion.create') }}" >
                    新建场合
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.color') }}" href="#">
          <span class="octicon octicon-color-mode"></span>
          <span class="sidebar-title">颜色管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.color.index') }}">
                <a href="{{ route('admin.color.index') }}">
                    颜色列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.color.create') }}">
                <a href="{{ route('admin.color.create') }}" >
                    新建颜色
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.supplier') }}" href="#">
          <span class="glyphicons glyphicons-truck"></span>
          <span class="sidebar-title">供应商管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.supplier.index') }}">
                <a href="{{ route('admin.supplier.index') }}">
                    供应商列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.supplier.create') }}">
                <a href="{{ route('admin.supplier.create') }}" >
                    新建供应商
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.tag') }}" href="#">
          <span class="glyphicons glyphicons-tags"></span>
          <span class="sidebar-title">标签管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.tag.index') }}">
                <a href="{{ route('admin.tag.index') }}">
                    标签列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.tag.create') }}">
                <a href="{{ route('admin.tag.create') }}" >
                    新建标签
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.order') }}" href="#">
          <span class="fa fa-money"></span>
          <span class="sidebar-title">订单管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['next_week_delivery'=> '1']]) }}">
                <a href="{{ route('admin.order.index').'?next_week_delivery=1' }}">
                    一周内待发货订单
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.order.index', 'admin.order.sale_service.index') }}">
                <a href="{{ route('admin.order.index') }}">
                    所有订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'paid_not_deliveried']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=paid_not_deliveried' }}">
                    已付款未发货订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'paid_not_take']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=paid_not_take' }}">
                    已付款未自提订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'not_paid_deposit']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=not_paid_deposit' }}">
                    未付押金订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'delivering']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=delivering' }}">
                    已发货订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'recieved']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=recieved' }}">
                    已签收订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'returned']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=returned' }}">
                    已返还订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'cancel_apply']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=cancel_apply' }}">
                    申请退单订单
                </a>
            </li>
            <li class="{{ nav_class_routes(['route' => 'admin.order.index', 'params' => ['filter%5Border_status%5D'=> 'sales_warranty']]) }}">
                <a href="{{ route('admin.order.index').'?filter[order_status]=sales_warranty' }}">
                    售后服务中订单
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.hot_word') }}" href="#">
          <span class="glyphicons glyphicons-book"></span>
          <span class="sidebar-title">热词管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.hot_word.index') }}">
                <a href="{{ route('admin.hot_word.index') }}">
                    热词列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.hot_word.create') }}">
                <a href="{{ route('admin.hot_word.create') }}" >
                    新建热词
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.comment') }}" href="#">
          <span class="glyphicons glyphicons-circle_info"></span>
          <span class="sidebar-title">评论管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.comment.index') }}">
                <a href="{{ route('admin.comment.index') }}">
                    评论列表
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.coupon_config') }}" href="#">
          <span class="glyphicons glyphicons-gift"></span>
          <span class="sidebar-title">优惠券管理</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.coupon_config.index') }}">
                <a href="{{ route('admin.coupon_config.index') }}">
                    套券列表
                </a>
            </li>
            <li class="{{ nav_class_routes('admin.coupon_config.create') }}">
                <a href="{{ route('admin.coupon_config.create') }}">
                    新建套券
                </a>
            </li>
        </ul>
      </li>
      <li>
        <a class="accordion-toggle {{ menu_open_routes('admin.feedback') }}" href="#">
          <span class="glyphicons glyphicons-chat"></span>
          <span class="sidebar-title">意见反馈</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
            <li class="{{ nav_class_routes('admin.feedback.index') }}">
                <a href="{{ route('admin.feedback.index') }}">
                    反馈列表
                </a>
            </li>
        </ul>
      </li>
    </ul>
  </div>
</aside>
