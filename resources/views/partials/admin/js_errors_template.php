<script type="text/template" id="js_errors_template">
<div class="alert alert-danger" id='js_errors'>
    <ul>
          <% _.each(items,function(item,key,arr) { %>
              <li>
                 <span><%= item %></span>
               </li>
           <% }); %>
    </ul>
</div>
</script>
