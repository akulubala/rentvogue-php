<header id="topbar">
<div class="topbar-left">
    <ol class="breadcrumb">
        <li class="crumb-icon">
          <a href="{{ route('admin.index.index') }}">
            <span class="glyphicon glyphicon-home"></span>
          </a>
        </li>
        @if(isset($first_level))
        <li>
            @if (isset($first_level['link']))
                <a href="{{ $first_level['link'] }}" >{{ $first_level['text'] }}</a>
            @else
                {{ $first_level['text'] }}
            @endif
        </li>
        @endif
        @if(isset($second_level))
        <li>
            @if (isset($second_level['link']))
                <a href="{{ $second_level['link'] }}" >{{ $second_level['text'] }}</a>
            @else
                {{ $second_level['text'] }}
            @endif
        </li>
        @endif
        @if(isset($third_level))
        <li>
            @if (isset($third_level['link']))
                <a href="{{ $third_level['link'] }}" >{{ $third_level['text'] }}</a>
            @else
                {{ $third_level['text'] }}
            @endif
        </li>
        @endif
    </ol>
</div>
</header>
