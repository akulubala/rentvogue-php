<header class="navbar navbar-fixed-top bg-light">
<div class="navbar-branding dark">
  <a class="navbar-brand" href="{{ route('admin.index.index') }}">
    婚纱租赁
  </a>
  <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
</div>
<ul class="nav navbar-nav navbar-right">
  <li class="dropdown">
    <a href="javascript:void(0);" data-toggle="dropdown">
      <span class="icon-admin"></span>
      {{ Auth::admin()->get()->user_name }}
      <span class="caret caret-tp hidden-xs"></span>
    </a>
    <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
      <li class="list-group-item">
        <a href="{{ route('admin.admin_password.eidt') }}">
          <span class="fa fa-gear"></span>更改密码</a>
      </li>
      <li class="list-group-item">
        <a href="{{ route('admin.login.sign_out') }}">
          <span class="fa fa-power-off"></span>退出</a>
      </li>
    </ul>
  </li>
</ul>
</header>
