<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>RentVogue | @yield('sub_title')</title>
  <meta name="csrf_token" content="{{ session()->token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{ elixir('complied/admin.css') }}">
  @yield('style')
</head>
<body class="form-inputs-page sb-l-o sb-r-c onload-check">
  <div id="main">
    @include('partials.admin.layout_header')
    @include('partials.admin.layout_left_side_bar')
    <section id="content_wrapper">
        @yield('breadcrumb')
        @yield('content')
    </section>
  </div>
  <script type="text/javascript" src="{{ elixir('complied/admin.js') }}"></script>
  @yield('js')
</body>
</html>
