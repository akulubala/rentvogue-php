import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/': {
		component: require('../com/b/Home')
	},
	'/你好': {
		component: require('../com/b/Home')
	}
})

router.redirect({
	'*': '/'
})

router.start({
	name: 'home'
}, 'body')