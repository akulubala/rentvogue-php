import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/': {
		component: require('../com/b/Notice')
	}
})

router.redirect({
	'*': '/'
})

router.start({
	name: 'notice'
}, 'body')