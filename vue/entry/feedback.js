import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/': {
		component: require('../com/b/Feedback')
	}
})

router.redirect({
	'*': '/'
})

router.start({
	name: 'feedback'
}, 'body')