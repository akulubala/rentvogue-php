import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/join': {
		component: require('../com/b/Partner/Join')
	},
	'/other': {
		component: require('../com/b/Partner/Other')
	}
})

router.redirect({
	'*': '/join'
})

router.start({
	name: 'partner'
}, 'body')