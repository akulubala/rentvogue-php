import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/': {
		component: require('../com/b/goods/Goods')
	},
	'/submit': {
		component: require('../com/b/goods/Submit')
	},
	'/wechat-pay': {
		component: require('../com/b/goods/WechatPay')
	},
	'/alipay': {
		component: require('../com/b/goods/Alipay')
	},
	'/pay-feedback': {
		component: require('../com/b/goods/PayFeedback')
	}
})

router.redirect({
	'*': '/'
})

router.start({
	name: 'goods'
}, 'body')