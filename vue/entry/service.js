import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/problem': {
		component: require('../com/b/Service/Problem')
	},
	'/contact': {
		component: require('../com/b/Service/Contact')
	}
})

router.redirect({
	'*': '/problem'
})

router.start({
	name: 'service'
}, 'body')