import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/users/orders': {
		component: require('../com/b/User/MyOrders')
	},
	'/users/orders/after-sale': {
		component: require('../com/b/User/AfterSale')
	},
	'/users/orders/return-submit-succeed': {
		component: require('../com/b/User/ReturnSubmitSucceed')
	},
	'/users/orders/return-goods': {
		component: require('../com/b/User/ReturnGoods')
	},
	'/users/orders/order-detail': {
		component: require('../com/b/User/OrderDetail')
	},
	'/users/address': {
		component: require('../com/b/User/Address')
	},
	'/users/setting': {
		component: require('../com/b/User/Setting')
	},
	'/users/coupon': {
		component: require('../com/b/User/Coupon')
	},
	'/notice': {
		component: require('../com/b/Notice')
	},
	'/home': {
		component: require('../com/b/Home')
	},
	'/goods/': {
		component: require('../com/b/goods/Goods')
	},
	'/goods/submit': {
		component: require('../com/b/goods/Submit')
	},
	'/goods/wechat-pay': {
		component: require('../com/b/goods/WechatPay')
	},
	'/goods/alipay': {
		component: require('../com/b/goods/Alipay')
	},
	'/goods/pay-feedback': {
		component: require('../com/b/goods/PayFeedback')
	},
	'/feedback': {
		component: require('../com/b/Feedback')
	},
	// '/': require('../route'),
	'/product': {
		component: require('../layout'),
		subRoutes: {
			'/': {
				component: require('../page/goods')
			}
		}
	},
	// '/company': require('../route/company'),
	'/company': {
		component: require('../layout/company'),
		subRoutes: {
			'/brand': {
				component: require('../page/company-brand')
			},
			'/product': {
				component: require('../page/company-product')
			}
		}
	},
	// '/partner': require('../route/partner'),
	'/partner': {
		component: require('../layout/partner'),
		subRoutes: {
			'/join': {
				component: require('../page/partner-join')
			},
			'/other': {
				component: require('../page/partner-other')
			}
		}
	},
	// '/service': require('../route/service'),
	'/service': {
		component: require('../layout/service'),
		subRoutes: {
			'/contact': {
				component: require('../page/service-contact')
			},
			'/problem': {
				component: require('../page/service-problem')
			}
		}
	},
	// '/user': require('../route/user'),
	'/user': {
		component: require('../layout/user'),
		subRoutes: {
			'/order': {
				component: require('../page/user-order'),
				subRoutes: {
					'/unpaid': {
						component: require('../page/user-order-unpaid')
					},
					'/finish': {
						component: require('../page/user-order-finish')
					},
					'/return': {
						component: require('../page/user-order-return')
					}
				}
			},
			'/address': {
				component: require('../page/user-address')
			},
			'/coupon': {
				component: require('../page/user-coupon')
			},
			'/invite': {
				component: require('../page/user-invite')
			},
			'/setting': {
				component: require('../page/user-setting'),
				subRoutes: {
					'/basic': {
						component: require('../page/user-setting-basic')
					},
					'/password': {
						component: require('../page/user-setting-password')
					},
					'/address': {
						component: require('../page/user-setting-address')
					},
					'/social': {
						component: require('../page/user-setting-social')
					}
				}
			}
		}
	}
})

router.redirect({
	'*': '/home',
	'/company': '/company/brand',
	'/service': '/service/problem',
	'/partner': '/partner/join',
	'/user': '/user/order',
	'/user/order': '/user/order/unpaid',
	'/user/setting': '/user/setting/basic'
})

router.start({
	name: 'web'
}, 'body')