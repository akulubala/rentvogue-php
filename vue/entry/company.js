import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/brand': {
		component: require('../com/b/Company/Brand')
	},
	'/product': {
		component: require('../com/b/Company/Product')
	}
})

router.redirect({
	'*': '/brand'
})

router.start({
	name: 'company'
}, 'body')