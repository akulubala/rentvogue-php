import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/orders': {
		component: require('../com/b/User/MyOrders')
	},
	'/orders/after-sale': {
		component: require('../com/b/User/AfterSale')
	},
	'/orders/return-submit-succeed': {
		component: require('../com/b/User/ReturnSubmitSucceed')
	},
	'/orders/return-goods': {
		component: require('../com/b/User/ReturnGoods')
	},
	'/orders/order-detail': {
		component: require('../com/b/User/OrderDetail')
	},
	'/address': {
		component: require('../com/b/User/Address')
	},
	'/setting': {
		component: require('../com/b/User/Setting')
	},
	'/coupon': {
		component: require('../com/b/User/Coupon')
	}
})

router.redirect({
	'*': '/orders'
})

router.start({
	name: 'user'
}, 'body')