var path = require('path');

module.exports = {
    entry: require('../entry'),
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: /\.js$/,
                loader: 'babel!eslint',
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'url',
                query: {
                    limit: 10000,
                    name: 'image/[name].[ext]?[hash]'
                }
            }
        ]
    },
    output: {
        path: 'public',
        publicPath: '/',
        filename: '[name].js'
    },
    resolve: {
        root: [
            path.join(__dirname, '..')
        ],
        extensions: ['', '.js', '.vue']
    },
    vue: {
        loaders: {
            js: 'babel!eslint'
        }
    }
}
