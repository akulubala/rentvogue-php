var config = require('./webpack.config')

config.devtool = '#eval-source-map'

config.devServer = {
    noInfo: true
}

config.output.filename = config.output.path + '/[name].js'

module.exports = config
