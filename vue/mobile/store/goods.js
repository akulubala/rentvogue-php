import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Popup from './popup'
import Basic from './basic'
import User from './user'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 获取商品标签
		initGoodsTag ({dispatch, state}) {
			Api.resource('style/2/tag').get()
				.then(({data, headers}) => {
					dispatch('setOrderTag', data.data.tags)
				})
		},
		// 获取热词
		initHotWords ({dispatch, state}) {
			Api.resource('hotword').get()
				.then(({data, headers}) => {
					dispatch('setHotWords', data.data.hotwords)
				})
		},
		getGoodsList ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			Api.getGoodsList({
				data: obj.data,
				success: ({data}) => {
					Basic.actions.loadEnd()
					if (obj.from == 'filter') {
						if (obj.data.page) {
							dispatch('setFilterGoodsPage', data.data)
						}
						else {
							dispatch('setFilterGoods', data.data)
						}
					}
					if (typeof obj.after !== 'undefined') {
						obj.after()
					}
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '获取商品失败！'
					Popup.state.globalMsg.show = true
				}
			})
		}
	},
	mutations: {
		// 传递标签参数
		setOrderTag (state, data) {
			state.tags = data
		},
		setHotWords (state, data) {
			state.hotWords = data
		},
		setFilterGoods (state, data) {
			state.filterGoods = data.data
			state.filterPage = {
				'current_page': data.current_page,
				'total': data.last_page
			}
		},
		setFilterGoodsPage (state, data) {
			for (var i = 0; i < data.data.length; i++) {
				state.filterGoods.push(data.data[i])
			}
			state.filterPage = {
				'current_page': data.current_page,
				'total': data.last_page
			}
		}
	},
	state: {
		tags: [],
		filterGoods: [],
		hotWords: [],
		filterPage: {}
	}
})