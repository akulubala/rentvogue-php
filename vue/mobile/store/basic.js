import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 获取全部分类信息
		initCategory ({dispatch, state}) {
			Api.getCategory({
				success: ({data}) => {
					dispatch('setCategory', data.data)
				}
			})
		},
		// 获取全部供应商
		initSupplier ({dispatch, state}) {
			Api.resource('supplier').get()
				.then(({data, headers}) => {
					dispatch('setSupplier', data.data.suppliers)
				})
		},
		// 获取首页 baaner slider 信息
		initBanner ({dispatch, state}) {
			Api.resource('slide').get()
				.then(({data, headers}) => {
					dispatch('setBanner', data.data.slides)
				})
		},
		// loading 三种状态
		loadStart ({actions, state}) {
			console.log('开始')
			state.loading.show = true
			state.loading.width = 0
			actions.loading()
		},
		loading ({actions, state}) {
			state.loading.width += 1
			if (state.loading.width < 95) {
				setTimeout(() => {
					actions.loading()
				}, 5)
			}
		},
		loadEnd ({state}) {
			console.log('结速')
			state.loading.width = 100
			setTimeout(() => {
				state.loading.show = false
			}, 100)
		}
	},
	mutations: {
		setCategory (state, data) {
			state.category = data
		},
		setSupplier (state, data) {
			state.supplier = data
		},
		setBanner (state, data) {
			state.banner = data
		}
	},
	state: {
		category: [],
		supplier: [],
		banner: [],
		loading: {
			show: false,
			width: 0
		}
	}
})