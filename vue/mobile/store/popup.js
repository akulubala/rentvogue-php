import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		globalMsg: {
			show: false,
			single: '',
			more: {
				list: [],
				title: ''
			}
		},
		confirmGoods: {
			show: false,
			args: {}
		},
		deleteOrder: {
			show: false,
			args: {}
		},
		cancelOrder: {
			show: false,
			args: {}
		},
		deleteAddress: {
			show: false,
			id: ''
		},
		exchange: {
			show: false,
			id: ''
		}
	}
})