import Vue from 'vue'
import Vuex from 'vuex'
import Popup from './popup'
import Basic from './basic'
import Api from 'api'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 登录
		signIn ({actions, dispatch, state}, obj) {
			Api.signIn({
				data: obj.data,
				success: res => {
					dispatch('setToken', res.data.data.token)
					obj.after()
					actions.getUser()
				},
				error: res => {
					Popup.state.globalMsg.single = res.data.message
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 退出登录
		signOut ({dispatch, state}, obj) {
			Api.signOut({
				data: {
					'Authorization': state.token
				},
				success: res => {
					let wsCache = new WebStorageCache()
					wsCache.delete('auth')
					obj.after()
				}
			})
		},
		// 是否已登录
		isSignin ({actions, state}, obj) {
			Basic.actions.initCategory()
			let wsCache = new WebStorageCache()
			if (wsCache.get('auth')) {
				state.token = wsCache.get('auth')
				actions.initCoupons()
				actions.initCity()
				actions.getUser(obj)
			}
		},
		bindWeixin ({actions, state}, obj) {
			Api.getWeiXin({
				data: {
					'code': obj.code
				},
				success: res => {
					if (res.data.data.access_token) {
						let data = {
						 	access_token: res.data.data.access_token,
							openid: res.data.data.openid,
							oauth_type: 'weixin'
						}
						actions.putUserInfo({
							data: data,
							after: (res) => {
								state.user.weixin_avatar = res.data.data.weixin_avatar
							}
						})
					}
				}
			})
		},
		bindWeiBo ({actions, state}, obj) {
			Api.getWeiBo({
				data: {
					'code': obj.code
				},
				success: res => {
					if (res.data.data.access_token) {
						let data = {
						 	access_token: res.data.data.access_token,
							openid: res.data.data.uid,
							oauth_type: 'weibo'
						}
						actions.putUserInfo({
							data: data,
							after: (res) => {
								state.user.weibo_avatar = res.data.data.weibo_avatar
							}
						})
					}
				}
			})
		},
		// 获取用户信息
		getUser ({dispatch, state}, obj) {
			Api.getUserInfo({
				data: {
					'token': state.token
				},
				success: res => {
					dispatch('setUser', res.data.data)
					if (obj) {
						obj.after()
					}
				},
				error: res => {
					this.$route.router.go('/home')
				}
			})
		},
		// 获取验证码
		getVerifyCode ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			Api.verifyCode({
				data: {
					'phone_number': obj.phone_number
				},
				success: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '验证码已发送，请注意查收！'
					Popup.state.globalMsg.show = true
					obj.after()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '验证码获取失败，请重试！'
					Popup.state.globalMsg.show = true
					obj.after()
				}
			})
		},
		// 用户注册
		register ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			Api.signUp({
				data: {
					'cell_phone': obj.cell_phone,
					'cell_phone_verify_code': obj.cell_phone_verify_code,
					'nick_name': obj.nick_name,
					'password': obj.password,
					'password_confirmation': obj.password
				},
				success: res => {
					Basic.actions.loadEnd()
					dispatch('setToken', res.data.data.token)
					obj.after()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '注册失败'
					for (let list in res.data) {
						for (let i = 0, j = res.data[list].length; i < j ; i++) {
							let text = res.data[list][i]
							Popup.state.globalMsg.more.list.push(text)
						}
					}
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 重置密码
		resetPassword ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			Api.findUserPassword({
				data: obj.data,
				success: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '重置密码成功！'
					Popup.state.globalMsg.show = true
					obj.after()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '重置密码失败'
					for (let list in res.data) {
						for (let i = 0, j = res.data[list].length; i < j ; i++) {
							let text = res.data[list][i]
							Popup.state.globalMsg.more.list.push(text)
						}
					}
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 修改用户信息
		putUserInfo ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			obj.data.id = state.user.id
			Api.putUserInfo({
				data: obj.data,
				success: res => {
					Basic.actions.loadEnd()
					obj.after(res)
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '操作失败!'
					for (let list in res.data) {
						for (let i = 0, j = res.data[list].length; i < j ; i++) {
							Popup.state.globalMsg.more.list.push(res.data[list][i])
						}
					}
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 获取通知消息
		initNotices ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.getNoticesPage({
				data: {
					'user_id': state.user.id,
					'page': args.page
				},
				success: ({data}) => {
					dispatch('setNotices', data.data)
					Basic.actions.loadEnd()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '获取消息失败！'
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 删除通知消息
		delNotice ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.delNotice({
				data: {
					'user_id': state.user.id,
					'notice_id': args.notice_id
				},
				success: res => {
					state.notices.data.splice(args.index, 1)
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '删除消息成功！'
					Popup.state.globalMsg.show = true
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '删除消息失败！'
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 获取所有订单
		initOrders ({dispatch, state}) {
			Basic.actions.loadStart()
			Api.resource('user/' + state.user.id + '/order').get()
				.then(({data, headers}) => {
					dispatch('setOrders', data.data)
					Basic.actions.loadEnd()
				})
		},
		// 更新订单状态
		putOrderStatus ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putOrderStatus({
				data: {
					'user_id': state.user.id,
					'order_id': args.order_id,
					'order_status': args.order_status
				},
				success: ({data}) => {
					dispatch('updateStatus', data.data)
					Basic.actions.loadEnd()
					args.after()
				},
				error: res => {
					args.afterClose()
					Popup.state.globalMsg.single = res.data.message
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 删除订单
		delOrderStatus ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.deleteOrder({
				data: {
					'user_id': state.user.id,
					'order_id': args.order_id
				},
				success: ({data}) => {
					dispatch('delOrder', args.order_id)
					Basic.actions.loadEnd()
					args.after()
				},
				error: res => {
					Popup.state.globalMsg.single = res.data.message
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 获取全部优惠券
		initCoupons ({dispatch, state}) {
			Api.getCouponList({
				success: ({data}) => {
					dispatch('setCoupons', data.data.coupons)
				}
			})
		},
		// 获取全部地址
		initAddress ({dispatch, state}) {
			Basic.actions.loadStart()
			Api.resource('user/' + state.user.id + '/address').get()
				.then(({data, headers}) => {
					dispatch('setAddress', data.data.addresses)
					Basic.actions.loadEnd()
				})
		},
		// 更新收货地址
		updateAddress ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			args.user_id = state.user.id
			Api.updateAddress({
				data: args,
				success: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '更新收货地址成功！'
					Popup.state.globalMsg.show = true
					actions.initAddress()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '更新地址信息失败！'
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 删除收货地址
		deleteAddress ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			Api.deleteAddress({
				data: {
					user_id: state.user.id,
					id: args.id
				},
				success: res => {
					args.after()
				},
				error: res => {
					Popup.state.globalMsg.single = '删除收货地址失败！'
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 添加收货地址
		addAddress({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			Api.addAddress({
				data: {
					'token': state.token,
					'user_id': state.user.id,
					'city': args.city,
					'district': args.district,
					'contact_user_cell_phone': args.contact_user_cell_phone,
					'contact_user_name': args.contact_user_name,
					'details': args.details,
					'is_default': args.is_default
				},
				success: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '添加收货地址成功！'
					Popup.state.globalMsg.show = true
					args.after()
					actions.initAddress()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '添加收货地址失败'
					for (let i in res.data) {
						Popup.state.globalMsg.more.list.push(res.data[i][0])
					}
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 获取全部供应城市
		initCity ({dispatch, state}) {
			Api.getCity({
				success: ({data}) => {
					dispatch('setcity', data.data)
				}
			})
		},
		// 更新用户当前供应城市
		putPrimaryCity ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putUserInfo({
				data: {
					'id': state.user.id,
					'primary_city': args.primary_city
				},
				success: res => {
					state.user.primary_city = args.primary_city
					Popup.state.globalMsg.single = '保存供应城市成功！'
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				},
				error: res => {
					Popup.state.globalMsg.single = '保存供应城市失败！'
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 意见反馈
		sentOpinion ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			Api.sentOpinion({
				data: obj.data,
				success: res => {
					obj.after()
					Popup.state.globalMsg.single = '您的意见反馈已经提交成功，我们会尽快处理！'
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				},
				error: res => {
					Popup.state.globalMsg.single = '提交失败，请重新提交!'
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 返还商品
		putRefund ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putRefund({
				data: {
					'order_id': args.order_id,
					'express_company': args.express_company,
					'express_number': args.express_number
				},
				success: res => {
					state.refund = res.data.data
					Popup.state.globalMsg.single = '返还信息提交成功!'
					Popup.state.globalMsg.show = true
					Basic.actions.loadEnd()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '提交失败'
					let message = '所提交的订单号错误，或者不存在！'
					Popup.state.globalMsg.more.list.push(message)
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 获取全部供应城市
		getRefund ({dispatch, state}, obj) {
			Basic.actions.loadStart()
			Api.resource('order/' + obj.id + '/refund').get()
				.then(({data, headers}) => {
					dispatch('setRefund', data.data)
					Basic.actions.loadEnd()
				})
		},
		// 获取订单详情
		initOrderDetail ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			Api.getOrderDetail({
				data: {
					'user_id': state.user.id,
					'order_id': args.order_id
				},
				success: ({data}) => {
					Basic.actions.loadEnd()
					dispatch('setOrderDetail', data.data)
					if (state.orderDetail.express_company !== '商家直送' && state.orderDetail.express_number > 0 && state.orderDetail.express_company !== '上门自提') {
						actions.initExpressDetail(state.orderDetail)
					}
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '获取订单信息失败！'
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 添加评论
		addComment ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.addComment({
				data: {
					'product_name_group_id': args.product_name_group_id,
					'order_id': args.order_id,
					'star': args.star,
					'tags': args.tags,
					'images': args.images,
					'comment': args.comment
				},
				success: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '提交成功'
					let message = '您的晒单评论已提交成功，我们将会尽快进行审核！'
					Popup.state.globalMsg.more.list.push(message)
					Popup.state.globalMsg.show = true
					args.after()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.more.title = '提交评价失败'
					for (let list in res.data) {
						for (let i = 0, j = res.data[list].length; i < j ; i++) {
							Popup.state.globalMsg.more.list.push(res.data[list][i])
						}
					}
					Popup.state.globalMsg.show = true
				}
			})
		},
		initCollects ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.getCollect({
				data: {
					'user_id': state.user.id,
					'page': args.page
				},
				success: ({data}) => {
					dispatch('setCollects', data.data)
					Basic.actions.loadEnd()
				}
			})
		},
		getCollectsPage ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.getCollect({
				data: {
					'user_id': state.user.id,
					'page': args.page
				},
				success: ({data}) => {
					dispatch('setCollectsPage', data.data)
					args.after()
					Basic.actions.loadEnd()
				}
			})
		}
	},
	mutations: {
		setToken (state, data) {
			state.token = data
			// 本地储存 token
			let wsCache = new WebStorageCache()
			// 设置 token 有效时间
			wsCache.set('auth', data, {exp: 86400})
		},
		setUser (state, data) {
			state.user = data
		},
		setNotices (state, data) {
			state.notices = data
		},
		setOrders (state, data) {
			state.orders = data
		},
		updateStatus (state, data) {
			for (var i = 0; i < state.orders.length; i++) {
				if (state.orders[i].id == data.id) {
					state.orders[i].order_status = data.order_status
				}
			}
		},
		delOrder (state, order_id) {
			for (var i = 0; i < state.orders.length; i++) {
				if (state.orders[i].id == order_id) {
					state.orders.splice(i, 1)
				}
			}
		},
		setCoupons (state, data) {
			state.coupons = data
			for (let i = 0, j = state.coupons.length; i < j; i++) {
				if (state.coupons[i].cut < 201) {
					state.coupons[i].class = 'coupon-1'
				}
				if (200 < state.coupons[i].cut && state.coupons[i].cut < 501) {
					state.coupons[i].class = 'coupon-2'
				}
				if (500 < state.coupons[i].cut && state.coupons[i].cut < 1001) {
					state.coupons[i].class = 'coupon-3'
				}
				if (1000 < state.coupons[i].cut) {
					state.coupons[i].class = 'coupon-4'
				}
			}
		},
		setAddress (state, data) {
			state.address = data
		},
		setcity (state, data) {
			state.cities = data
		},
		setRefund (state, data) {
			state.refund = data
		},
		setOrderDetail (state, data) {
			state.orderDetail = data
			state.product = data.product
		},
		setCollects (state, data) {
			state.collects = data.data
			state.collectsPage = {
				'current_page': data.current_page,
				'total': data.last_page
			}
		},
		setCollectsPage (state, data) {
			for (var i = 0; i < data.data.length; i++) {
				state.collects.push(data.data[i])
			}
			state.collectsPage = {
				'current_page': data.current_page,
				'total': data.last_page
			}
		}
	},
	state: {
		token: '',
		user: {},
		notices: {},
		orders: [],
		coupons: [],
		address: [],
		cities: [],
		refund: false,
		orderDetail: {},
		product: {},
		collects: [],
		collectsPage: {}
	}
})