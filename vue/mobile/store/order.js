import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Popup from './popup'
import Basic from './basic'
import User from './user'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 获取订单详情
		initOrderDetails ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			Api.getOrderDetail({
				data: {
					'user_id': User.state.user.id,
					'order_id': args.order_id
				},
				success: ({data}) => {
					Basic.actions.loadEnd()
					dispatch('setOrderDetails', data.data)
					if (state.orderDetails.express_company !== '商家直送' && state.orderDetails.express_number > 0 && state.orderDetails.express_company !== '上门自提') {
						actions.initExpressDetail(state.orderDetails)
					}
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = '获取订单信息失败！'
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 获取快递物流信息
		initExpressDetail ({dispatch, state}, orderDetails) {
			Api.getExpressDetail({
				data: {
					express_number: orderDetails.express_number
				},
				success: ({data}) => {
					dispatch('setExpressTraces', data.data.Traces)
				},
				error: res => {
					Popup.state.globalMsg.single = '获取物流信息失败！'
					Popup.state.globalMsg.show = true
				}
			})
		},
		// 更新订单状态
		putOrderStatus ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putOrderStatus({
				data: {
					'user_id': User.state.user.id,
					'order_id': args.order_id,
					'order_status': args.order_status
				},
				success: ({data}) => {
					Basic.actions.loadEnd()
					state.orderDetails.order_status = data.data.order_status
					User.actions.initOrder({
						id: args.user_id
					})
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.globalMsg.single = res.data.message
					Popup.state.globalMsg.show = true
				}
			})
		}
	},
	mutations: {
		setOrderDetails (state, data) {
			state.orderDetails = data
			state.product = data.product
		},
		setExpressTraces (state, data) {
			state.expressTraces = data
		}
	},
	state: {
		expressTraces: [],
		orderDetails: {},
		product: {}
	}
})