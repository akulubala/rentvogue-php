import Vue from 'vue'
import Router from 'vue-router'
import User from '../../mobile/store/user'

// common styles in table list page & table detail page
// TODO load by .vue (support autoprefixer)
require('!style!css!sass!../../mobile/styles/reset.scss')
require('!style!css!sass!../../mobile/styles/global.scss')

Vue.use(Router)

var router = new Router()

router.map({
	'/': {
		component: require('../../mobile/components/layout'),
		subRoutes: {
			'home': {
				name: '首页',
				component: require('../../mobile/components/page/home')
			},
			'signin': {
				name: '登录',
				component: require('../../mobile/components/page/account/signin')
			},
			'register': {
				name: '注册',
				component: require('../../mobile/components/page/account/register')
			},
			'reset-password': {
				name: '忘记密码',
				component: require('../../mobile/components/page/account/reset-password')
			},
			'user': {
				component: require('../../mobile/components/layout/user'),
				subRoutes: {
					'/index': {
						name: '个人中心',
						bg: 'white',
						component: require('../../mobile/components/page/user')
					},
					'/basic': {
						name: '个人资料',
						hideMenu: true,
						component: require('../../mobile/components/page/user/basic')
					},
					'/bind-phone': {
						name: '绑定手机',
						component: require('../../mobile/components/page/user/bind-phone')
					},
					'/id-card': {
						name: '实名认证',
						component: require('../../mobile/components/page/user/id-card')
					},
					'/id-card-info': {
						name: '实名认证',
						component: require('../../mobile/components/page/user/id-card-info')
					},
					'/notices': {
						name: '消息通知',
						component: require('../../mobile/components/page/user/notices')
					},
					'/invitation': {
						name: '邀请返利',
						component: require('../../mobile/components/page/user/invitation')
					},
					'/coupons': {
						name: '我的优惠券',
						bg: 'white',
						component: require('../../mobile/components/page/user/coupons')
					},
					'/collects': {
						name: '我的收藏',
						bg: 'white',
						component: require('../../mobile/components/page/user/collects')
					},
					'/address': {
						name: '地址管理',
						hideMenu: true,
						component: require('../../mobile/components/page/user/address')
					},
					'/address/:id': {
						name: '修改地址',
						bg: 'white',
						component: require('../../mobile/components/page/user/address-reset')
					},
					'/address/add': {
						name: '添加地址',
						bg: 'white',
						component: require('../../mobile/components/page/user/address-add')
					},
					'/settings': {
						name: '设置',
						bg: 'white',
						component: require('../../mobile/components/page/user/settings')
					},
					'/reset-password': {
						name: '修改密码',
						component: require('../../mobile/components/page/user/reset-password')
					},
					'/reset-city': {
						name: '城市',
						bg: 'white',
						component: require('../../mobile/components/page/user/reset-city')
					},
					'/opinion': {
						name: '意见反馈',
						component: require('../../mobile/components/page/user/opinion')
					},
					'/problems': {
						name: '常见问题',
						component: require('../../mobile/components/page/user/problems')
					},
					'/about': {
						name: '关于',
						component: require('../../mobile/components/page/user/about')
					}
				}
			},
			'orders/:category': {
				name: '订单列表',
				hideMenu: true,
				component: require('../../mobile/components/page/orders')
			},
			'order-description': {
				name: '订单须知',
				component: require('../../mobile/components/page/orders/description')
			},
			'order-details/:order_id': {
				name: '订单详情',
				bg: 'white',
				component: require('../../mobile/components/page/orders/details')
			},
			'express/:order_id': {
				name: '物流信息',
				bg: 'white',
				component: require('../../mobile/components/page/orders/express')
			},
			'after-sale': {
				name: '申请售后',
				bg: 'white',
				component: require('../../mobile/components/page/orders/after-sale')
			},
			'refund': {
				name: '返还商品',
				hideMenu: true,
				component: require('../../mobile/components/page/orders/refund')
			},
			'refund-help': {
				name: '返还流程',
				component: require('../../mobile/components/page/orders/refund-help')
			},
			'order-comment': {
				name: '晒单',
				component: require('../../mobile/components/page/orders/comment')
			}
		}
	},
	'search': {
		name: '找产品',
		component: require('../../mobile/components/page/goods/search')
	}
})

router.redirect({
	'/': '/home',
	'*': '/home',
	'/user': '/user/index'
})

router.start({
	created () {
		let _this = this
		User.actions.isSignin({
			after () {
				_this.$root.$broadcast('signIn')
			}
		})
		$('title').text(this.$route.name + ' | RentVogue')
		$('html').removeClass('white', 'orders-color')
		if (this.$route.bg) {
			$('html').addClass(this.$route.bg)
		}
	},
	name: 'mobile'
}, 'body')

router.afterEach(function (transition) {
	$('title').text(transition.to.name + ' | RentVogue')
	$('html').removeClass('white', 'orders-color')
	if (transition.to.bg) {
		$('html').addClass(transition.to.bg)
	}
})
