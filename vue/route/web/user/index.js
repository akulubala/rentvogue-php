module.exports = {
	component: require('../../../layout/user'),
	subRoutes: {
		'/order': require('./order'),
		'/address': {
			name: '收货地址',
			component: require('../../../page/user/address')
		},
		'/coupon': {
			name: '我的优惠券',
			component: require('../../../page/user/coupon')
		},
		'/invite': {
			name: '邀请返现',
			component: require('../../../page/user/invite')
		},
		'/setting': require('./setting')
	}
}
