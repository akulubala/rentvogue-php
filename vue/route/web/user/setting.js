module.exports = {
	component: require('../../../layout/user/setting'),
	subRoutes: {
		'/basic': {
			name: '基本资料',
			component: require('../../../page/user/setting/basic')
		},
		'/password': {
			name: '修改密码',
			component: require('../../../page/user/setting/password')
		},
		'/address': {
			name: '城市服务',
			component: require('../../../page/user/setting/address')
		},
		'/social': {
			name: '帐号关联',
			component: require('../../../page/user/setting/social')
		}
	}
}
