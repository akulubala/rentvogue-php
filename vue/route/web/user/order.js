module.exports = {
	name: '我的订单',
	component: require('../../../layout/user/order'),
	subRoutes: {
		'/unpaid': {
			component: require('../../../page/user/order/unpaid')
		},
		'/finish': {
			component: require('../../../page/user/order/finish')
		},
		'/return': {
			component: require('../../../page/user/order/return')
		}
	}
}
