import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router()

router.map({
	'/company': require('./company'),
	'/partner': require('./partner'),
	'/service': require('./service'),
	'/user': require('./user'),
	'/': {
		component: require('../../layout'),
		subRoutes: {
			'id-card': {
				name: '实名认证',
				component: require('../../page/index/id-card')
			},
			'id-card-info': {
				name: '实名认证',
				component: require('../../page/index/id-card-info')
			},
			'phone': {
				name: '手机验证',
				component: require('../../page/index/phone')
			},
			'id-card-option': {
				name: '实名认证',
				component: require('../../page/index/id-card-option')
			},
			'user-info-option': {
				name: '身材信息',
				component: require('../../page/index/user-info-option')
			},
			'collect': {
				name: '收藏',
				component: require('../../page/index/collect')
			},
			'order-detail': {
				name: '订单详情',
				component: require('../../page/user/order/detail')
			},
			'order-comment': {
				name: '晒单',
				component: require('../../page/user/order/comment')
			},
			'notice': {
				name: '通知',
				component: require('../../page/index/notice')
			},
			'after-sale': {
				name: '申请售后',
				component: require('../../page/user/order/after-sale')
			},
			'customer-service': {
				name: '联系客服',
				component: require('../../page/user/order/customer-service')
			},
			'see-why': {
				name: '查看原因',
				component: require('../../page/user/order/see-why')
			},
			'return-goods': {
				name: '返还商品',
				component: require('../../page/user/order/return-goods')
			},
			'return-submit-succeed': {
				name: '返还商品成功',
				component: require('../../page/user/order/return-submit-succeed')
			},
			'pay': {
				name: '订单支付',
				component: require('../../page/pay/pay')
			},
			'pay-deposit': {
				name: '支付押金',
				component: require('../../page/user/order/pay-deposit')
			},
			'pay-feedback': {
				name: '支付反馈',
				component: require('../../page/pay/feedback')
			},
			'pay-deposit-feedback': {
				name: '支付反馈',
				component: require('../../page/pay/deposit-feedback')
			}
		}
	},
	'/share': {
		name: '分享',
		component: require('../../page/share/share')
	},
	'/share-goods': {
		name: '分享商品',
		component: require('../../page/share/goods')
	},
	'/guide': {
		name: '首页',
		component: require('../../layout/guide')
	},
	'/home': {
		name: '服装',
		component: require('../../com/b/Home')
	},
	'/goods': {
		component: require('../../com/b/goods/Goods')
	},
	'/goods/submit': {
		name: '提交订单',
		component: require('../../com/b/goods/Submit')
	}
})

router.redirect({
	'/': '/guide',
	'*': '/guide',
	'/company': '/company/brand',
	'/service': '/service/problem',
	'/partner': '/partner/join',
	'/user': '/users/orders',
	'/user/order': '/user/order/unpaid',
	'/user/setting': '/user/setting/basic'
})

router.start({
	data () {
		return {
			token: '',
			user: {},
			qiniuToken: {}
		}
	},
	created () {
		if (this.$route.name) {
			$('title').text(this.$route.name + ' | RentVogue')
		}
	},
	name: 'web'
}, 'body')

router.afterEach(function (transition) {
	if (transition.from.fullPath == '/goods' && transition.to.fullPath == '/goods') {
		return false
	}
	else {
		window.scrollTo(0, 0)
	}
})

router.afterEach(function (transition) {
	if (transition.to.name) {
		$('title').text(transition.to.name + ' | RentVogue')
	}
})
