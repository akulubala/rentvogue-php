module.exports = {
	component: require('../../layout/partner'),
	subRoutes: {
		'/join': {
			name: '加入品牌',
			component: require('../../page/partner/join')
		},
		'/other': {
			name: '其他合作',
			component: require('../../page/partner/other')
		}
	}
}
