module.exports = {
	component: require('../../layout/company'),
	subRoutes: {
		'/brand': {
			name: '品牌故事',
			component: require('../../page/company/brand')
		},
		'/product': {
			name: '产品',
			component: require('../../page/company/product')
		}
	}
}
