module.exports = {
	component: require('../../layout/service'),
	subRoutes: {
		'/contact': {
			name: '联系方式',
			component: require('../../page/service/contact')
		},
		'/problem': {
			name: '常见问题',
			component: require('../../page/service/problem')
		}
	}
}
