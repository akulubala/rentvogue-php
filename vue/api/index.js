import Vue from 'vue'
import Resource from 'vue-resource'

Vue.use(Resource)
let wsCache = new WebStorageCache()

Vue.http.headers.common = {
	'Content-Type': 'application/json',
	'Authorization': 'Bearer ' + wsCache.get('auth')
}
Vue.http.options.emulateHTTP = true
Vue.http.options.emulateJSON = true
// Vue.http.options.root = 'http://121.201.14.60:8080/index.php/api/v1'
Vue.http.options.root = '/index.php/api/v1'

export default {
	resource (resource) {
		return Vue.resource(resource)
	},
	signIn (input) {
		Vue.http.post('authenticate/sign-in', input.data).then(input.success, input.error)
	},
	signUp (input) {
		Vue.http.post('authenticate/sign-up', input.data).then(input.success, input.error)
	},
	// 退出登录
	signOut(input) {
		Vue.http.get('authenticate/sign-out', input.data).then(input.success, input.error)
	},
	// 获取手机验证码
	verifyCode (input) {
		Vue.http.get('cell_phone_code', input.data).then(input.success, input.error)
	},
	// 获取用户信息
	getUserInfo (input) {
		Vue.http.headers.common['Authorization'] = 'Bearer ' + input.data.token
		Vue.http.get('user/0').then(input.success, input.error)
	},
	// 更新用户信息
	putUserInfo ({data, success, error}) {
		Vue.http.put('user/' + data.id, data).then(success, error)
	},
	// 手机重置密码
	findUserPassword ({data, success, error}) {
		Vue.http.put('user/0', data).then(success, error)
	},
	//  首页获取商品列表
	getGoodsList ({data, success, error}) {
        // // data 引用同步，解决方案需精简
        // if (data.end_price == 0 || tmp.end_price == 10000) {
        //     data.end_price = ''
        // }
        // Vue.http.get('product', data).then(success, error)
        let tmp = JSON.parse(JSON.stringify(data))
        if (tmp.end_price == 0 || tmp.end_price == 10000) {
            tmp.end_price = ''
        }
		Vue.http.get('product', tmp).then(success, error)
	},
	// 获取分类
	getFilter ({data, success, error}) {
		Vue.http.get('category', data).then(success, error)
	},
	// 获取收藏产品
	getCollect ({data, success, error}) {
		Vue.http.get('user/' + data.user_id + '/favorite/?page=' + data.page).then(success, error)
	},
	// 更新收藏状态
	putFavorite (input) {
		Vue.http.post('product/favorite/' + input.data.goods_id).then(input.success, input.error)
	},
	// 获取产品详情
    getGoodsDetail ({data, success, error}) {
        Vue.http.get('product/' + data.id, data).then(success, error)
    },
	// 获取产品详情
	getGoods (input) {
		Vue.http.get('product/' + input.data.goods_id).then(input.success, input.error)
	},
	getUserAddressList (input) {
		Vue.http.get('user/' + input.data.id + '/address', input.data).then(input.success, input.error)
	},
	// 获取订单详情
	getOrderDetail (input) {
		Vue.http.get('user/' + input.data.user_id + '/order/' + input.data.order_id).then(input.success, input.error)
	},
	// 更新订单状态
	putOrderStatus (input) {
		Vue.http.put('user/' + input.data.user_id + '/order/' + input.data.order_id, input.data).then(input.success, input.error)
	},
	// 删除订单
	deleteOrder (input) {
		Vue.http.delete('user/' + input.data.user_id + '/order/' + input.data.order_id).then(input.success, input.error)
	},
	// 订单号获取订单信息
	getOrderInfo ({data, success, error}) {
		Vue.http.get('order/' + data.order_number).then(success, error)
	},
	// 获取其他用户感兴趣
	getGoodsRecommend (input) {
		Vue.http.get('product/recommend', input.data).then(input.success, input.error)
	},
	// 获取产品评论
	getGoodsComment (input) {
		Vue.http.get('product/' + input.data.goods_id + '/comment').then(input.success, input.error)
	},
	// 根据页码获取产品评论
	getGoodsCommentIndex (input) {
		Vue.http.get('product/' + input.data.goods_id + '/comment?page=' + input.data.page).then(input.success, input.error)
	},
	// 提交返还商品
	putRefund (input) {
		Vue.http.post('order/' + input.data.order_id + '/refund', input.data).then(input.success, input.error)
	},
	// 快递信息查询
	getExpressDetail ({data, success, error}) {
		Vue.http.get('get_express_number/' + data.express_number).then(success, error)
	},
	// 获取优惠券
	getCouponList (input) {
		Vue.http.get('coupon').then(input.success, input.error)
	},
	// 领取优惠券
	getCoupon (input) {
		Vue.http.post('coupon/get', input.data).then(input.success, input.error)
	},
	// 获取通知消息
	getNoticesPage (input) {
		Vue.http.get('user/' + input.data.user_id + '/sms?page=' + input.data.page).then(input.success, input.error)
	},
	// 删除单挑通知消息
	delNotice (input) {
		Vue.http.delete('user/' + input.data.user_id + '/sms/' + input.data.notice_id).then(input.success, input.error)
	},
	// 获取城市服务所有城市
	getCity ({success, error}) {
		Vue.http.get('city').then(success)
	},
	// 添加收货地址
	addAddress ({data, success, error}) {
		Vue.http.post('user/' + data.user_id + '/address', data).then(success, error)
	},
	// 修改收货地址
	updateAddress ({data, success, error}) {
		Vue.http.put('user/' + data.user_id + '/address/' + data.id, data).then(success, error)
	},
	// 删除收货地址
	deleteAddress ({data, success, error}) {
		Vue.http.delete('user/' + data.user_id + '/address/' + data.id, data).then(success, error)
	},
	// 提交订单
	submitOrder ({data, success, error}) {
		Vue.http.post('user/' + data.user_id + '/order', data).then(success, error)
	},
	// 获取所有分类
	getCategory ({success}) {
		Vue.http.get('category').then(success)
	},
	// 获取七牛 uptoken
	initQiniuToken ({data, success}) {
		Vue.http.get('qiniu_token').then(success)
	},
	// 提交评论
	addComment ({data, success, error}) {
		Vue.http.post('product/' + data.product_name_group_id + '/comment', data).then(success, error)
	},
	// 获取微信token
	getWeiXin ({data, success}) {
		Vue.http.get('weixin_access_token/' + data.code).then(success)
	},
	// 获取新浪token
	getWeiBo ({data, success}) {
		Vue.http.get('weibo_access_token/' + data.code).then(success)
	},
	// 第三方登录
	otherSignIn ({data, success}) {
		Vue.http.post('authenticate/app-oauth', data).then(success)
	},
	// 验证身份信息
	verifyIdCard ({data, success, error}) {
		Vue.http.post('id-verify', data).then(success, error)
	},
	// 支付押金
	payDeposit ({data, success, error}) {
		Vue.http.post('user/' + data.user_id + '/order/' + data.order_id + '/deposit', data).then(success, error)
	},
	// 订单支付
	pay ({data, success, error}) {
		Vue.http.post('user/' + data.user_id + '/order/' + data.order_id + '/pay', data).then(success, error)
	},
	// 提交意见
	sentOpinion ({data, success, error}) {
		Vue.http.post('feedback', data).then(success, error)
	}
}
