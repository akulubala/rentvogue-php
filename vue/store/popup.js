import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		login: false,
		sizeModal: false,
		findPassWord: false,
		register: false,
		addAddress: false,
		otherCoupon: false,
		cancelOrder: {
			show: false,
			args: {}
		},
		deleteOrder: {
			show: false,
			args: {}
		},
		deleteAddress: {
			show: false,
			args: {}
		},
		confirmGoods: {
			show: false,
			args: {}
		},
		orderDescription: false,
		commonInfo: {
			show: false,
			single: '',
			more: {
				list: [],
				title: ''
			}
		}
	}
})