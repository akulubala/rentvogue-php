import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Basic from 'store/basic'
import Popup from 'store/popup'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
        init ({dispatch, actions}, args) {

            // 暂存页码
            let page = args.page

            // 1. 无参时显示推荐商品列表
            if ('{}' === JSON.stringify(args)) {
                actions.initRecommend()
            }
            // 2. 只有页码参数时显示推荐商品列表
            else if ((delete args.page) && '{}' === JSON.stringify(args)) {
                actions.initRecommend(page)
            }
            // 3. 其他情况显示筛选结果商品列表
            else {
                args.page = page
                actions.initList(args)
            }
        },
        initRecommend ({dispatch}, args) {
            Basic.actions.loadStart()
            Api.getGoodsRecommend({
                data: {
                    page: args || 1
                },
                success: ({data}) => {
                    // 更新商品列表
                    dispatch('setListContent', data.data.data)
                    // 更新商品分页
                    dispatch('setListPage', {
                        prev: data.data.prev_page_url,
                        now: data.data.current_page,
                        next: data.data.next_page_url,
                        total: data.data.last_page
                    })
                    // 清空商品规格筛选
                    dispatch('setListFilter', {})
                    Basic.actions.loadEnd()
                }
            })
        },
        initList ({dispatch, actions}, args) {
            Basic.actions.loadStart()
			Api.getGoodsList({
				data: args,
				success: ({data}) => {
                    // 更新商品列表
					dispatch('setListContent', data.data.data)
                    // 更新商品分页
                    dispatch('setListPage', {
                        prev: data.data.prev_page_url,
                        now: data.data.current_page,
                        next: data.data.next_page_url,
                        total: data.data.last_page
                    })
                    // 更新商品筛选
                    dispatch('setListFilter', args)
                    Basic.actions.loadEnd()
				},
                error: ({data}) => {
                    Basic.actions.loadEnd()
                    Popup.state.commonInfo.single = data.message
                    Popup.state.commonInfo.show = true
                }
			})
        },
        initCategory ({dispatch}, args) {
            Api.getFilter({
                success: ({data}) => {
                    ~`
                    color
                    occasions
                    product_category
                    body_figures
                    sizes
                    style
                    usage_categories
                    `.replace(/\w+/g, (s) => {
                        dispatch('setCategory' + s.charAt(0).toUpperCase() + s.substr(1), data.data[s])
                    })
                }
            })
        },
        // 获取商品标签
        initGoodsTag ({dispatch, state}) {
            Api.resource('style/2/tag').get()
                .then(({data, headers}) => {
                    dispatch('setOrderTag', data.data.tags)
                })
        },
        // 获取品牌介绍
        initBrandIntr ({dispatch, state}, args) {
            Api.resource('category/' + args.brand_id).get()
                .then(({data, headers}) => {
                    dispatch('setBrandIntr', data.data.detail)
                })
        }
	},
	mutations: {
        setCategoryColor (state, data) {
            state.category.color = data
        },
        setCategoryOccasions (state, data) {
            let occasions = []
            for (let i in data) {
                occasions.push({
                    id: i,
                    name: data[i]
                })
            }
            state.category.occasions = occasions
        },
        setCategoryProduct_category (state, data) {
            let product_category = []
            for (let i in data) {
                data[i].id = i
                product_category.push(data[i])
            }
            state.category.product_category = product_category
        },
        setCategorySizes (state, data) {
            let sizes = []
            for (let i in data) {
                sizes.push({
                    id: i,
                    name: data[i]
                })
            }
            state.category.sizes = sizes
        },
        setCategoryStyle (state, data) {
            let style = []
            for (let i in data) {
                style.push({
                    id: i,
                    name: data[i]
                })
            }
            state.category.style = style
        },
        setCategoryBody_figures (state, data) {
            let body_figures = []
            for (let i in data) {
                body_figures.push({
                    id: i,
                    name: data[i]
                })
            }
            state.category.body_figure = body_figures
        },
        setCategoryUsage_categories (state, data) {
            let usage_categories = []
            for (let i in data) {
                usage_categories.push({
                    id: i,
                    name: data[i]
                })
            }
            state.category.usage_categories = usage_categories
        },
		setListFilter (state, data) {
			state.list.filter = data
		},
        setListPage (state, data) {
            state.list.page = data
        },
        setListContent (state, data) {
            state.list.content = data
        },
        // 传递标签参数
        setOrderTag (state, data) {
            state.tags = data
        },
        // 传递品牌介绍
        setBrandIntr (state, data) {
            state.brandIntr = data
        }
	},
	state: {
        category: {
            // 分类列表
            usage_categories: [],
            // 使用场景列表
            occasions: [],
            // 品牌列表
            product_category: [],
            // 尺寸列表
            sizes: [],
            // 颜色列表
            color: [],
            // 款式列表
            style: [],
            // 身材列表
            body_figure: []
        },
		list: {
            filter: {
                // 尺寸
                size: undefined,
                // 颜色
                color: undefined,
                // 款式
                style: undefined,
                // 品牌
                category: undefined,
                // 分类
                usage: undefined,
                // 使用场景
                occasion: undefined,
                // 身材
                body_figure: undefined,
                // 价格起点
                start_price: undefined,
                // 价格终点
                end_price: undefined,
                // //
                // prices: undefined,
                // 搜索
                name: undefined,
                // 租赁日期起点
                rent_start: undefined,
                // 租赁日期终点
                rent_end: undefined,
                // 第几页
                page: undefined
            },
            page: {
                prev: '',
                now: '',
                next: '',
                total: ''
            },
            content: []
        },
        tags: [],
        brandIntr: ''
	}
})
