import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Basic from './basic'
import Popup from './popup'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 获取通知消息
		initNotices ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.getNoticesPage({
				data: {
					'user_id': args.user_id,
					'page': args.page
				},
				success: ({data}) => {
					dispatch('setNotices', data.data)
					Basic.actions.loadEnd()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.single = '获取消息失败！'
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 删除通知消息
		delNotice ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.delNotice({
				data: {
					'user_id': args.user_id,
					'notice_id': args.notice_id
				},
				success: res => {
					state.notices.data.splice(args.index, 1)
					Basic.actions.loadEnd()
					Popup.state.commonInfo.single = '删除消息成功！'
					Popup.state.commonInfo.show = true
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.single = '删除消息失败！'
					Popup.state.commonInfo.show = true
				}
			})
		}
	},
	mutations: {
		setNotices (state, data) {
			state.notices = data
		}
	},
	state: {
		notices: {}
	}
})