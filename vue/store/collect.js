import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Basic from './basic'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		initCollect ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.getCollect({
				data: {
					'user_id': args.user_id,
					'page': args.page
				},
				success: ({data}) => {
					dispatch('setCollect', data.data)
					Basic.actions.loadEnd()
				}
			})
		}
	},
	mutations: {
		setCollect (state, data) {
			state.collect = data.data
			state.collectPage = {
				'current_page': data.current_page,
				'total': data.last_page
			}
		}
	},
	state: {
		collect: {},
		collectPage: {}
	}
})