import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Popup from 'store/popup'
import Basic from 'store/basic'
import User from 'store/user'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 获取订单详情
		initOrderDetail ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			Api.getOrderDetail({
				data: {
					'user_id': args.user_id,
					'order_id': args.order_id
				},
				success: ({data}) => {
					Basic.actions.loadEnd()
					dispatch('setOrderDetail', data.data)
					if (state.orderDetail.express_company !== '商家直送' && state.orderDetail.express_number > 0 && state.orderDetail.express_company !== '上门自提') {
						actions.initExpressDetail(state.orderDetail)
					}
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.single = '获取订单信息失败！'
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 获取快递物流信息
		initExpressDetail ({dispatch, state}, orderDetail) {
			Api.getExpressDetail({
				data: {
					express_number: orderDetail.express_number
				},
				success: ({data}) => {
					dispatch('setExpressTraces', data.data.Traces)
				},
				error: res => {
					Popup.state.commonInfo.single = '获取物流信息失败！'
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 更新订单状态
		putOrderStatus ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putOrderStatus({
				data: {
					'user_id': args.user_id,
					'order_id': args.order_id,
					'order_status': args.order_status
				},
				success: ({data}) => {
					Basic.actions.loadEnd()
					state.orderDetail.order_status = data.data.order_status
					User.actions.initOrder({
						id: args.user_id
					})
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.single = res.data.message
					Popup.state.commonInfo.show = true
				}
			})
		}
	},
	mutations: {
		setOrderDetail (state, data) {
			state.orderDetail = data
			state.product = data.product
		},
		setExpressTraces (state, data) {
			state.expressTraces = data
		}
	},
	state: {
		expressTraces: [],
		orderDetail: {},
		product: {}
	}
})