import Vue from 'vue'
import Api from 'api'
import Goods from 'store/goods'
import Basic from 'store/basic'

export default new Vue({
    computed: {
        category () {
            return Goods.state.category
        }
    },
    data () {
        return {
            list: [],
            size: {
                list: [],
                target: {
                    id: '',
                    name: ''
                }
            },
            color: {
                list: [],
                target: {
                    id: '',
                    rgb: ''
                }
            },
            unavaliable: {
                four_days: [],
                eight_days: []
            },
            target: {
                property_group_id: '',
                unavaliable: {}
            },
            goods: {},
            recommend: {},
            comment: {},
            loadStatus: 0
    	}
    },
	methods: {
        init (args, next) {
            Goods.actions.initCategory()
            Basic.actions.loadStart()
            this.loadStatus = 0
            Api.getGoodsDetail({
                data: {
                    id: args
                },
                success: ({data}) => {
                    $('title').text(data.data.name + ' | RentVogue')
                    // 1. 所有库存型号
                    this.list = data.data.other_data
                    // 2. 所有尺寸设置可否
                    this.setSize(data.data.size)
                    // 3. 所有颜色设置可否
                    this.setColor(data.data.color)
                    // 4. 设置不可租时间
                    this.setTarget({
                        property_group_id: data.data.property_group_id,
                        unavaliable: data.data.unavaliable
                    })
                    this.goods = data.data

                    let product_category = Basic.state.category.product_category
                    let brandId = ''
                    for (let key in product_category) {
                        if (product_category[key].name == this.goods.product_category) {
                            brandId = key
                        }
                    }
                    Goods.actions.initBrandIntr({
                        brand_id: brandId
                    })
                    this.loadStatus += 1
                    if (this.loadStatus == 3) {
                        Basic.actions.loadEnd()
                    }
                    next && next()
                }
            })
            Api.getGoodsRecommend({
                data: {
                    'name_group_id': args
                },
                success: res => {
                    this.recommend = res.data.data
                    this.loadStatus += 1
                    if (this.loadStatus == 3) {
                        Basic.actions.loadEnd()
                    }
                },
                error: res => {
                }
            })
            Api.getGoodsComment({
                data: {
                    'goods_id': args
                },
                success: res => {
                    this.comment = res.data.data
                    this.loadStatus += 1
                    if (this.loadStatus == 3) {
                        Basic.actions.loadEnd()
                    }
                },
                error: res => {
                }
            })
        },
        changeSize (id) {
            for (let i = 0, j = this.size.list.length; i < j; i++) {
                // 选择尺寸
                if (this.size.list[i].id == id) {
                    this.size.target = this.size.list[i]
                    break
                }
            }
            // 更新尺寸相应颜色列表
            this.setColor('', this.size.target.name)
            // 清空颜色选择
            this.color.target = ''
            // 清空当前 target
            this.target = {}
        },
        changeColor (id) {
            // 激活为选择颜色
            for (let i = 0, j = this.color.list.length; i < j; i++) {
                if (this.color.list[i].id == id) {
                    this.color.target = this.color.list[i]
                }
            }
            // 切换不可租时间
            for (let i = 0, j = this.list.length; i < j; i++) {
                if ((this.color.target.rgb == this.list[i].color) && (this.size.target.name == this.list[i].size)) {
                    this.setTarget(this.list[i])
                }
            }
        },
        setTarget (args) {
            this.target = args
        },
        setSize (args) {
            let sizeList = []
            // 循环所有尺寸
            for (let i = 0, j = this.category.sizes.length; i < j; i++) {
                // 添加到尺寸列表
                sizeList.push(this.category.sizes[i])
                // 循环库存设置有无
                for (let a = 0, b = this.list.length; a < b; a++) {
                    if (sizeList[i].name == this.list[a].size) {
                        sizeList[i].have = true
                    }
                }
                // 检测 target
                if (sizeList[i].name == args) {
                    this.size.target = sizeList[i]
                }
            }
            this.size.list = sizeList
        },
        setColor (args, size) {
            let colorList = []
            // 循环所有颜色
            for (let i = 0, j = this.category.color.length; i < j; i++) {
                // 添加到颜色列表
                colorList.push({
                    id: this.category.color[i].id,
                    rgb: this.category.color[i].rgb
                })
                // 循环库存设置有无
                for (let a = 0, b = this.list.length; a < b; a++) {
                    // 根据尺寸过滤颜色
                    if (size && this.list[a].size != size) {
                        continue
                    }
                    // 颜色筛选
                    if (colorList[i].rgb == this.list[a].color) {
                        colorList[i].have = true
                    }
                }
                // 检测 target
                if (colorList[i].rgb == args) {
                    this.color.target = colorList[i]
                }
            }
            this.color.list = colorList
        }
	}
})
