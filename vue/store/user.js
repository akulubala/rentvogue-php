import Vue from 'vue'
import Vuex from 'vuex'
import Api from 'api'
import Basic from 'store/basic'
import Popup from 'store/popup'

Vue.use(Vuex)

export default new Vuex.Store({
	actions: {
		// 获取全部地址
		initAddress ({dispatch, state}, args) {
			Api.resource('user/{id}/address').get(args)
				.then(({data, headers}) => {
					dispatch('setAddress', data.data.addresses)
					state.loadStatus += 1
					if (state.loadStatus == 2) {
						Basic.actions.loadEnd()
					}
				})
		},
		// 更新收货地址
		updateAddress ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			state.loadStatus = 1
			Api.updateAddress({
				data: args,
				success: res => {
					actions.initAddress({
						id: args.user_id
	 				})
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.single = '更新地址信息失败！'
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 删除收货地址
		deleteAddress ({dispatch, state, actions}, args) {
			Basic.actions.loadStart()
			state.loadStatus = 1
			Api.deleteAddress({
				data: args,
				success: res => {
					actions.initAddress({
						id: args.user_id
	 				})
				},
				error: res => {
					Popup.state.commonInfo.single = '删除收货地址失败！'
					Popup.state.commonInfo.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 添加收货地址
		addAddress({dispatch, state, actions}, args) {
			Api.addAddress({
				data: {
					'token': args.token,
					'user_id': args.user_id,
					'city': args.city,
					'district': args.district,
					'contact_user_cell_phone': args.contact_user_cell_phone,
					'contact_user_name': args.contact_user_name,
					'details': args.details,
					'is_default': args.is_default
				},
				success: res => {
					Basic.actions.loadStart()
					state.loadStatus = 1
					actions.initAddress({
						id: args.user_id
	 				})
	 				args.closeInput()
				},
				error: res => {
					Popup.state.commonInfo.more.title = '添加收货地址失败'
					for (let i in res.data) {
						Popup.state.commonInfo.more.list.push(res.data[i][0])
					}
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 获取全部订单
		initOrder ({dispatch, state}, args) {
			Api.resource('user/{id}/order').get(args)
				.then(({data, headers}) => {
					dispatch('setOrder', data.data)
					state.loadStatus += 1
					if (state.loadStatus == 2) {
						Basic.actions.loadEnd()
					}
				})
		},
		// 获取全部优惠券
		initCoupons ({dispatch, state}) {
			Api.getCouponList({
				success: ({data}) => {
					dispatch('setCoupons', data.data.coupons)
				}
			})
		},
		// 获取全部供应城市
		initCity ({dispatch, state}) {
			Api.getCity({
				success: ({data}) => {
					dispatch('setcity', data.data)
				}
			})
		},
		// 更新用户当前供应城市
		putPrimaryCity ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putUserInfo({
				data: {
					'id': args.id,
					'primary_city': args.primary_city
				},
				success: res => {
					args.after()
					Popup.state.commonInfo.single = '保存供应城市成功！'
					Popup.state.commonInfo.show = true
					Basic.actions.loadEnd()
				},
				error: res => {
					Popup.state.commonInfo.single = '保存供应城市失败！'
					Popup.state.commonInfo.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 更新订单状态
		putOrderStatus ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putOrderStatus({
				data: {
					'user_id': args.user_id,
					'order_id': args.order_id,
					'order_status': args.order_status
				},
				success: ({data}) => {
					dispatch('updateStatus', data.data)
					Basic.actions.loadEnd()
					if (args.source) {
						args.after()
					}
				},
				error: res => {
					Popup.state.commonInfo.single = res.data.message
					Popup.state.commonInfo.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 删除订单
		delOrderStatus ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.deleteOrder({
				data: {
					'user_id': args.user_id,
					'order_id': args.order_id
				},
				success: ({data}) => {
					dispatch('delOrder', args.order_id)
					Basic.actions.loadEnd()
					if (args.source == 'order-detail') {
						args.after()
					}
				},
				error: res => {
					Popup.state.commonInfo.single = res.data.message
					Popup.state.commonInfo.show = true
					Basic.actions.loadEnd()
				}
			})
		},
		// 添加评论
		addComment ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.addComment({
				data: {
					'product_name_group_id': args.product_name_group_id,
					'order_id': args.order_id,
					'star': args.star,
					'tags': args.tags,
					'images': args.images,
					'comment': args.comment
				},
				success: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.more.title = '您的评价已提交成功'
					let message = '您的晒单评论已提交成功，我们将会尽快进行审核！'
					Popup.state.commonInfo.more.list.push(message)
					Popup.state.commonInfo.show = true
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.more.title = '提交评价失败'
					for (let list in res.data) {
						for (let i = 0, j = res.data[list].length; i < j ; i++) {
							Popup.state.commonInfo.more.list.push(res.data[list][i])
						}
					}
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 返还商品
		putRefund ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.putRefund({
				data: {
					'order_id': args.order_id,
					'express_company': args.express_company,
					'express_number': args.express_number
				},
				success: res => {
					Basic.actions.loadEnd()
					args.after()
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.more.title = '提交失败'
					let message = '所提交的订单号错误，或者不存在！'
					Popup.state.commonInfo.more.list.push(message)
					Popup.state.commonInfo.show = true
				}
			})
		},
		// 支付押金
		payDeposit ({dispatch, state}, args) {
			Basic.actions.loadStart()
			Api.payDeposit({
				data: {
					'order_id': args.order_id,
					'user_id': args.user_id,
					'paid_method': args.paid_method,
					'call_back': args.call_back
				},
				success: res => {
					Basic.actions.loadEnd()
					let data = res.data.data
					args.after(data)
				},
				error: res => {
					Basic.actions.loadEnd()
					Popup.state.commonInfo.more.title = '支付押金失败！'
					for (let list in res.data) {
						for (let i = 0, j = res.data[list].length; i < j ; i++) {
							Popup.state.commonInfo.more.list.push(res.data[list][i])
						}
					}
					Popup.state.commonInfo.show = true
				}
			})
		}
	},
	mutations: {
		setAddress (state, data) {
			state.address = data
		},
		setOrder (state, data) {
			state.order = data
		},
		setCoupons (state, data) {
			state.coupons = data
			for (let i = 0, j = state.coupons.length; i < j; i++) {
				if (state.coupons[i].cut < 201) {
					state.coupons[i].class = 'coupon-1'
				}
				if (200 < state.coupons[i].cut && state.coupons[i].cut < 501) {
					state.coupons[i].class = 'coupon-2'
				}
				if (500 < state.coupons[i].cut && state.coupons[i].cut < 1001) {
					state.coupons[i].class = 'coupon-3'
				}
				if (1000 < state.coupons[i].cut) {
					state.coupons[i].class = 'coupon-4'
				}
			}
		},
		setcity (state, data) {
			state.cities = data
		},
		updateStatus (state, data) {
			for (var i = 0; i < state.order.length; i++) {
				if (state.order[i].id == data.id) {
					state.order[i].order_status = data.order_status
				}
			}
		},
		delOrder (state, order_id) {
			for (var i = 0; i < state.order.length; i++) {
				if (state.order[i].id == order_id) {
					state.order.splice(i, 1)
				}
			}
		}
	},
	state: {
		address: [],
		order: [],
		coupons: [],
		cities: [],
		loadStatus: 0
	}
})