<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    /**
     * 七牛云存储
     */
    'qiniu' => [
        'access_key' => env('QINIU_ACCESS_KEY'),
        'domain_name' => env('QINIU_DOMAIN_NAME'),
        'secret_key' => env('QINIU_SECRET_KEY'),
        'water_mask' => '?watermark/1/image/' . env('QINIU_WATERMARK') . '/gravity/Center'
    ],
    /**
     *  mobile sms
     */
    'sms' => [
        'url' => env('SMS_URL'),
        'username' => env('SMS_USERNAME', 'api'),
        'password' => env('SMS_PASSWD')
    ],
    /**
     * JPush
     */
    'jpush' => [
        'app_key' => env('JPUSH_APP_KEY'),
        'master_secret' => env('JPUSH_MASTER_SECRET')
    ],
    /**
     *  third part login
     */
    'weibo' => [
        'client_id' => env('WEIBO_KEY'),
        'client_secret' => env('WEIBO_SECRET'),
        'redirect' => env('WEIBO_REDIRECT_URI'),
    ],
    'weibo-web' => [
        'client_id' => env('WEIBO_WEB_KEY'),
        'client_secret' => env('WEIBO_WEB_SECRET'),
        'redirect' => env('WEIBO_WEB_REDIRECT_URI')
    ],
    'weixin-web' => [
        'client_id' => env('WEIXIN_WEB_KEY'),
        'client_secret' => env('WEIXIN_WEB_SECRET'),
        'redirect' => env('WEIXIN_WEB_REDIRECT_URI'),
    ],
    'weixin' => [
        'client_id' => env('WEIXIN_APP_KEY'),
        'client_secret' => env('WEIXIN_APP_SECRET'),
        'redirect' => env('WEIXIN_APP_REDIRECT_URI'),
    ],
    'weixin-mp' => [
        'app_id' => env('WEIXIN_MP_APP_ID'),
        'secret' => env('WEIXIN_MP_SECRET'),
        'token'  => env('WEIXIN_MP_TOKEN'),
        'aes_key' => env('WEIXIN_MP_AES'), // 可选
        'redirect' => env('WEIXIN_MP_REDIRECT')
    ],
    /**
     * ping++
     */
    'pingxx' => [
        'api_key' => env('PINGXX_API_KEY'),
        'app_key' => env('PINGXX_APP_KEY'),
        'subject' => env('PINGXX_SUBJECT'),
        'body' => env('PINGXX_BODY'),
        'deposit_subject' => env('PINGXX_DEPOSIT_SUBJECT'),
        'call_back' => env('PINGXX_CALL_BACK')
    ],
    /**
     * express
     */
    'express' => [
        'jump_url' => env('EXPRESS_JUMP_URL'),
        'key' => env('EXPRESS_KEY'),
        'b_id' => env('EXPRESS_B_ID')
    ],
    'id_verify' => [
        'url' => env('IDVERIFY_URL'),
        'key' => env('IDVERIFY_KEY')
    ]
];
